<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

		<title>RPP Editor</title>

		<!-- Bootstrap core CSS -->
		<link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="<?php echo asset('css/editor.css')?>" rel="stylesheet">
  </head>

  <body style="">

    <!-- Static navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.html">RPP Editor</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="dashboard.html">RPP</a></li>
            <li><a href="search.html">Pencarian</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
			<li>
				<a href="home.html">Keluar</a>
			</li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>


    <div class="container">
      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Editor</h1>
		<h2>Identitas</h2>
		<div class="identitas">
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="namaSekolah" class="col-sm-3 control-label">Nama Sekolah</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" id="namaSekolah" placeholder="Nama Sekolah">
					</div>
				</div>
				<div class="form-group">
					<label for="kelas" class="col-sm-3 control-label">Pemilik</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" id="author" placeholder="Pemilik">
					</div>
				</div>
				<div class="form-group">
					<label for="kelas" class="col-sm-3 control-label">Kelas</label>
					<div class="col-sm-3">
						<select class="form-control">
						  <option>1</option>
						  <option>2</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
						  <option>6</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="semester" class="col-sm-3 control-label">Semester</label>
					<div class="col-sm-3">
						<select class="form-control">
						  <option>I</option>
						  <option>II</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="temaUmum" class="col-sm-3 control-label">Tema Umum</label>
					<div class="col-sm-3">
						<select class="form-control">
						  <option>Tema 1</option>
						  <option>Tema 2</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="temaKhusus" class="col-sm-3 control-label">Tema Khusus</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="temaKhusus" placeholder="Tema Khusus">
					</div>
				</div>
				<div class="form-group">
					<label for="pelajranTerkait" class="col-sm-3 control-label">Pelajaran Terkait</label>
					<div class="col-sm-8">
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="agama">
							Agama
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="pkn">
							Pancasila dan Kewarganegaraan
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="bahasa">
							Bahasa Indonesia
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="matematika">
							Matematika
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="ipa">
							Ilmu Pengetahuan Alam
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="ips">
							Ilmu Pengetahuan Sosial
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="seni">
							Seni Budaya dan Prakarya
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="penjaskes">
							Pendidikan Jasmani, Olahraga, dan Kesehatan
						  </label>
						</div>
					</div>
				</div>
			</form>
			<h2>Detil Kompetensi Pelajaran</h2>
			<h3>Pelajaran X</h3>
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="kompetensiInti1" class="col-sm-3 control-label">Kompetensi Inti</label>
					<div class="col-sm-3">
						<select class="form-control">
						  <option>Kompetensi Inti 1</option>
						  <option>Kompetensi Inti 2</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="kompetensiDasar1" class="col-sm-3 control-label">Kompetensi Dasar</label>
					<div class="col-sm-9">
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="komdas1">
							Kompetensi Dasar 1
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="komdas2">
							Kompetensi Dasar 2
						  </label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="indikator1" class="col-sm-3 control-label">Indikator Pembelajaran</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="indikator1-1" placeholder="Indikator">
					</div>
					<button type="button" class="btn btn-primary btn-sm">Tambah</button>
				</div>
				<div class="form-group">
					<label for="tujuan1" class="col-sm-3 control-label">Tujuan Pembelajaran</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="tujuan1-1" placeholder="Tujuan">
					</div>
					<button type="button" class="btn btn-primary btn-sm">Tambah</button>
				</div>
			</form>
		</div>
		<div class="materimetode">
			<h2>Materi dan Metode Pembelajaran</h2>
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="materi" class="col-sm-3 control-label">Materi Pembelajaran</label>
					<div class="col-sm-6">
						<textarea class="form-control" rows="5" cols="50"  id="materi" placeholder="Materi"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="metode" class="col-sm-3 control-label">Metode Pembelajaran</label>
					<div class="col-sm-9">
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="diskusi">
							Diskusi
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="ceramah">
							Ceramah
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="demonstrasi">
							Demonstrasi
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="drama">
							Bermain Drama
						  </label>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="langkahpembelajaran">
		<h2>Langkah Pembelajaran</h2>
		<h3>Kegiatan Awal</h3>
			<div class="langkah">
				<h4>Langkah 1</h4>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="aksiAwal1" class="col-sm-3 control-label">Aksi Guru</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="3" cols="50"  id="aksiAwal1"></textarea>
						</div>
						<button type="button" class="btn btn-primary btn-sm">Detil</button>
					</div>
				</form>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="responAwal1" class="col-sm-3 control-label">Respon Siswa</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="3" cols="50"  id="aksiAwal1"></textarea>
						</div>
					</div>
				</form>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="alternatifAwal1" class="col-sm-3 control-label">Jika Respon Siswa Tidak Sesuai</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="3" cols="50"  id="alternatifAwal1"></textarea>
						</div>
						<button type="button" class="btn btn-primary btn-sm">Detil</button>
					</div>
				</form>
			</div>
			<button type="button" class="btn btn-primary">Tambah Langkah</button>
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="durasitotalAwal" class="col-sm-3 control-label">Durasi Total</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="durasitotalAwal">
					</div>
					Menit
				</div>
			</form>
		<h3>Kegiatan Inti</h3>
			<div class="langkah">
				<h4>Langkah 1</h4>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="aksiInti1" class="col-sm-3 control-label">Aksi Guru</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="3" cols="50"  id="aksiInti1"></textarea>
						</div>
						<button type="button" class="btn btn-primary btn-sm">Detil</button>
					</div>
				</form>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="responInti1" class="col-sm-3 control-label">Respon Siswa</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="3" cols="50"  id="aksiInti1"></textarea>
						</div>
					</div>
				</form>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="alternatifInti1" class="col-sm-3 control-label">Jika Respon Siswa Tidak Sesuai</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="3" cols="50"  id="alternatifInti1"></textarea>
						</div>
						<button type="button" class="btn btn-primary btn-sm">Detil</button>
					</div>
				</form>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="durasiInti1" class="col-sm-3 control-label">Durasi</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="durasiInti1">
						</div>
						Menit
					</div>
				</form>
			</div>
			<button type="button" class="btn btn-primary">Tambah Langkah</button>
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="durasitotalInti" class="col-sm-3 control-label">Durasi Total</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="durasitotalInti">
					</div>
					Menit
				</div>
			</form>
		<h3>Kegiatan Penutup</h3>
			<div class="langkah">
				<h4>Langkah 1</h4>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="aksiPenutup1" class="col-sm-3 control-label">Aksi Guru</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="3" cols="50"  id="aksiPenutup1"></textarea>
						</div>
						<button type="button" class="btn btn-primary btn-sm">Detil</button>
					</div>
				</form>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="responPenutup1" class="col-sm-3 control-label">Respon Siswa</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="3" cols="50"  id="aksiPenutup1"></textarea>
						</div>
					</div>
				</form>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="alternatifPenutup1" class="col-sm-3 control-label">Jika Respon Siswa Tidak Sesuai</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="3" cols="50"  id="alternatifPenutup1"></textarea>
						</div>
						<button type="button" class="btn btn-primary btn-sm">Detil</button>
					</div>
				</form>
				
			</div>
			<button type="button" class="btn btn-primary">Tambah Langkah</button>
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="durasitotalPenutup" class="col-sm-3 control-label">Durasi Total</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="durasitotalPenutup">
					</div>
					Menit
				</div>
			</form>
		</div>
		<div class="alatsumber">
			<h2>Alat dan Sumber Pembelajaran</h2>
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="alatPembelajaran" class="col-sm-3 control-label">Alat</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" id="alatPembelajaran" placeholder="Alat">
					</div>
				</div>
				<div class="form-group">
					<label for="sumberPembelajaran" class="col-sm-3 control-label">Sumber Belajar</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" id="sumberPembelajaran" placeholder="Sumber">
					</div>
				</div>
			</form>
		</div>
		<div class="penilaian">
			<h2>Penilaian</h2>
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="teknik" class="col-sm-3 control-label">Teknik</label>
					<div class="col-sm-3">
						<select class="form-control">
						  <option>Lisan</option>
						  <option>Tulisan</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="bentuk" class="col-sm-3 control-label">Bentuk</label>
					<div class="col-sm-9">
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="pilgan">
							Pilihan Ganda
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="uraian">
							Uraian
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="uraiansingkat">
							Uraian Singkat
						  </label>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="menjodohkan">
							Menjodohkan
						  </label>
						</div>
					</div>
				</div>
			</form>
			<div class="instrumen">
				<h3>Instrumen</h3>
				<div class="instrumenPilihanGanda">
					<h4>Pilihan Ganda<h4/>
					<table class="table">
						<thead>
							<tr>
								<th>Nomor</th>
								<th>Indikator</th>
								<th>Pertanyaan</th>
								<th>Jawaban</th>
								<th>Skor</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>
									<div class="form-group">
										<select class="form-control">
										  <option>1</option>
										  <option>2</option>
										</select>
									</div>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Pertanyaan">
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Jawaban A">
									<input type="text" class="form-control" placeholder="Jawaban B">
									<input type="text" class="form-control" placeholder="Jawaban C">
									<input type="text" class="form-control" placeholder="Jawaban D">
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Skor">
								</td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-primary btn-sm">Tambah Soal</button>
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="skorTotalPilihanGanda" class="col-sm-3 control-label">Skor Total</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="skorTotalPilihanGanda" placeholder="0-100">
							</div>
						</div>
					</form>
				</div>
				<div class="instrumenUraian">
					<h4>Uraian<h4/>
					<table class="table">
						<thead>
							<tr>
								<th>Nomor</th>
								<th>Indikator</th>
								<th>Pertanyaan</th>
								<th>Jawaban</th>
								<th>Skor</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>
									<div class="form-group">
										<select class="form-control">
										  <option>1</option>
										  <option>2</option>
										</select>
									</div>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Pertanyaan">
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Jawaban Grade A">
									<input type="text" class="form-control" placeholder="Jawaban Grade B">
									<input type="text" class="form-control" placeholder="Jawaban Grade C">
									<input type="text" class="form-control" placeholder="Jawaban Grade D">
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Skor Grade A">
									<input type="text" class="form-control" placeholder="Skor Grade B">
									<input type="text" class="form-control" placeholder="Skor Grade C">
									<input type="text" class="form-control" placeholder="Skor Grade D">
								</td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-primary btn-sm">Tambah Soal</button>
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="skorTotalUraian" class="col-sm-3 control-label">Skor Total</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="skorTotalUraian" placeholder="0-100">
							</div>
						</div>
					</form>
				</div>
				<div class="instrumenUraianSingkat">
					<h4>Uraian Singkat<h4/>
					<table class="table">
						<thead>
							<tr>
								<th>Nomor</th>
								<th>Indikator</th>
								<th>Pertanyaan</th>
								<th>Jawaban</th>
								<th>Skor</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>
									<div class="form-group">
										<select class="form-control">
										  <option>1</option>
										  <option>2</option>
										</select>
									</div>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Pertanyaan">
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Jawaban">
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Skor">
								</td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-primary btn-sm">Tambah Soal</button>
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="skorTotalUraianSingkat" class="col-sm-3 control-label">Skor Total</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="skorTotalUraianSingkat" placeholder="0-100">
							</div>
						</div>
					</form>
				</div>
				<div class="instrumenMenjodohkan">
					<h4>Menjodohkan<h4/>
					<table class="table">
						<thead>
							<tr>
								<th>Nomor</th>
								<th>Indikator</th>
								<th>Pertanyaan</th>
								<th>Jawaban</th>
								<th>Skor</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>
									<div class="form-group">
										<select class="form-control">
										  <option>1</option>
										  <option>2</option>
										</select>
									</div>
								</td>
								<td>
									Pasangan 1
									<textarea class="form-control" rows="5" cols="30"  id="materi" placeholder="Pasangan 1"></textarea>
									Pasangan 2
									<textarea class="form-control" rows="5" cols="30"  id="materi" placeholder="Pasangan 2"></textarea>
								</td>
								<td>
									<textarea class="form-control" rows="5" cols="20"  id="materi" placeholder="Jawaban"></textarea>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Skor">
								</td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-primary btn-sm">Tambah Soal</button>
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="skorTotalMenjodohkan" class="col-sm-3 control-label">Skor Total</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="skorTotalMenjodohkan" placeholder="0-100">
							</div>
						</div>
					</form>
				</div>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="skorTotalInstrumen" class="col-sm-3 control-label">Skor Total Instrumen</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="skorTotalInstrumen" placeholder="0-100">
						</div>
					</div>
				</form>
			</div>
		</div>
		<button type="button" class="btn btn-primary">Simpan Draft</button>
		<button type="button" class="btn btn-primary">Simpan Final</button>
      </div>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>
  

</body></html>