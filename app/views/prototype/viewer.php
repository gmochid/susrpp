<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>RPP Editor</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo asset('css/viewer.css')?>" rel="stylesheet">

  </head>

  <body style="">

    <!-- Static navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.html">RPP Editor</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="dashboard.html">RPP</a></li>
            <li><a href="search.html">Pencarian</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
			<li>
				<a href="home.html">Keluar</a>
			</li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>


    <div class="container">
      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Rencana Pelaksaan Pengajaran</h1>
		<table class="table">
        <tbody>
			<tr>
				<th>Identitas</th>
			</tr>
			<tr>
				<td>Nama Sekolah: </td>
				<td>SD Negeri 1 Bandung</td>
			</tr>
			<tr>
				<td>Kelas/Semester: </td>
				<td>VI/I</td>
			</tr>
			<tr>
				<td>Tema Umum: </td>
				<td></td>
			</tr>
			<tr>
				<td>Tema Khusus: </td>
				<td></td>
			</tr>
			<tr>
				<td>Alokasi Waktu: </td>
				<td>120 menit</td>
			</tr>
			<tr>
				<td>Pelajaran terkait</td>
				<td></td>
			</tr>
			<tr>
				<th>Detil Kompetensi Pelajaran</th>
			</tr>
			<tr>
				<td>A. Standar Kompetensi: </td>
				<td>Memahami  hubungan antara struktur organ tubuh manusia dengan fungsinya, serta pemeliharaannya
				</td>
			</tr>
			<tr>
				<td>B. Kompetensi Dasar: </td>
				<td>1. Mendeskripsikan hubungan antara struktur kerangka tubuh manusia dengan fungsinya<br>
					2. Menerapkan cara memelihara kesehatan kerangka tubuh
			</tr>
			<tr>
				<td>C. Indikator Pembelajaran: </td>
				<td>Indikator</td>
			</tr>
			<tr>
				<td>D. Tujuan Pembelajaran: </td>
				<td>Tujuan</td>
			</tr>
			<tr>
				<td>Materi dan Metode Pelajaran</td>
			</tr>
			<tr>
				<td>E. Materi Pembelajaran: </td>
				<td>Materi</td>
			</tr>
			<tr>
				<td>F. Metode Pembelajaran: </td>
				<td>Diskusi</option>
				</td>
			</tr>
			<tr>
				<th>Langkah Pembelajaran</th>
			</tr>
			<tr>
				<td>Kegiatan Awal</td>
				<td>
					Langkah 1<br>
					Aksi Guru<br>
					Respon siswa<br>
					Jika respon siswa tidak sesuai
				</td>
			</tr>
			<tr>
				<td>Kegiatan Inti</td>
				<td>
					Langkah 1<br>
					Aksi Guru<br>
					Respon siswa<br>
					Jika respon siswa tidak sesuai
				</td>
			</tr>
			<tr>
				<td>Kegiatan Penutup</td>
				<td>
					Langkah 1<br>
					Aksi Guru<br>
					Respon siswa<br>
					Jika respon siswa tidak sesuai
				</td>
			</tr>
			<tr>
				<th>Alat dan Sumber Pembelajaran </th>
			</tr>
			<tr>
				<td>Alat</td>
				<td></td>
			</tr>
			<tr>
				<td>Sumber Belajar</td>
				<td></tr>
			<tr>
				<th>Penilaian </th>
			</tr>
			<tr>
				<td>Teknik </td>
				<td></tr>
			<tr>
				<td>Bentuk </td>
				<td></td>
			</tr>
			<tr>
				<td>Instrumen</td>
				<td></td>
			</tr>
        </tbody>
      </table>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>
  

</body></html>