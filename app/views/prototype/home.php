<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>RPP Editor</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo asset('css/home.css')?>" rel="stylesheet">

  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">RPP Editor</a>
        </div>
        <div class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" role="form">
            <div class="form-group">
              <input type="text" placeholder="Username" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
			<a href="dashboard.html" class="btn btn-success btn-sm btn-primary btn-lg active" role="button">Masuk</a>
            <!-- <button type="" class="btn btn-success" href="dashboard.html">Masuk</button> -->
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Selamat datang!</h1>
        <p>RPP Editor adalah sebuah website pengelola Rencana Pelaksanaan Pengajaran. Pada website ini Anda dapat membuat RPP sendiri atau melihat RPP yang dibuat oleh orang lain.</p>
        <p><a class="btn btn-primary btn-lg" role="button">Lebih lanjut »</a></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        
      </div>

      <hr>

      <footer>
        <p>© Fast-Track Group 2013 feat YAW</p>
      </footer>
    </div> <!-- /container -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>
  

</body></html>