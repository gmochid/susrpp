<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>RPP Editor</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo asset('css/search.css')?>" rel="stylesheet">

  </head>

  <body style="">

    <!-- Static navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.html">RPP Editor</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="dashboard.html">RPP</a></li>
            <li class="active"><a href="search.html">Pencarian</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
			<li>
				<a href="home.html">Keluar</a>
			</li>
          </ul>
        </div><!--/.nav-collapse -->
		
      </div>
    </div>


    <div class="container">
      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Pencarian RPP</h1>
        <p>Silahkan isi field dibawah untuk mencari RPP yang Anda inginkan.</p>
		
		<form class="form-horizontal" role="form">
			<div class="form-group">
				<label for="penulis" class="col-sm-3 control-label">Penulis</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="penulis" placeholder="Nama Penulis">
				</div>
			</div>
			<div class="form-group">
				<label for="namaSekolah" class="col-sm-3 control-label">Nama Sekolah</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="namaSekolah" placeholder="Nama Sekolah">
				</div>
			</div>
			<div class="form-group">
				<label for="mataPelajaran" class="col-sm-3 control-label">Mata Pelajaran</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="mataPelajaran" placeholder="Mata Pelajaran">
				</div>
			</div>
			<div class="form-group">
				<label for="kelas" class="col-sm-3 control-label">Kelas</label>
				<div class="col-sm-1">
					<select class="form-control" id="kelas">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
					</select>
				</div>
				<label for="semester" class="col-sm-2 control-label">Semester</label>
				<div class="col-sm-1">
					<select class="form-control" id="semester">
						<option>I</option>
						<option>II</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="mataPelajaran" class="col-sm-3 control-label">Tema</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="tema" placeholder="Tema">
				</div>
			</div>
		</form>
		<a href="searchresult.html" class="btn btn-primary btn-lg active" role="button">Cari</a>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>
  

</body></html>