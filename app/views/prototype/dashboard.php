<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>RPP Editor</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo asset('css/dashboard.css')?>" rel="stylesheet">

  </head>

  <body style="">

    <!-- Static navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.html">RPP Editor</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="dashboard.html">RPP</a></li>
            <li><a href="search.html">Pencarian</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
			<li>
				<a href="home.html">Keluar</a>
			</li>
          </ul>
        </div><!--/.nav-collapse -->
		
      </div>
    </div>


    <div class="container">
      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Daftar RPP</h1>
        <p>Berikut adalah daftar RPP yang Anda miliki.</p>
		<table class="table">
        <thead>
          <tr>
            <th>Mata Pelajaran</th>
            <th>Kelas/Semester</th>
            <th>Tema</th>
			<th>Tanggal</th>
			<th>Status</th>
            <th>Lihat/Ubah</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>IPA</td>
            <td>VI/1</td>
            <td>Makhluk Hidup dan Proses Kehidupan</td>
			<td>03/12/2013</td>
			<td>Final</td>
            <td>
				<a href="viewer.html" class="btn btn-primary btn-sm btn-lg active" role="button">Lihat</a>
			</td>
          </tr>
          <tr>
            <td>IPA</td>
            <td>VI/1</td>
            <td>Benda dan Sifatnya</td>
			<td>03/12/2013</td>
			<td>Draft</td>
            <td>
				<a href="viewer.html" class="btn btn-primary btn-sm btn-lg active" role="button">Lihat</a>
				<a href="editor.html" class="btn btn-primary btn-sm btn-lg active" role="button">Ubah</a>
			</td>
          </tr>
        </tbody>
      </table>
	  <a href="editor.html" class="btn btn-primary btn-lg active" role="button">Buat baru</a>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>

</body></html>