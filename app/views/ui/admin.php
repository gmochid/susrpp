<!DOCTYPE html>
<html ng-app>

<head>
  <meta charset="utf-8">
  <title>Admin</title>

  <script type="text/javascript">(function(){window.RootLocation="<?php echo url('/'); ?>";})();</script>
  <script type="text/javascript" src="<?php echo asset('lib/js/jquery-1.10.2.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('js/api.js'); ?>"></script>

  <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">
  <link href="<?php echo asset('css/dashboard.css')?>" rel="stylesheet">
  <script type="text/javascript" src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>
</head>

<body>

<!-- Static Navbar -->
<div class="navbar navbar-default" role="navigation">
  <div class="container">

  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
      data-target=".navbar-collapse">

      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <a class="navbar-brand" href="<?php echo url('/'); ?>">RPP Editor</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      <li class="active"><a href="<?php echo url('/admin'); ?>">Admin</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a id="nameLink" href="/admin">Foo</a></li>
      <li><a href="<?php echo url('/logout'); ?>">Keluar</a></li>
    </ul>
  </div>

  </div>
</div>

<!-- Admin -->
<div class="container" ng-controller="UserList">

  <div class="row">
    <div class="col-md-offset-1 col-md-10">

    <h1>Daftar User</h1>

    <div class="row">
      <div class="col-md-12">
        <a id="openFormButton" class="btn btn-default btn-info btn-sm pull-right">Buat User Baru</a>
        <span class="pull-right">&nbsp;&nbsp;&nbsp;</span>
        <a id="openFormButton2" class="btn btn-default btn-info btn-sm pull-right">Buat Grup Baru</a>
      </div>
    </div>

    <form id="newuser-form" ng-submit="registerUser()">
      <div>
        <a class="btn btn-default btn-xs pull-right" id="closeFormButton" href="">Tutup Form</a>
        <div class="clearfix"></div>
      </div>

      <div class="form-group">
        <label for="username">Username:</label>
        <input id="username" class="form-control" ng-model="newUser.username" type="text"
          placeholder="Masukkan username" required/>
      </div>

      <div class="form-group">
        <label for="name">Nama Lengkap:</label>
        <input id="name" class="form-control" ng-model="newUser.name" type="text"
          placeholder="Masukkan nama lengkap" required/>
      </div>

      <div class="form-group">
        <label for="password">Password:</label>
        <input id="password" class="form-control" ng-model="newUser.password" type="password"
          placeholder="Masukkan password" required/>
      </div>

      <div class="form-group">
        <label for="email">Email:</label>
        <input id="email" class="form-control" ng-model="newUser.email" type="email"
          placeholder="e.g. foo@example.org" required/>
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-primary">Daftarkan!</button>
      </div>
    </form>

    <form id="newgroup-form" ng-submit="registerGroup()">
      <div>
        <a class="btn btn-default btn-xs pull-right" id="closeFormButton2" href="">Tutup Form</a>
        <div class="clearfix"></div>
      </div>

      <div class="form-group">
        <label for="groupname">Nama Grup:</label>
        <input id="groupname" class="form-control" ng-model="newGroup.groupName" type="text"
          placeholder="Masukkan nama grup yang akan dibuat" required/>
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-primary">Buat Grup</button>
      </div>
    </form>

    <div><table class="table table-striped">

      <thead>
        <tr>
          <th>Username</th>
          <th>Nama</th>
          <th>Email</th>
          <th>Group</th>
          <th>Status</th>
          <th></th>
        </tr>
      </thead>

      <tbody>
        <tr ng-repeat="user in userList">
          <td>{{user.username}}</td>
          <td>{{user.name}}</td>
          <td>{{user.email}}</td>
          <td>{{user.groupName}}</td>
          <td>
            <span ng-if="user.status == 1">Approved</span>
            <span ng-if="user.status == 0">Unapproved</span>
          </td>
          <td>
            <div>
              <a class="btn btn-default btn-xs" ng-click="approveUser($index)"
                ng-if="user.status == 0">Approve</a>
              <a class="btn btn-default btn-xs" ng-click="disapproveUser($index)"
                ng-if="user.status == 1">Disapprove</a>
              <a class="btn btn-danger btn-xs" ng-click="removeUser($index)">Hapus</a>
              <a class="btn btn-warning btn-xs">Pindah Grup</a>
            </div>
          </td>
        </tr>
      </tbody>

    </table></div>

    </div>
  </div>

</div><!-- end Admin -->

<!-- scripts -->
<script type="text/javascript" src="<?php echo asset('lib/js/angular.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('js/ui-admin.js'); ?>"></script>

</body>

</html>
