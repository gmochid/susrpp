<!DOCTYPE html>
<html ng-app>

<head>
  <meta charset="utf-8">
  <title>RPP Editor - Dashboard</title>

  <script type="text/javascript">(function(){window.RootLocation="<?php echo url('/'); ?>";})();</script>
  <script type="text/javascript" src="<?php echo asset('lib/js/jquery-1.10.2.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('js/api.js'); ?>"></script>

  <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">
  <link href="<?php echo asset('css/dashboard.css')?>" rel="stylesheet">
  <script type="text/javascript" src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>
</head>

<body>

<!-- Static Navbar -->
<?php echo \View::make('ui/editor/navbar', array(
    'action' => 'dashboardRpp',
)); ?>

<div class="container" ng-controller="RppList">
  <h2 class="text-center">Dashboard RPP</h2>

  <div class="row">

    <div class="col-sm-2">
      <!-- left -->
      <div class="row">
        <b>Toolbox</b>
      </div>
      <hr>
      
      <ul class="nav nav-stacked">
        <li><a href="javascript:;"><i class="glyphicon"></i> Alerts</a></li>
        <li><a href="javascript:;"><i class="glyphicon"></i> Links</a></li>
        <li><a href="javascript:;"><i class="glyphicon"></i> Reports</a></li>
        <li><a href="javascript:;"><i class="glyphicon"></i> Books</a></li>
        <li><a href="javascript:;"><i class="glyphicon"></i> Tools</a></li>
        <li><a href="javascript:;"><i class="glyphicon"></i> Real-time</a></li>
        <li><a href="javascript:;"><i class="glyphicon"></i> Advanced..</a></li>
      </ul>
      
      <hr>
    </div><!-- /span-3 -->

    <div class="col-md-7">


    <!-- RppList -->
    <div class="row">
      <div class="col-md 12">
        <b>Daftar RPP</b>
      </div>
    </div>

    <div>
      <hr>
      <div ng-repeat="rpp in rppList">
        <div class="row">
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-4">
                <div><b>Tema </b></div>
              </div>
              <div class="col-md-5">
                <div>{{rpp.data.topic}}</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div><b>Sub-Tema </b></div>
              </div>
              <div class="col-md-5">
                <div>{{rpp.data.theme}}</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div><b>Kelas </b></div>
              </div>
              <div class="col-md-5">
                <div>{{rpp.data.class}}</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div><b>Semester </b></div>
              </div>
              <div class="col-md-5">
                <div>{{rpp.data.term}}</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div><b>Update Terakhir </b></div>
              </div>
              <div class="col-md-5">
                <div>{{rpp.lastUpdated}}</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div><b>Status </b></div>
              </div>
              <div class="col-md-5">
                <div>
                  <span ng-if="rpp.status == 'draft'">Draft</span>
                  <span ng-if="rpp.status == 'published'">Final</span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div><b>Pembuat </b></div>
              </div>
              <div class="col-md-5">
                <div ng-repeat="author in rpp.authors">
                  <a href="#">{{author}}</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-1">
            <div>
              <div>
                <a class="btn btn-default btn-xs" ng-href="/editor?action=view&amp;id={{rpp._id}}">Lihat</a>
              </div>
              <div>
                <a class="btn btn-default btn-xs" ng-href="/view/{{rpp._id}}">Print</a>
              </div>
              <div ng-if="rpp.status == 'draft'">
                <a class="btn btn-default btn-xs" ng-href="/editor?action=edit&amp;id={{rpp._id}}">Ubah</a>
              </div>
              <div ng-if="rpp.status == 'draft'">
                <a class="btn btn-default btn-xs btn-success" ng-click="finalizeRpp(rpp._id, rpp.data.theme)">Finalisasi</a>
              </div>
              <div ng-if="rpp.status == 'draft'">
                <a class="btn btn-default btn-xs btn-danger" ng-click="deleteRpp(rpp._id, rpp.data.theme)">Hapus</a>
              </div>
            </div>
          </div>
        </div>
        <hr>
      </div>
      <table class="table table-striped">
      <thead>
        <tr>
          <th>Tema</th>
          <th>Sub Tema</th>
          <th>Kelas</th>
          <th>Semester</th>
          <th>Pembuat</th>
          <th>Status</th>
          <th>Last Updated</th>
          <th class="action-buttons"></th>
        </tr>
      </thead>

      <tbody>
        <tr ng-repeat="rpp in rppList">
          <td>{{rpp.data.topic}}</td>
          <td>{{rpp.data.theme}}</td>
          <td>{{rpp.data.class}}</td>
          <td>{{rpp.data.term}}</td>
          <td>
            <div ng-repeat="author in rpp.authors">
              {{author}}
            </div>
          </td>
          <td>
            <span ng-if="rpp.status == 'draft'">Draft</span>
            <span ng-if="rpp.status == 'published'">Final</span>
          </td>
          <td>{{rpp.lastUpdated}}</td>
          <td class="action-buttons">
            <div>
              <div>
                <a class="btn btn-default btn-xs" ng-href="/editor?action=view&amp;id={{rpp._id}}">Lihat</a>
              </div>
              <div>
                <a class="btn btn-default btn-xs" ng-href="/view/{{rpp._id}}">Print</a>
              </div>
              <div ng-if="rpp.status == 'draft'">
                <a class="btn btn-default btn-xs" ng-href="/editor?action=edit&amp;id={{rpp._id}}">Ubah</a>
              </div>
              <div ng-if="rpp.status == 'draft'">
                <a class="btn btn-default btn-xs btn-success" ng-click="finalizeRpp(rpp._id, rpp.data.theme)">Finalisasi</a>
              </div>
              <div ng-if="rpp.status == 'draft'">
                <a class="btn btn-default btn-xs btn-danger" ng-click="deleteRpp(rpp._id, rpp.data.theme)">Hapus</a>
              </div>
            </div>
          </td>
        </tr>
      </tbody>

    </table>
    </div>

    </div>

    <div class="col-md-3">
      <div class="row">
        <b>Aktivitas Terakhir</b>
      </div>

      <div class="list-group">
        <div class="list-group-item row">
          <div class="col-md-3">
          [Gambar]
          </div>
          <div class="col-md-9">
            <b>[Nama user]</b>
            <div>
              Membuat RPP [linkRPP]
            </div>
            <div>
              [timestamp]
            </div>
          </div>
        </div>
        <div class="list-group-item row">
          <div class="col-md-3">
          [Gambar]
          </div>
          <div class="col-md-9">
            <b>[Nama user]</b>
            <div>
              Mengedit RPP [linkRPP]
            </div>
            <div>
              [timestamp]
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div><!-- end RppList -->

<!-- scripts -->
<script type="text/javascript" src="<?php echo asset('lib/js/angular.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('js/ui-dashboard.js'); ?>"></script>

</body>

</html>
