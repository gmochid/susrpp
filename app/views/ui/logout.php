<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Keluar</title>

    <script type="text/javascript" src="<?php echo asset('lib/js/jquery-1.10.2.min.js'); ?>"></script>
</head>

<body>

<form id="logoutForm" method="get" action="<?php echo url('/api/user/session/forget'); ?>">
    <h2>Mengeluarkan...</h2>
</form>

<!-- scripts -->
<script type="text/javascript" src="<?php echo asset('js/ui-logout.js'); ?>"></script>

</body>

</html>
