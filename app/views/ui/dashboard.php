<!DOCTYPE html>
<html ng-app>

<head>
  <meta charset="utf-8">
  <title>RPP Editor - Dashboard</title>

  <script type="text/javascript">(function(){window.RootLocation="<?php echo url('/'); ?>";})();</script>
  <script type="text/javascript" src="<?php echo asset('lib/js/jquery-1.10.2.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('js/api.js'); ?>"></script>

  <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">
  <link href="<?php echo asset('css/dashboard.css')?>" rel="stylesheet">
  <script type="text/javascript" src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>
</head>

<body>

<!-- Static Navbar -->
<?php echo \View::make('ui/editor/navbar', array(
    'action' => 'dashboard',
)); ?>

<div class="container">

<div class="row">
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
      <img data-src="holder.js/300x200" alt="Logo RPP Editor">
      <div class="caption">
        <h3>RPP Editor</h3>
        <p>RPP Editor adalah sebuah aplikasi pengelola Rencana Pelaksanaan Pengajaran. Pada aplikasi ini Anda dapat membuat RPP, mengedit RPP atau melihat RPP yang dibuat oleh orang lain.</p>
        <p><a href="/rpp" class="btn btn-primary" role="button">Masuk RPP Editor</a></p>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
      <img data-src="holder.js/300x200" alt="Logo Teachingware Editor">
      <div class="caption">
        <h3>Teachingware Editor</h3>
        <p>...</p>
        <p><a href="#" class="btn btn-primary" role="button">Masuk Teachingware Editor</a></p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="row">
      <b>Aktivitas Terakhir Global</b>
    </div>

    <div class="list-group">
      <div class="list-group-item row">
        <div class="col-md-3">
        [Gambar]
        </div>
        <div class="col-md-9">
          <b>[Nama user]</b>
          <div>
            Membuat RPP [linkRPP]
          </div>
          <div>
            [timestamp]
          </div>
        </div>
      </div>
      <div class="list-group-item row">
        <div class="col-md-3">
        [Gambar]
        </div>
        <div class="col-md-9">
          <b>[Nama user]</b>
          <div>
            Mengedit RPP [linkRPP]
          </div>
          <div>
            [timestamp]
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>

<!-- scripts -->
<script type="text/javascript" src="<?php echo asset('lib/js/angular.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('js/ui-dashboard.js'); ?>"></script>
</body>

</html>
