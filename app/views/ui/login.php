<!DOCTYPE html>
<html ng-app>

<head>
  <meta charset="utf-8">
  <title>RPP Editor - Masuk</title>

  <script type="text/javascript">(function(){window.RootLocation="<?php echo url('/'); ?>";})();</script>
  <script type="text/javascript" src="<?php echo asset('lib/js/jquery-1.10.2.min.js'); ?>"></script>

  <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">
  <script type="text/javascript" src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>

<style type="text/css">
@import url('/css/font.css');

body { font-family: Ubuntu; }
.alert { margin: 0; padding: 8px; }
#message-box { display: none; }
#bottom-info { margin: 20px 0 0 0; font-size: 0.9em; }
</style>

</head>

<body>

<!-- Static Navbar -->
<div class="navbar navbar-default" role="navigation">
  <div class="container">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse"
        data-target=".navbar-collapse">

        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="<?php echo url('/'); ?>">RPP Editor</a>
    </div>

  </div>
</div>

<!-- Login Form -->
<div class="container">
  <div class="row">
    <div class="col-md-offset-2 col-md-8">

      <form id="login-form" role="form" class="form-horizontal" method="post"
        action="<?php echo url('/api/user/session/auth'); ?>">

        <div class="text-center">
          <h2>Silahkan masuk terlebih dahulu</h2>
          <br/><br/>
        </div>

        <div class="form-group">
          <label for="username" class="col-md-5 control-label">Username</label>
          <div class="col-md-5">
            <input type="text" placeholder="Username" class="form-control" name="username">
          </div>
        </div>

        <div class="form-group">
          <label for="username" class="col-md-5 control-label">Password</label>
          <div class="col-md-5">
            <input type="password" placeholder="Password" class="form-control" name="password">
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-offset-5 col-md-5">
            <button class="btn btn-primary" type="submit">Masuk</button>
            <div class="pull-right">
            Ingin mendaftar? <a href="/register">Klik disini</a>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-offset-2 col-md-10">
            <div id="message-box"></div>
            <?php if (\Session::has('register-success')): ?>
                <div class="alert alert-success">
                    <?php echo \Session::get('register-success'); ?>
                </div>
            <?php endif; ?>
          </div>
        </div>
      </form>

    </div>
  </div>

  <div class="row">
    <div class="col-md-offset-2 col-md-8">
      <div id="bottom-info" class="text-justify">
        <p>RPP Editor adalah sebuah aplikasi pengelola Rencana Pelaksanaan Pengajaran.
        Pada aplikasi ini Anda dapat membuat RPP sendiri atau melihat RPP yang
        dibuat oleh orang lain.</p>
      </div>
    </div>
  </div>
</div>


<!-- scripts -->
<script type="text/javascript" src="<?php echo asset('js/ui-login.js'); ?>"></script>

</body>

</html>
