<!-- [[editor:rpp-description]] -->
<div id="part-description" class="item-part active-item-part">

<div class="item-box-header">
  <h3>Deskripsi RPP</h3>
</div>

<!-- begin item-box --><div id="item-description-box" class="item-box">

<!--  Author -->
<div class="form-group">
  <label for="itemAuthor">Penulis:</label>
  <ul class="list-group">
    <li class="list-group-item" ng-repeat="author in authors">
      {{author.name}}
      <a class="btn btn-info btn-xs" href="" ng-click="removeAuthor(author.username)" ng-show="!view">Hapus</a>
    </li>
    <li class="list-group-item" ng-hide="authors.length">Belum ada penulis</li>
  </ul>

  <div class="form-inline" ng-show="!view">
    <div class="form-group">
      <select id="itemAuthor" class="form-control"
        ng-model="authorOption"
        ng-options="user.name for user in users"></select>
    </div>

    <div class="form-group">
      <button class="btn btn-default" ng-click="addAuthor()">Tambahkan</button>
    </div>
  </div>
</div>

<!-- Description -->
<div class="form-group">
  <label for="itemDescription">Deskripsi:</label>
  <input id="itemDescription" class="form-control"
    type="text" ng-model="item.description" placeholder="Masukkan Deskripsi" ng-show="!view"/>
  <div id="itemDescriptionView" class="form-control" ng-show="view">
    {{item.description}}
  </div>
</div>

<!-- Tags -->
<div class="form-group">

  <label for="itemNewTag">Tag:</label>

  <ul class="tag-list">
    <li class="tag-list-item" ng-repeat="tag in item.tags">
      {{tag}}
      <a href="" class="tag-remove-button glyphicon glyphicon-remove"
        ng-click="removeTag($index)" ng-show="!view"></a>
    </li>
    <li class="tag-list-item" ng-hide="item.tags.length">-</li>
    <li class="clearfix">
  </ul>

  <form class="form-inline" ng-submit="addTag()" ng-show="!view">
    <div class="form-group">
      <input id="itemNewTag" class="form-control input-sm" type="text"
        placeholder="Masukkan Nama Tag" />
    </div>

    <div class="form-group">
      <button class="btn btn-default btn-sm" ng-click="addTag()">Tambahkan</button>
    </div>
  </form>

</div>

<!-- end item-box --></div>

</div>
<!-- [[/editor:rpp-description]] -->
