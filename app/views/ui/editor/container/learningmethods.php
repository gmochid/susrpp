<!-- [[editor:learning-methods]] -->
<div id="part-learning-methods" class="item-part">

<div class="item-box-header">
  <h3>Model, Metode dan Pendekatan Pembelajaran</h3>
</div>

<!-- begin item box --><div id="item-learning-methods-box" class="item-box">

<div class="form-group">
  <label for="learnModels">Model Pembelajaran yang digunakan:</label>

  <div class="checkbox" ng-repeat="model in learnModels">
    <label>
      <input type="checkbox" ng-model="model.checked" ng-show="!view">
      <input type="checkbox" ng-model="model.checked" ng-show="view" disabled="disabled">
      {{model.name}}
    </label>
  </div>
</div>

<div class="form-group">
  <label for="learnApproach">Pendekatan yang digunakan:</label>

  <div class="checkbox" ng-repeat="approach in learnApproaches">
    <label>
      <input type="checkbox" ng-model="approach.checked" ng-show="!view">
      <input type="checkbox" ng-model="approach.checked" ng-show="view" disabled="disabled">
      {{approach.name}}
    </label>
  </div>
</div>

<div class="form-group">
  <label for="learnMethods">Metode yang digunakan:</label>

  <div class="checkbox" ng-repeat="method in learnMethods">
    <label>
      <input type="checkbox" ng-model="method.checked" ng-show="!view">
      <input type="checkbox" ng-model="method.checked" ng-show="view" disabled="disabled">
      {{method.name}}
    </label>
  </div>
</div>

<!-- end item box --></div>

</div>
<!-- [[/editor:learning-methods]] -->
