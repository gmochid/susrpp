<!-- [[editor:rpp-identity]] -->
<div id="part-identity" class="item-part">

<div class="item-box-header">
  <h3>Identitas RPP</h3>
</div>

<!-- begin item-box --><div id="item-identity-box" class="item-box">

<!-- School Name -->
<div class="form-group">
  <label for="dataSchoolName">Nama Sekolah:</label>
  <input id="dataSchoolName" class="form-control"
    type="text" ng-model="item.data.school" placeholder="Masukkan Nama Sekolah" ng-show="!view"/>
  <div id="dataSchoolNameView" class="form-control" ng-show="view">
    {{item.data.school}}
  </div>
</div>

<!-- Topic -->
<div class="form-group">
  <label for="dataTopic">Tema:</label>
  <input id="dataTopic" class="form-control"
    type="text" ng-model="item.data.topic" placeholder="Masukkan Tema" ng-show="!view"/>
  <div id="dataTopicView" class="form-control" ng-show="view">
    {{item.data.topic}}
  </div>
</div>

<!-- Tema -->
<div class="form-group">
  <label for="dataTheme">Sub Tema:</label>
  <input id="dataTheme" class="form-control"
    type="text" ng-model="item.data.theme" placeholder="Masukkan Sub Tema" ng-show="!view"/>
  <div id="dataThemeView" class="form-control" ng-show="view">
    {{item.data.theme}}
  </div>
</div>

<!-- Kelas -->
<div class="form-group">
  <label for="dataKelas">Kelas:</label>
  <select id="dataKelas" class="form-control" ng-model="item.data.class" ng-show="!view">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
  </select>
  <div id="dataKelasView" class="form-control" ng-show="view">
    {{item.data.class}}
  </div>
</div>

<!-- Semester -->
<div class="form-group">
  <label for="dataSemester">Semester:</label>
  <select id="dataSemester" class="form-control" ng-model="item.data.term" ng-show="!view">
    <option value="1">1</option>
    <option value="2">2</option>
  </select>
  <div id="dataSemesterView" class="form-control" ng-show="view">
    {{item.data.term}}
  </div>
</div>

<!-- end item-box --></div>

</div>
<!-- [[/editor:rpp-identity]] -->
