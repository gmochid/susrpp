<!-- [[editor:rpp-activities]] -->
<div id="part-activities" class="item-part">

<div class="item-box-header">
  <h3>Aktivitas Pembelajaran</h3>
</div>

<!-- begin item box --><div id="item-activity-box" class="item-box">

<div class="form-group">
  <label>Alokasi waktu aktivitas pembuka:</label>
  <div class="input-group x-width-110px">

    <input type="text" class="form-control" placeholder="..."
      ng-model="item.data.activities.startDuration" ng-show="!view">
    <input type="text" class="form-control" placeholder="..."
      ng-model="item.data.activities.startDuration" readonly ng-show="view">
    <span class="input-group-addon">Menit</span>
  </div>
</div>

<div class="form-group">
  <label>Alokasi waktu aktivitas inti:</label>
  <div class="input-group x-width-110px">

    <input type="text" class="form-control" placeholder="..."
      ng-model="item.data.activities.coreDuration" ng-show="!view">
    <input type="text" class="form-control" placeholder="..."
      ng-model="item.data.activities.coreDuration" readonly ng-show="view">
    <span class="input-group-addon">Menit</span>
  </div>
</div>

<div class="form-group">
  <label>Alokasi waktu aktivitas penutup:</label>
  <div class="input-group x-width-110px">

    <input type="text" class="form-control" placeholder="..."
      ng-model="item.data.activities.closingDuration" ng-show="!view">
    <input type="text" class="form-control" placeholder="..."
      ng-model="item.data.activities.closingDuration" readonly ng-show="view">
    <span class="input-group-addon">Menit</span>
  </div>
</div>

<div class="form-group">
  <strong>Total Durasi Aktivitas: {{sumDuration()}} Menit</strong><br>
  (Maksimal 70 menit)
</div>

<div class="form-group">
  <label>Aktivitas Pembuka:</label>

  <textarea class="form-control wysiwyg-editor" ng-show="!view" id="itemStartActivity">
    {{item.data.activities.startActivity}}
  </textarea>
  <div class="form-control wysiwyg-viewer" ng-show="view" id="itemStartActivityView">
  </div>
</div>

<div class="form-group">
  <label>Aktivitas Inti:</label>

  <textarea class="form-control wysiwyg-editor" ng-show="!view" id="itemCoreActivity">
    {{item.data.activities.coreActivity}}
  </textarea>
  <div class="form-control wysiwyg-viewer" ng-show="view" id="itemCoreActivityView">
  </div>
</div>

<div class="form-group">
  <label>Aktivitas Penutup:</label>

  <textarea class="form-control wysiwyg-editor" ng-show="!view" id="itemClosingActivity">
    {{item.data.activities.closingActivity}}
  </textarea>
  <div class="form-control wysiwyg-viewer" ng-show="view" id="itemClosingActivityView">
  </div>
</div>

<!-- end item box --></div>

</div>
<!-- [[/editor:rpp-activities]] -->
