 <!-- [[editor:rpp-related-subjects]] -->
<div id="part-related-subjects" class="item-part">

<div class="item-box-header">
  <h3>Mata Pelajaran Terkait</h3>
</div>

<!-- begin item-box --><div id="item-related-subjects-box" class="item-box">

<!-- Empty subject list -->
<div class="well well-sm" ng-show="item.data.relatedSubjects.length == 0">
  Belum ada mata pelajaran terkait
</div>

<!-- Add subject button -->
<div ng-if="!view">
  <div class="form-group">
    <button class="btn btn-primary" ng-click="prepareAddRelatedSubject()">Tambah Mata Pelajaran Terkait</button>
  </div>
</div>

<!-- [subject-list] -->
<div class="subjectList" ng-repeat="relatedSubject in item.data.relatedSubjects">
  <!-- begin panel --><div class="panel panel-default">

    <div class="panel-heading">
      <h3 class="panel-title">
        <strong>{{relatedSubject.subjectName}}</strong>
        <button class="btn btn-danger pull-right btn-xs" ng-show="!view"
          ng-click="removeRelatedSubject($index)">Hapus</button>
        <button class="btn btn-warning pull-right btn-xs" ng-show="!view"
          ng-click="prepareUpdateRelatedSubject($index)">Ubah</button>
      </h3>
    </div>

    <div class="panel-body">
      <h4>Kompetensi:</h4>

      <div class="panel panel-default">
        <div class="panel-heading">{{relatedSubject.coreCompetence}}</div>

        <ul class="list-group">
          <li class="list-group-item"
            ng-repeat="basicCompetence in relatedSubject.basicCompetence">
            {{basicCompetence}}
          </li>
        </ul>
      </div>

      <h4>Indikator:</h4>
      <ul class="list-group">
        <li class="list-group-item"
          ng-repeat="indicator in relatedSubject.indicator">
          {{$index + 1}}. {{indicator}}
        </li>
      </ul>
    </div>

  <!-- end panel --></div>
</div>
<!-- [/subject-list] -->

<!-- Add subjects -->
<a href="#x" class="overlay" id="item-related-subject-popup"></a>
<!-- begin panel 1 --><div class="panel panel-default popup" ng-show="!view">

  <div class="panel-heading" ng-show="updateRelatedSubjectIndex == null">
    <h3 class="panel-title">Tambah Mata Pelajaran Terkait</h3>
  </div>

  <div class="panel-heading" ng-hide="updateRelatedSubjectIndex == null">
    <h3 class="panel-title">Ubah Mata Pelajaran Terkait</h3>
  </div>

  <!-- begin panel 2 --><div class="panel-body">

    <!-- Subject -->
    <div class="form-group" ng-show="updateRelatedSubjectIndex == null">
      <select id="dataRelatedSubject" class="form-control"
          ng-model="subjectOption"
          ng-options="subject.subjectName for subject in subjects">
        <option value="" ng-selected="resetSubjectOption()">-- Pilih Mata Pelajaran --</option>
      </select>
    </div>

    <div class="form-group" ng-hide="updateRelatedSubjectIndex == null">
      <div id="dataRelatedSubject" class="form-control">
        {{subjectOption.subjectName}}
      </div>
    </div>

    <!-- Core competence -->
    <div class="form-group">
      <select id="coreCompetence" class="form-control"
          ng-model="coreCompetenceOption"
          ng-options="coreCompetence.fulldesc for coreCompetence in subjectOption.coreCompetences">
        <option value="">-- Pilih Kompetensi Inti --</option>
      </select>
    </div>

    <!-- Basic competence -->
    <div class="form-group" ng-show="coreCompetenceOption != null">
      <label>Kompetensi Inti:</label>
      <div><p>{{coreCompetenceOption.fulldesc}}</p></div>
      <label>Kompetensi Dasar:</label>
      <ul class="list-group">
        <li class="list-group-item" ng-repeat="basicCompetence in coreCompetenceOption.basicCompetences">
          <label style="font-weight: normal">
            <input type="checkbox" ng-model="basicCompetence.selected"> &nbsp; {{basicCompetence.fulldesc}}
          </label>
        </li>
      </ul>
    </div>

    <!-- Indicator -->
    <div class="form-group" ng-show="relatedSubjectOption != null">
      <label>Indikator:</label>
      <ul class="list-group">
        <li class="list-group-item"
          ng-repeat="indicator in relatedSubjectOption.indicator">
          {{$index + 1}}. {{indicator}}
          <a class="btn btn-info btn-xs" href=""
            ng-click="removeRelatedSubjectIndicator($index)">Hapus</a>
        </li>
        <li class="list-group-item" ng-hide="relatedSubjectOption.indicator.length">Belum ada</li>
      </ul>

      <form class="input-group x-width-520px" ng-submit="addRelatedSubjectIndicator()">
        <input id="newIndicator" type="text" class="form-control"
          placeholder="Masukkan Indikator">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"
            ng-click="addRelatedSubjectIndicator()">Tambahkan Indikator</button>
        </span>
      </form>
    </div>

    <div class="form-group">
      <button class="btn btn-default" ng-click="addRelatedSubject()">Simpan Mata Pelajaran Terkait</button>
    </div>

    <a class="close" href="#close"></a>

  <!-- end panel 2 --></div>

<!-- end panel 1 --></div>

<!-- end item-box --></div>

</div>
<!-- [[/editor:rpp-related-subjects]] -->
