<!-- [[editor:rpp-resources]] -->
<div id="part-resources" class="item-part">

<div class="item-box-header">
  <h3>Sumberdaya dan Media Pembelajaran</h3>
</div>

<!-- begin item box --><div id="itemMaterialBox" class="item-box">

<div class="form-group">
  <ul class="list-group">
    <li class="list-group-item"
      ng-repeat="resource in item.data.resources">
      {{$index + 1}}. {{resource}}
      <a class="btn btn-info btn-xs" href="" ng-show="!view"
        ng-click="removeResource($index)">Hapus</a>
    </li>
    <li class="list-group-item" ng-hide="item.data.resources.length">Belum ada sumber dan media pembelajaran</li>
  </ul>

  <form class="input-group x-width-520px" ng-submit="addResource()" ng-show="!view">
    <input id="newResource" type="text" class="form-control"
      placeholder="Masukkan Nama Resource (contoh: Buku X, Video Y)">
    <span class="input-group-btn">
      <button class="btn btn-default" type="button" ng-click="addResource()">Tambahkan</button>
    </span>
  </form>
</div>

<!-- end item box --></div>

</div>
<!-- [[/editor:rpp-resources]] -->
