<!-- [[/editor:rpp-assessments]] -->
<div id="part-assessments" class="item-part">

<div class="item-box-header">
  <h3>Penilaian Pembelajaran</h3>
</div>

<!-- begin item box --><div id="item-assessments-box" class="item-box">

<div ng-show="!view">
  <div class="form-group">
    <button class="btn btn-primary" ng-click="prepareAddAssessment()">Tambah Soal</button>
  </div>
</div>

<div class="panel panel-success" ng-show="item.data.assessment.multipleChoice.length">
  <div class="panel-heading">
    <h3 class="panel-title">Pilihan Ganda</h3>
  </div>

  <div class="panel-body" ng-repeat="mc in item.data.assessment.multipleChoice">
    <div class="panel panel-default">

    <div class="panel-heading">
      Pilihan Ganda {{$index + 1}}
      <a class="btn btn-danger pull-right btn-xs" href="" ng-show="!view"
        ng-click="removeAssessment('mc', $index)">Hapus</a>
      <a class="btn btn-warning pull-right btn-xs" ng-show="!view"
          ng-click="prepareUpdateAssessment('mc', $index)">Ubah</a>
    </div>

    <div class="panel-body">
      <strong>Indikator</strong><br>
      {{mc.indicator}}<br/>
      <strong>Jenjang Kognitif</strong><br>
      {{mc.cognitiveLevel}}<br/>
      <strong>Pertanyaan</strong><br>
      {{mc.question}}<br/>
      <strong>Pilihan Jawaban</strong><br>
      <ul>
        <li ng-repeat="option in mc.options">{{option}}</li>
      </ul>
      <strong>Solusi</strong><br>
      {{mc.solution}}<br/>
      <strong>Skor</strong><br>
      {{mc.score}}<br/>
    </div>

    </div>
  </div>

</div>

<div class="panel panel-warning" ng-show="item.data.assessment.essay.length">
  <div class="panel-heading">
    <h3 class="panel-title">Essay</h3>
  </div>

  <div class="panel-body" ng-repeat="es in item.data.assessment.essay">
    <div class="panel panel-default">

    <div class="panel-heading">
      Essay {{$index + 1}}
      <a class="btn btn-danger pull-right btn-xs" href="" ng-show="!view"
        ng-click="removeAssessment('es', $index)">Hapus</a>
      <a class="btn btn-warning pull-right btn-xs" ng-show="!view"
          ng-click="prepareUpdateAssessment('es', $index)">Ubah</a>
    </div>

    <div class="panel-body">
      <strong>Indikator</strong><br>
      {{es.indicator}}<br/>
      <strong>Jenjang Kognitif</strong><br>
      {{es.cognitiveLevel}}<br/>
      <strong>Pertanyaan</strong><br>
      {{es.question}}<br/>
      <strong>Solusi dan Skor</strong><br>
      <ul>
        <li ng-repeat="solution in es.solutions">{{solution.keyPoint}} - {{solution.score}}</li>
      </ul>
    </div>

    </div>
  </div>

</div>

<div class="panel panel-info" ng-show="item.data.assessment.simpleQuiz.length">
  <div class="panel-heading">
    <h3 class="panel-title">Pertanyaan Singkat</h3>
  </div>

  <div class="panel-body" ng-repeat="sq in item.data.assessment.simpleQuiz">
    <div class="panel panel-default">

    <div class="panel-heading">
      Pertanyaan Singkat {{$index + 1}}
      <a class="btn btn-danger pull-right btn-xs" href="" ng-show="!view"
        ng-click="removeAssessment('sq', $index)">Hapus</a>
      <a class="btn btn-warning pull-right btn-xs" ng-show="!view"
          ng-click="prepareUpdateAssessment('sq', $index)">Ubah</a>
    </div>

    <div class="panel-body">
      <strong>Indikator</strong><br>
      {{sq.indicator}}<br/>
      <strong>Jenjang Kognitif</strong><br>
      {{sq.cognitiveLevel}}<br/>
      <strong>Pertanyaan</strong><br>
      {{sq.question}}<br/>
      <strong>Solusi</strong><br>
      {{sq.solution}}<br/>
      <strong>skor</strong><br>
      {{sq.score}}<br/>
    </div>

    </div>
  </div>

</div>

<div class="panel panel-danger" ng-show="item.data.assessment.matching.length">
  <div class="panel-heading">
    <h3 class="panel-title">Mencocokkan</h3>
  </div>

  <div class="panel-body" ng-repeat="ma in item.data.assessment.matching">
    <div class="panel panel-default">

    <div class="panel-heading">
      Mencocokkan {{$index + 1}}
      <a class="btn btn-danger pull-right btn-xs" href="" ng-show="!view"
        ng-click="removeAssessment('ma', $index)">Hapus</a>
      <a class="btn btn-warning pull-right btn-xs" ng-show="!view"
          ng-click="prepareUpdateAssessment('ma', $index)">Ubah</a>
    </div>

    <div class="panel-body">
      <strong>Indikator</strong><br>
      {{ma.indicator}}<br/>
      <strong>Jenjang Kognitif</strong><br>
      {{ma.cognitiveLevel}}<br/>
      <strong>Pertanyaan</strong><br>
      {{ma.question}}<br/>
      <strong>Pasangan A</strong><br>
      {{ma.pairA}}<br/>
      <strong>Pasangan B</strong><br>
      {{ma.pairB}}<br/>
      <strong>Solusi</strong><br>
      {{ma.solution}}<br/>
      <strong>Skor</strong><br>
      {{ma.score}}<br/>
    </div>

    </div>
  </div>

</div>

<a href="#x" class="overlay" id="item-assessment-popup"></a>
<div class="panel panel-default popup" ng-show="!view">
  <div class="panel-heading" ng-show="updateAssessmentIndex == null">
    <h3 class="panel-title">Tambah Soal</h3>
  </div>

  <div class="panel-heading" ng-hide="updateAssessmentIndex == null">
    <h3 class="panel-title">Ubah Soal</h3>
  </div>

  <div class="panel-body">
    <form id="newAssessmentForm" ng-submit="addAssessment()">
      <div>
        <div class="form-group" ng-show="updateAssessmentIndex==null">
          <label for="newAssessmentType">Tipe Soal:</label>
          <select class="form-control" id="newAssessmentType" ng-model="newAssessmentType">
            <option value="">-- Pilih Tipe Soal --</option>
            <option value="mc">Pilihan Ganda</option>
            <option value="sq">Pertanyaan Singkat</option>
            <option value="es">Essay</option>
            <option value="ma">Mencocokkan</option>
          </select>
        </div>

        <div class="form-group" ng-hide="updateAssessmentIndex==null">
          <label>Tipe Soal:</label>
          <div class="form-control" ng-if="newAssessmentType=='mc'">
            Pilihan Ganda
          </div>
          <div class="form-control" ng-if="newAssessmentType=='sq'">
            Pertanyaan Singkat
          </div>
          <div class="form-control" ng-if="newAssessmentType=='es'">
            Essay
          </div>
          <div class="form-control" ng-if="newAssessmentType=='ma'">
            Mencocokkan
          </div>
        </div>

        <!-- [multipleChoice] -->
        <div ng-if="newAssessmentType == 'mc'">
          <div class="form-group">
            <label for="mcIndicator">Indikator:</label>
            <select id="mcIndicator" class="form-control"
              ng-model="mcAsmt.indicator" ng-init="mcAsmt.indicator = indicators[0]"
              ng-options="i for i in indicators" required></select>
          </div>

          <div class="form-group x-width-170px">
            <label>Jenjang Kognitif:</label>
            <select class="form-control" ng-model="mcAsmt.cognitiveLevel">
              <option value="C1">C1 - Mengingat</option>
              <option value="C2">C2 - Memahami</option>
              <option value="C3">C3 - Menerapkan</option>
              <option value="C4">C4 - Analisis</option>
              <option value="C5">C5 - Sintesis</option>
              <option value="C6">C6 - Evaluasi</option>
            </select>
          </div>

          <div class="form-group">
            <label for="mcQuestion">Pertanyaan:</label>
            <input id="mcQuestion" class="form-control" type="text"
              ng-model="mcAsmt.question" placeholder="Masukkan Pertanyaan" required/>
          </div>

          <div>
            <label>Opsi Jawaban</label>
            <ul class="list-group">
              <li class="list-group-item">
                <div class="form-group">
                  <input type="text" class="form-control" ng-model="mcAsmt.options[0]"
                    placeholder="Pilihan A" required/>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" ng-model="mcAsmt.options[1]"
                    placeholder="Pilihan B" required/>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" ng-model="mcAsmt.options[2]"
                    placeholder="Pilihan C" required/>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" ng-model="mcAsmt.options[3]"
                    placeholder="Pilihan D" required/>
                </div>
              </li>
            </ul>
          </div>

          <div class="form-group">
            <label>Solusi:</label>
              <select class="form-control" ng-model="mcAsmt.solution" required>
                <option value="" selected="selected">-- Pilih Solusi --</option>
                <option value="A. {{mcAsmt.options[0]}}">A. {{mcAsmt.options[0]}}</option>
                <option value="B. {{mcAsmt.options[1]}}">B. {{mcAsmt.options[1]}}</option>
                <option value="C. {{mcAsmt.options[2]}}">C. {{mcAsmt.options[2]}}</option>
                <option value="D. {{mcAsmt.options[3]}}">D. {{mcAsmt.options[3]}}</option>
              </select>
          </div>

          <div class="form-group">
            <label>Nilai:</label>
            <input class="form-control" ng-model="mcAsmt.score" required/>
          </div>

          <div class="form-group">
            <button type="submit" class="btn btn-default">Simpan</button>
          </div>
        </div>
        <!-- [/multipleChoice] -->

        <!-- [simpleQuiz] -->
        <div ng-if="newAssessmentType == 'sq'">
          <div class="form-group">
            <label for="sqIndicator">Indikator:</label>
            <select id="sqIndicator" class="form-control"
              ng-model="sqAsmt.indicator" ng-init="sqAsmt.indicator = indicators[0]"
              ng-options="i for i in indicators" required></select>
          </div>

          <div class="form-group x-width-170px">
            <label>Jenjang Kognitif:</label>
            <select class="form-control" ng-model="sqAsmt.cognitiveLevel">
              <option value="C1">C1 - Mengingat</option>
              <option value="C2">C2 - Memahami</option>
              <option value="C3">C3 - Menerapkan</option>
              <option value="C4">C4 - Analisis</option>
              <option value="C5">C5 - Sintesis</option>
              <option value="C6">C6 - Evaluasi</option>
            </select>
          </div>

          <div class="form-group">
            <label for="sqQuestion">Pertanyaan:</label>
            <input id="sqQuestion" class="form-control" type="text"
              ng-model="sqAsmt.question" placeholder="Masukkan Pertanyaan" required/>
          </div>

          <div class="form-group">
            <label>Solusi:</label>
            <input id="sqSolution" class="form-control" type="text"
              ng-model="sqAsmt.solution" placeholder="Masukkan Solusi" required/>
          </div>

          <div class="form-group">
            <label>Nilai:</label>
            <input class="form-control" ng-model="sqAsmt.score" required/>
          </div>

          <div class="form-group">
            <button type="submit" class="btn btn-default">Simpan</button>
          </div>
        </div>
        <!-- [/simpleQuiz] -->

        <!-- [essay] -->
        <div ng-if="newAssessmentType == 'es'">
          <div class="form-group">
            <label for="esIndicator">Indikator:</label>
            <select id="esIndicator" class="form-control"
              ng-model="esAsmt.indicator" ng-init="esAsmt.indicator = indicators[0]"
              ng-options="i for i in indicators" required></select>
          </div>

          <div class="form-group x-width-170px">
            <label>Jenjang Kognitif:</label>
            <select class="form-control" ng-model="esAsmt.cognitiveLevel">
              <option value="C1">C1 - Mengingat</option>
              <option value="C2">C2 - Memahami</option>
              <option value="C3">C3 - Menerapkan</option>
              <option value="C4">C4 - Analisis</option>
              <option value="C5">C5 - Sintesis</option>
              <option value="C6">C6 - Evaluasi</option>
            </select>
          </div>

          <div class="form-group">
            <label for="esQuestion">Pertanyaan:</label>
            <input id="esQuestion" class="form-control" type="text"
              ng-model="esAsmt.question" placeholder="Masukkan Pertanyaan" required/>
          </div>

          <div>
            <label>Solusi:</label>
            <ul class="list-group">
              <li class="list-group-item" ng-repeat="solution in esAsmt.solutions">
                <div><strong>Kata Kunci</strong>: {{solution.keyPoint}}</div>
                <div><strong>Score</strong>: {{solution.score}}</div>
                <div><a href="" class="btn btn-xs btn-info" ng-click="removeAsmtSolution($index)">
                  Hapus
                </a></div>
              </li>
              <li class="list-group-item" ng-hide="esAsmt.solutions.length">
                <div>Belum ada solusi. Silahkan menambahkan.</div>
              </li>
            </ul>

            <div class="new-essay-solution">
              <label>Tambah Solusi</label>
              <div class="form-group">
                <label>Kata Kunci:</label>
                <input id="esSolutionKeyPoint" class="form-control input-sm" type="text"
                  ng-model="esAsmtSolution.keyPoint" placeholder="Masukkan Kata Kunci"/>
              </div>

              <div class="form-group">
                <label>Nilai:</label>
                <input id="esSolutionScore" class="form-control input-sm" type="text"
                  ng-model="esAsmtSolution.score"/>
              </div>

              <div class="form-group">
                <button type="button" class="btn btn-default btn-sm"
                  ng-click="addAsmtSolution()">
                  Tambahkan Solusi
                </button>
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-default">Simpan</button>
            </div>
          </div>
        </div>
        <!-- [/essay] -->

        <!-- [matching] -->
        <div ng-if="newAssessmentType == 'ma'">
          <div class="form-group">
            <label for="maIndicator">Indikator:</label>
            <select id="maIndicator" class="form-control"
              ng-model="maAsmt.indicator" ng-init="maAsmt.indicator = indicators[0]"
              ng-options="i for i in indicators" required></select>
          </div>

          <div class="form-group x-width-170px">
            <label>Jenjang Kognitif:</label>
            <select class="form-control" ng-model="maAsmt.cognitiveLevel">
              <option value="C1">C1 - Mengingat</option>
              <option value="C2">C2 - Memahami</option>
              <option value="C3">C3 - Menerapkan</option>
              <option value="C4">C4 - Analisis</option>
              <option value="C5">C5 - Sintesis</option>
              <option value="C6">C6 - Evaluasi</option>
            </select>
          </div>

          <div class="form-group">
            <label for="maQuestion">Pertanyaan:</label>
            <input id="maQuestion" class="form-control" type="text"
              ng-model="maAsmt.question" placeholder="Masukkan Pertanyaan" required/>
          </div>

          <div class="form-group">
            <label>Daftar Pilihan 1:</label>
            <input id="maPairA" class="form-control" type="text"
              ng-model="maAsmt.pairA" placeholder="Daftar Pilihan 1 (Contoh: Oli, Meja, Udara)" required/>
          </div>

          <div class="form-group">
            <label>Daftar Pilihan 2:</label>
            <input id="maPairB" class="form-control" type="text"
              ng-model="maAsmt.pairB" placeholder="Daftar Pilihan 2 (Contoh: Cair, Padat, Gas)"/>
          </div>

          <div class="form-group">
            <label>Solusi:</label>
            <input id="maSolution" class="form-control" type="text"
              ng-model="maAsmt.solution" placeholder="Solusi (Contoh: TV-Ruang Keluarga, Mobil-Garasi, Kompor-Dapur)"/>
          </div>

          <div class="form-group">
            <label>Nilai:</label>
            <input class="form-control" ng-model="maAsmt.score" required/>
          </div>

          <div class="form-group">
            <button type="submit" class="btn btn-default">Simpan</button>
          </div>
        </div>
        <!-- [/matching] -->
      </div>

    </form>
  </div>

  <a class="close" href="#close"></a>
</div>

<div class="form-group">
  <label>Rubrik Penilaian Unjuk Kerja:</label>
  <ul class="list-group">
    <li class="list-group-item"
      ng-repeat="workAssessment in item.data.workAssessment">
      {{$index + 1}}. {{workAssessment}}
      <a class="btn btn-info btn-xs" href="" ng-show="!view"
        ng-click="removeWorkAssessment($index)">Hapus</a>
    </li>
    <li class="list-group-item" ng-hide="item.data.workAssessment.length">Belum ada kriteria penilaian unjuk kerja</li>
  </ul>

  <form class="input-group x-width-520px" ng-submit="addWorkAssessment()" ng-show="!view">
    <input id="newWorkAssessment" type="text" class="form-control"
      placeholder="Masukkan Tujuan Pembelajaran">
    <span class="input-group-btn">
      <button class="btn btn-default" type="button" ng-click="addWorkAssessment()">Tambahkan</button>
    </span>
  </form>
</div>

<div class="form-group">
  <label>Rubrik Penilaian Observasi:</label>
  <ul class="list-group">
    <li class="list-group-item"
      ng-repeat="observationAssessment in item.data.observationAssessment">
      {{$index + 1}}. {{observationAssessment}}
      <a class="btn btn-info btn-xs" href="" ng-show="!view"
        ng-click="removeObservationAssessment($index)">Hapus</a>
    </li>
    <li class="list-group-item" ng-hide="item.data.observationAssessment.length">Belum ada kriteria penilaian observasi</li>
  </ul>

  <form class="input-group x-width-520px" ng-submit="addObservationAssessment()" ng-show="!view">
    <input id="newObservationAssessment" type="text" class="form-control"
      placeholder="Masukkan Tujuan Pembelajaran">
    <span class="input-group-btn">
      <button class="btn btn-default" type="button" ng-click="addObservationAssessment()">Tambahkan</button>
    </span>
  </form>
</div>

<!-- end item box --></div>

</div>
<!-- [[/editor:rpp-assessments]] -->
