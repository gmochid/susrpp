<!-- [[editor:rpp-material] -->
<div id="part-material" class="item-part">

<div class="item-box-header">
  <h3>Tujuan dan Materi Pembelajaran</h3>
</div>

<!-- begin item box --><div id="item-material-box" class="item-box">

<div class="form-group">
  <label>Tujuan Pembelajaran:</label>
  <ul class="list-group">
    <li class="list-group-item"
      ng-repeat="goal in item.data.goals">
      {{$index + 1}}. {{goal}}
      <a class="btn btn-info btn-xs" href="" ng-show="!view"
        ng-click="removeGoal($index)">Hapus</a>
    </li>
    <li class="list-group-item" ng-hide="item.data.goals.length">Belum ada tujuan pembelajaran</li>
  </ul>

  <form class="input-group x-width-520px" ng-submit="addGoal()" ng-show="!view">
    <input id="newGoal" type="text" class="form-control"
      placeholder="Masukkan Tujuan Pembelajaran">
    <span class="input-group-btn">
      <button class="btn btn-default" type="button" ng-click="addGoal()">Tambahkan</button>
    </span>
  </form>
</div>

<div class="form-group">
  <label>Materi Pembelajaran:</label>
  <textarea class="form-control wysiwyg-editor" ng-show="!view" id="itemMaterial">
    {{item.data.materials}}
  </textarea>
  <div class="form-control wysiwyg-viewer" ng-show="view" id="itemMaterialView">
  </div>
</div>

<!-- end item box --></div>

</div>
<!-- [[editor:rpp-material] -->
