<!-- [[editor:container-bottom]] -->
<div id="bottom" ng-show="!view">

<div class="item-box-header">
  <h3>Simpan RPP</h3>
</div>

<div class="item-box" id="bottom-box">
  <form>
    <button type="submit" class="btn btn-primary"
      ng-click="persistItem()">Simpan RPP</button>
    <button type="submit" class="btn btn-warning"
      ng-click="back()">Kembali ke Daftar RPP</button>
  </form>
</div>

</div>

<div class="item-box-header" ng-show="!new">
  <h3>Komentar</h3>
</div>

<div class="item-box" id="comments" ng-show="!new">

<div id="comments-area">
  <li class="list-group-item" ng-repeat="comment in comments">
    <div>
      <a class="commenter-name">{{comment.commenterName}}</a>
      <a>{{comment.timeCreated}}</a>
      <div class="pull-right" ng-if="currentUser == comment.commenterUsername">
        <a href="" ng-click="removeComment($index)">Hapus</a>
      </div>
    <div>
    <div><span>{{comment.comment}}</span></div>
  </li>
</div>

<div>
  <label>Tambahkan Komentar:</label>
  <form>
    <div class="form-group">
      <textarea id="comment-content" class="form-control"></textarea>
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-default" ng-click="addComment()">Submit</button>
    </div>
  </form>
</div>

</div>
<!-- [[/editor:container-bottom]] -->
