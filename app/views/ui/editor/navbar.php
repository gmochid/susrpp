<!-- [[editor:static-navbar]] -->

<div class="navbar navbar-default" role="navigation">
  <div class="container">

  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
      data-target=".navbar-collapse">

      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <div class="navbar-brand dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle">
        Teachingware 
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo url('/'); ?>">Home</a></li>
        <li class="divider"></li>
        <li><a href="<?php echo url('/rpp'); ?>">RPP Editor</a></li>
        <li><a href="<?php echo url('/'); ?>">Teachingware Editor</a></li>
      </ul>
    </div>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      <li <?php echo ($action == 'dashboardRpp') ? 'class="active"' : ''; ?>><a href="<?php echo url('/rpp'); ?>">Dashboard RPP</a></li>
      <li><a href="">Pencarian RPP</a></li>
      <li>
        <form method="get" action="<?php echo url('/editor?action=new'); ?>">
          <button type="submit" class="btn btn-info navbar-btn">Buat RPP Baru</button>
        </form>
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
          <div id="nameLink"></div>
        </a>
        <ul class="dropdown-menu">
          <li><a href="#">Profil</a></li>
          <li><a href="#">Pengaturan Akun</a></li>
        </ul>
      </li>
      <li><a href="<?php echo url('/logout'); ?>">Keluar</a></li>
    </ul>
  </div>

  </div>
</div>

<!-- [[/editor:static-navbar]] -->
