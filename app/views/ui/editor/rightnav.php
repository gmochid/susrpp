<!-- [[editor:right-nav]] -->

<div id="right-nav" data-spy="affix">
  <ul class="nav">
    <li id="right-nav-header">Navigasi</li>
    <li><a class="right-nav-link active-nav" href="#top" data-show="part-description">Deskripsi RPP</a></li>
    <li><a class="right-nav-link" href="#top" data-show="part-identity">Identitas RPP</a></li>
    <li><a class="right-nav-link" href="#top" data-show="part-related-subjects">Mata Pelajaran Terkait</a></li>
    <li><a class="right-nav-link" href="#top" data-show="part-material">Tujuan dan Materi Pembelajaran</a></li>
    <li><a class="right-nav-link" href="#top" data-show="part-learning-methods">Model, Metode dan Pendekatan Pembelajaran</a></li>
    <li><a id="part-activities-nav" class="right-nav-link" href="#top" data-show="part-activities">Aktivitas Pembelajaran</a></li>
    <li><a class="right-nav-link" href="#top" data-show="part-resources">Sumberdaya dan Media Pembelajaran</a></li>
    <li><a class="right-nav-link" href="#top" data-show="part-assessments">Penilaian Pembelajaran</a></li>
  </ul>
</div>

<!-- [[/editor:right-nav]] -->
