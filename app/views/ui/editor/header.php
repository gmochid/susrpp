<!-- [[editor:header]] -->

<!DOCTYPE html>
<html ng-app="SusrppEditor">

<head>
  <meta charset="utf-8"/>
  <title><?php echo $title; ?></title>

  <script type="text/javascript">(function(w){w.RootLocation="<?php echo url('/'); ?>";w.EditorData={action:"<?php echo $action; ?>",id:"<?php echo $id; ?>"};})(window);</script>
  <script type="text/javascript" src="<?php echo asset('lib/js/jquery-1.10.2.min.js') ?>"></script>
  <script type="text/javascript" src="<?php echo asset('lib/js/ckeditor/ckeditor.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('lib/js/ckeditor/adapters/jquery.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('js/api.js'); ?>"></script>

  <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet"/>
  <script type="text/javascript" src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>

  <link href="<?php echo asset('css/editor.css'); ?>" rel="stylesheet"/>

  <script type="text/javascript" src="<?php echo asset('js/data.js'); ?>"></script>
</head>

<body>

<div id="top"></div>

<!-- [[/editor:header]] -->
