<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Masuk</title>

    <script type="text/javascript" src="<?php echo asset('lib/js/jquery-1.10.2.min.js'); ?>"></script>

    <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">
    <script src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">RPP Editor</a>
		</div>
		<div class="navbar-collapse collapse">
			<form class="navbar-form navbar-right" role="form" id="loginForm" method="post" action="<?php echo url('/api/user/session/auth'); ?>">
				<div class="form-group">
				  <input type="text" placeholder="Username" class="form-control" name="username">
				</div>
				<div class="form-group">
				  <input type="password" placeholder="Password" class="form-control" name="password">
				</div>
				<button class="btn btn-success btn-sm btn-primary btn-lg active" type="submit">Login</button>
				<!-- <button type="" class="btn btn-success" href="dashboard.html">Masuk</button> -->
			</form>
		</div><!--/.navbar-collapse -->
	</div>
</div>



<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
  <div class="container">
    <h1>Selamat datang!</h1>
    <p>RPP Editor adalah sebuah website pengelola Rencana Pelaksanaan Pengajaran. Pada website ini Anda dapat membuat RPP sendiri atau melihat RPP yang dibuat oleh orang lain.</p>
    <!--
    <p><a class="btn btn-primary btn-lg" role="button">Lebih lanjut »</a></p>
    -->
  </div>
</div>

<!-- scripts -->
<script type="text/javascript" src="<?php echo asset('js/ui-login.js'); ?>"></script>

</body>

</html>
