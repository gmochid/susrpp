
<?php echo \View::make('ui/editor/header', array(
    'action' => $action,
    'id' => $id,
    'title' => 'RPP Editor - Editor/Viewer')
); ?>

<?php echo \View::make('ui/editor/navbar', array(
    'action' => $action,
)); ?>

<!-- [[editor:container]] -->

<div class="container" ng-controller="EditorController">

  <!-- [editor-form] -->
  <div id="editor-form">
    <div class="row">

      <div class="col-md-offset-1 col-md-8">
        <?php echo "\n" . View::make('ui/editor/container/description') . "\n"; ?>
        <?php echo "\n" . View::make('ui/editor/container/identity'). "\n"; ?>
        <?php echo "\n" . View::make('ui/editor/container/relatedsubjects'). "\n"; ?>
        <?php echo "\n" . View::make('ui/editor/container/material'). "\n"; ?>
        <?php echo "\n" . View::make('ui/editor/container/learningmethods'). "\n"; ?>
        <?php echo "\n" . View::make('ui/editor/container/activities'). "\n"; ?>
        <?php echo "\n" . View::make('ui/editor/container/resources'). "\n"; ?>
        <?php echo "\n" . View::make('ui/editor/container/assessments'). "\n"; ?>
        <?php echo "\n" . View::make('ui/editor/container/bottom'). "\n"; ?>
      </div>

      <div class="col-md-3">
        <?php echo "\n" . View::make('ui/editor/rightnav') . "\n"; ?>
      </div>

    </div>
  </div>
  <!-- [/editor-form] -->

</div>

<!-- [[/editor:container]] -->

<?php echo View::make('ui/editor/footer'); ?>
