<!-- [[/viewer:rpp-assessments]] -->
<div id="part-assessments">

<div class="item-box-header">
  <h3>Penilaian Pembelajaran</h3>
</div>

<!-- begin item box --><div id="item-assessments-box" class="item-box">

<div class="panel panel-success" ng-show="item.data.assessment.multipleChoice.length">
  <div class="panel-heading">
    <h3 class="panel-title">Pilihan Ganda</h3>
  </div>

  <div class="panel-body" ng-repeat="mc in item.data.assessment.multipleChoice">
    <div class="panel panel-default">

    <div class="panel-heading">
      Pilihan Ganda {{$index + 1}}
    </div>

    <div class="panel-body">
      <strong>Indikator</strong><br>
      {{mc.indicator}}<br/>
      <strong>Jenjang Kognitif</strong><br>
      {{mc.cognitiveLevel}}<br/>
      <strong>Pertanyaan</strong><br>
      {{mc.question}}<br/>
      <strong>Pilihan Jawaban</strong><br>
      <ul>
        <li ng-repeat="option in mc.options">{{option}}</li>
      </ul>
      <strong>Solusi</strong><br>
      {{mc.solution}}<br/>
      <strong>Skor</strong><br>
      {{mc.score}}<br/>
    </div>

    </div>
  </div>

</div>

<div class="panel panel-warning" ng-show="item.data.assessment.essay.length">
  <div class="panel-heading">
    <h3 class="panel-title">Essay</h3>
  </div>

  <div class="panel-body" ng-repeat="es in item.data.assessment.essay">
    <div class="panel panel-default">

    <div class="panel-heading">
      Essay {{$index + 1}}
    </div>

    <div class="panel-body">
      <strong>Indikator</strong><br>
      {{es.indicator}}<br/>
      <strong>Jenjang Kognitif</strong><br>
      {{es.cognitiveLevel}}<br/>
      <strong>Pertanyaan</strong><br>
      {{es.question}}<br/>
      <strong>Solusi dan Skor</strong><br>
      <ul>
        <li ng-repeat="solution in es.solutions">{{solution.keyPoint}} - {{solution.score}}</li>
      </ul>
    </div>

    </div>
  </div>

</div>

<div class="panel panel-info" ng-show="item.data.assessment.simpleQuiz.length">
  <div class="panel-heading">
    <h3 class="panel-title">Pertanyaan Singkat</h3>
  </div>

  <div class="panel-body" ng-repeat="sq in item.data.assessment.simpleQuiz">
    <div class="panel panel-default">

    <div class="panel-heading">
      Pertanyaan Singkat {{$index + 1}}
    </div>

    <div class="panel-body">
      <strong>Indikator</strong><br>
      {{sq.indicator}}<br/>
      <strong>Jenjang Kognitif</strong><br>
      {{sq.cognitiveLevel}}<br/>
      <strong>Pertanyaan</strong><br>
      {{sq.question}}<br/>
      <strong>Solusi</strong><br>
      {{sq.solution}}<br/>
      <strong>skor</strong><br>
      {{sq.score}}<br/>
    </div>

    </div>
  </div>

</div>

<div class="panel panel-danger" ng-show="item.data.assessment.matching.length">
  <div class="panel-heading">
    <h3 class="panel-title">Mencocokkan</h3>
  </div>

  <div class="panel-body" ng-repeat="ma in item.data.assessment.matching">
    <div class="panel panel-default">

    <div class="panel-heading">
      Mencocokkan {{$index + 1}}
    </div>

    <div class="panel-body">
      <strong>Indikator</strong><br>
      {{ma.indicator}}<br/>
      <strong>Jenjang Kognitif</strong><br>
      {{ma.cognitiveLevel}}<br/>
      <strong>Pertanyaan</strong><br>
      {{ma.question}}<br/>
      <strong>Pasangan A</strong><br>
      {{ma.pairA}}<br/>
      <strong>Pasangan B</strong><br>
      {{ma.pairB}}<br/>
      <strong>Solusi</strong><br>
      {{ma.solution}}<br/>
      <strong>Skor</strong><br>
      {{ma.score}}<br/>
    </div>

    </div>
  </div>

</div>

<div class="form-group">
  <label>Rubrik Penilaian Unjuk Kerja:</label>
  <ul class="list-group">
    <li class="list-group-item"
      ng-repeat="workAssessment in item.data.workAssessment">
      {{$index + 1}}. {{workAssessment}}
    </li>
    <li class="list-group-item" ng-hide="item.data.workAssessment.length">Belum ada kriteria penilaian unjuk kerja</li>
  </ul>

</div>

<div class="form-group">
  <label>Rubrik Penilaian Observasi:</label>
  <ul class="list-group">
    <li class="list-group-item"
      ng-repeat="observationAssessment in item.data.observationAssessment">
      {{$index + 1}}. {{observationAssessment}}
    </li>
    <li class="list-group-item" ng-hide="item.data.observationAssessment.length">Belum ada kriteria penilaian observasi</li>
  </ul>
</div>

<!-- end item box --></div>

</div>
<!-- [[/viewer:rpp-assessments]] -->
