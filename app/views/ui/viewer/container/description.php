<!-- [[viewer:rpp-description]] -->
<div id="part-description" class="item-part">

<div class="item-box-header">
  <h3>Deskripsi RPP</h3>
</div>

<!-- begin item-box --><div id="item-description-box" class="item-box">

<!--  Author -->
<div class="form-group">
  <label for="itemAuthor">Penulis:</label>
  <ul class="list-group">
    <li class="list-group-item" ng-repeat="author in authors">
      {{author.name}}
    </li>
    <li class="list-group-item" ng-hide="authors.length">Belum ada penulis</li>
  </ul>
</div>

<!-- Description -->
<div class="form-group">
  <label for="itemDescription">Deskripsi:</label>
  <div id="itemDescriptionView" class="form-control">
    {{item.description}}
  </div>
</div>

<!-- Tags -->
<div class="form-group">

  <label for="itemNewTag">Tag:</label>

  <ul class="tag-list">
    <li class="tag-list-item" ng-repeat="tag in item.tags">
      {{tag}}
    </li>
    <li class="tag-list-item" ng-hide="item.tags.length">-</li>
    <li class="clearfix">
  </ul>

</div>

<!-- end item-box --></div>

</div>
<!-- [[/viewer:rpp-description]] -->
