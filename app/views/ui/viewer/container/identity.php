<!-- [[viewer:rpp-identity]] -->
<div id="part-identity">

<div class="item-box-header">
  <h3>Identitas RPP</h3>
</div>

<!-- begin item-box --><div id="item-identity-box" class="item-box">

<!-- School Name -->
<div class="form-group">
  <label for="dataSchoolName">Nama Sekolah:</label>
  <div id="dataSchoolNameView" class="form-control">
    {{item.data.school}}
  </div>
</div>

<!-- Topic -->
<div class="form-group">
  <label for="dataTopic">Tema:</label>
  <div id="dataTopicView" class="form-control">
    {{item.data.topic}}
  </div>
</div>

<!-- Tema -->
<div class="form-group">
  <label for="dataTheme">Sub Tema:</label>
  <div id="dataThemeView" class="form-control">
    {{item.data.theme}}
  </div>
</div>

<!-- Kelas -->
<div class="form-group">
  <label for="dataKelas">Kelas:</label>
  <div id="dataKelasView" class="form-control">
    {{item.data.class}}
  </div>
</div>

<!-- Semester -->
<div class="form-group">
  <label for="dataSemester">Semester:</label>
  <div id="dataSemesterView" class="form-control">
    {{item.data.term}}
  </div>
</div>

<!-- end item-box --></div>

</div>
<!-- [[/viewer:rpp-identity]] -->
