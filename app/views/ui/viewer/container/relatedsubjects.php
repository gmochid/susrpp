 <!-- [[viewer:rpp-related-subjects]] -->
<div id="part-related-subjects">

<div class="item-box-header">
  <h3>Mata Pelajaran Terkait</h3>
</div>

<!-- begin item-box --><div id="item-related-subjects-box" class="item-box">

<!-- Empty subject list -->
<div class="well well-sm" ng-show="item.data.relatedSubjects.length == 0">
  Belum ada mata pelajaran terkait
</div>

<!-- [subject-list] -->
<div class="subjectList" ng-repeat="relatedSubject in item.data.relatedSubjects">
  <!-- begin panel --><div class="panel panel-default">

    <div class="panel-heading">
      <h3 class="panel-title">
        <strong>{{relatedSubject.subjectName}}</strong>
      </h3>
    </div>

    <div class="panel-body">
      <h4>Kompetensi:</h4>

      <div class="panel panel-default">
        <div class="panel-heading">{{relatedSubject.coreCompetence}}</div>

        <ul class="list-group">
          <li class="list-group-item"
            ng-repeat="basicCompetence in relatedSubject.basicCompetence">
            {{basicCompetence}}
          </li>
        </ul>
      </div>

      <h4>Indikator:</h4>
      <ul class="list-group">
        <li class="list-group-item"
          ng-repeat="indicator in relatedSubject.indicator">
          {{$index + 1}}. {{indicator}}
        </li>
      </ul>
    </div>

  <!-- end panel --></div>
</div>
<!-- [/subject-list] -->

<!-- end item-box --></div>

</div>
<!-- [[/viewer:rpp-related-subjects]] -->
