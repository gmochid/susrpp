<!-- [[viewer:rpp-resources]] -->
<div id="part-resources">

<div class="item-box-header">
  <h3>Sumberdaya dan Media Pembelajaran</h3>
</div>

<!-- begin item box --><div id="itemMaterialBox" class="item-box">

<div class="form-group">
  <ul class="list-group">
    <li class="list-group-item"
      ng-repeat="resource in item.data.resources">
      {{$index + 1}}. {{resource}}
    </li>
    <li class="list-group-item" ng-hide="item.data.resources.length">Belum ada sumber dan media pembelajaran</li>
  </ul>
</div>

<!-- end item box --></div>

</div>
<!-- [[/viewer:rpp-resources]] -->
