<!-- [[viewer:rpp-activities]] -->
<div id="part-activities">

<div class="item-box-header">
  <h3>Aktivitas Pembelajaran</h3>
</div>

<!-- begin item box --><div id="item-activity-box" class="item-box">

<div class="form-group">
  <label>Alokasi waktu aktivitas pembuka:</label>
  <div class="input-group x-width-110px">

    <input type="text" class="form-control" placeholder="..."
      ng-model="item.data.activities.startDuration" readonly>
    <span class="input-group-addon">Menit</span>
  </div>
</div>

<div class="form-group">
  <label>Alokasi waktu aktivitas inti:</label>
  <div class="input-group x-width-110px">

    <input type="text" class="form-control" placeholder="..."
      ng-model="item.data.activities.coreDuration" readonly>
    <span class="input-group-addon">Menit</span>
  </div>
</div>

<div class="form-group">
  <label>Alokasi waktu aktivitas penutup:</label>
  <div class="input-group x-width-110px">

    <input type="text" class="form-control" placeholder="..."
      ng-model="item.data.activities.closingDuration" readonly>
    <span class="input-group-addon">Menit</span>
  </div>
</div>

<div class="form-group">
  <strong>Total Durasi Aktivitas: {{sumDuration()}} Menit</strong><br>
  (Maksimal 70 menit)
</div>

<div class="form-group">
  <label>Aktivitas Pembuka:</label>

  <div class="form-control wysiwyg-viewer" id="itemStartActivityView">
  </div>
</div>

<div class="form-group">
  <label>Aktivitas Inti:</label>

  <div class="form-control wysiwyg-viewer" id="itemCoreActivityView">
  </div>
</div>

<div class="form-group">
  <label>Aktivitas Penutup:</label>

  <div class="form-control wysiwyg-viewer" id="itemClosingActivityView">
  </div>
</div>

<!-- end item box --></div>

</div>
<!-- [[/viewer:rpp-activities]] -->
