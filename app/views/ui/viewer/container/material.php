<!-- [[viewer:rpp-material] -->
<div id="part-material">

<div class="item-box-header">
  <h3>Tujuan dan Materi Pembelajaran</h3>
</div>

<!-- begin item box --><div id="item-material-box" class="item-box">

<div class="form-group">
  <label>Tujuan Pembelajaran:</label>
  <ul class="list-group">
    <li class="list-group-item"
      ng-repeat="goal in item.data.goals">
      {{$index + 1}}. {{goal}}
    </li>
    <li class="list-group-item" ng-hide="item.data.goals.length">Belum ada tujuan pembelajaran</li>
  </ul>
</div>

<div class="form-group">
  <label>Materi Pembelajaran:</label>
  <div class="form-control wysiwyg-viewer" id="itemMaterialView">
  </div>
</div>

<!-- end item box --></div>

</div>
<!-- [[viewer:rpp-material] -->
