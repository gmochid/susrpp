
<?php echo \View::make('ui/viewer/header', array(
    'action' => 'view',
    'id' => $id,
    'title' => 'RPP Editor - Editor/Viewer')
); ?>

<!-- [[viewer:container]] -->

<div class="container" ng-controller="ViewerController">

  <!-- [viewer-form] -->
  <div id="viewer-form">
    <div class="row">

      <div class="col-md-offset-1 col-md-8">
        <?php echo "\n" . View::make('ui/viewer/container/description') . "\n"; ?>
        <?php echo "\n" . View::make('ui/viewer/container/identity'). "\n"; ?>
        <?php echo "\n" . View::make('ui/viewer/container/relatedsubjects'). "\n"; ?>
        <?php echo "\n" . View::make('ui/viewer/container/material'). "\n"; ?>
        <?php echo "\n" . View::make('ui/viewer/container/learningmethods'). "\n"; ?>
        <?php echo "\n" . View::make('ui/viewer/container/activities'). "\n"; ?>
        <?php echo "\n" . View::make('ui/viewer/container/resources'). "\n"; ?>
        <?php echo "\n" . View::make('ui/viewer/container/assessments'). "\n"; ?>
      </div>

    </div>
  </div>
  <!-- [/viewer-form] -->

</div>

<!-- [[/viewer:container]] -->

<?php echo View::make('ui/viewer/footer'); ?>
