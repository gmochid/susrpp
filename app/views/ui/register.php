<!DOCTYPE html>
<html ng-app>

<head>
  <meta charset="utf-8">
  <title>RPP Editor - Daftar</title>

  <script type="text/javascript">(function(){window.RootLocation="<?php echo url('/'); ?>";})();</script>
  <script type="text/javascript" src="<?php echo asset('lib/js/jquery-1.10.2.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('js/api.js'); ?>"></script>

  <link href="<?php echo asset('lib/css/bootstrap.css')?>" rel="stylesheet">
  <script type="text/javascript" src="<?php echo asset('lib/js/bootstrap.min.js') ?>"></script>

<style type="text/css">
@import url('/css/font.css');

body { font-family: Ubuntu; }
.alert { margin: 0; padding: 8px; }
#message-box { display: none; }
#bottom-info { margin: 20px 0 0 0; font-size: 0.9em; }
</style>

</head>

<body>

<!-- Static Navbar -->
<div class="navbar navbar-default" role="navigation">
  <div class="container">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse"
        data-target=".navbar-collapse">

        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="<?php echo url('/'); ?>">RPP Editor</a>
    </div>

  </div>
</div>

<!-- Login Form -->
<div class="container">
  <div class="row">
    <div class="col-md-offset-2 col-md-8">

      <form id="register-form" role="form" class="form-horizontal" method="post"
        action="<?php echo url('/api/user/register'); ?>">

        <div class="text-center">
          <h2>Pendaftaran</h2>
          <br/><br/>
        </div>

        <div class="form-group">
          <label for="name" class="col-md-5 control-label">Nama Lengkap</label>
          <div class="col-md-5">
            <input type="text" placeholder="Nama Lengkap" class="form-control" name="name">
          </div>
        </div>

        <div class="form-group">
          <label for="email" class="col-md-5 control-label">Email</label>
          <div class="col-md-5">
            <input type="text" placeholder="Contoh: bagus@example.com" class="form-control" name="email">
          </div>
        </div>

        <div class="form-group">
          <label for="username" class="col-md-5 control-label">Username</label>
          <div class="col-md-5">
            <input type="text" placeholder="Username" class="form-control" name="username">
          </div>
        </div>

        <div class="form-group">
          <label for="password" class="col-md-5 control-label">Password</label>
          <div class="col-md-5">
            <input type="password" placeholder="Password" class="form-control" name="password">
          </div>
        </div>

        <div class="form-group">
          <label for="username" class="col-md-5 control-label">Grup</label>
          <div class="col-md-5">
            <select class="form-control" name="groupName">
                <?php foreach ($groups as $group): ?>
                <option value="<?php echo $group->groupName; ?>"><?php echo $group->groupName; ?></option>
                <?php endforeach; ?>
            </select>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-offset-5 col-md-5">
            <button class="btn btn-primary" type="submit">Daftar</button>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-offset-2 col-md-10">
            <div id="message-box"></div>
          </div>
        </div>
      </form>

    </div>
  </div>

  <div class="row">
    <div class="col-md-offset-2 col-md-8">
      <div id="bottom-info" class="text-justify">
        <p>RPP Editor adalah sebuah aplikasi pengelola Rencana Pelaksanaan Pengajaran.
        Pada aplikasi ini Anda dapat membuat RPP sendiri atau melihat RPP yang
        dibuat oleh orang lain.</p>
      </div>
    </div>
  </div>
</div>


<!-- scripts -->
<script type="text/javascript" src="<?php echo asset('js/ui/register.js'); ?>"></script>

</body>

</html>
