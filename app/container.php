<?php
/* ----------------------------------------------------------------------------
 * IoC Container
 * ----------------------------------------------------------------------------
 */

// Drivers --------------------------------------------------------------------
App::bind('mongoDbDriver', function()
{
    $env = Config::get('app.susenv');

    return new \SusRpp\Lib\DbDriver\MongoDbDriver(
        Config::get('mongodbdriver.'.$env.'.connection_string'),
        Config::get('mongodbdriver.'.$env.'.database')
    );
});

// Factories ------------------------------------------------------------------
App::bind('itemFactory', function()
{
    return new \SusRpp\Factory\ItemFactory();
});

App::bind('subjectFactory', function()
{
    return new \SusRpp\Factory\SubjectFactory();
});

App::bind('groupFactory', function()
{
    return new \SusRpp\Factory\GroupFactory();
});

App::bind('commentFactory', function()
{
    return new \SusRpp\Factory\CommentFactory();
});

App::bind('resourceFactory', function()
{
    return new \SusRpp\Factory\ResourceFactory();
});

// Services -------------------------------------------------------------------
App::bind('itemService', function()
{
    return new \SusRpp\Service\ItemService(
        App::make('mongoDbDriver'),
        App::make('itemFactory')
    );
});

App::bind('subjectService', function()
{
    return new \SusRpp\Service\SubjectService(
        App::make('mongoDbDriver'),
        App::make('subjectFactory')
    );
});

App::bind('userService', function()
{
    return new \SusRpp\Service\UserService(
        App::make('mongoDbDriver'),
        App::make('itemService')
    );
});

App::bind('groupService', function()
{
    return new \SusRpp\Service\GroupService(
        App::make('mongoDbDriver'),
        App::make('userService'),
        App::make('groupFactory')
    );
});

App::bind('commentService', function()
{
    return new \SusRpp\Service\CommentService(
        App::make('mongoDbDriver'),
        App::make('commentFactory')
    );
});

App::bind('resourceService', function()
{
    return new \SusRpp\Service\ResourceService(
        App::make('mongoDbDriver'),
        App::make('resourceFactory')
    );
});

App::bind('authService', function()
{
    return new \SusRpp\Service\AuthService(
        App::make('mongoDbDriver'),
        App::make('userService')
    );
});

// Controllers ----------------------------------------------------------------
App::bind('rppController', function()
{
    return new \SusRpp\Controller\RppController(
        App::make('userService'),
        App::make('authService'),
        App::make('itemService'),
        App::make('itemFactory')
    );
});

App::bind('subjectController', function()
{
    return new \SusRpp\Controller\SubjectController(
        App::make('subjectService'),
        App::make('subjectFactory')
    );
});

App::bind('userController', function()
{
    return new \SusRpp\Controller\UserController(
        App::make('userService'),
        App::make('groupService'),
        App::make('authService')
    );
});

App::bind('commentController', function()
{
    return new \SusRpp\Controller\CommentController(
        App::make('commentService'),
        App::make('commentFactory')
    );
});

App::bind('resourceController', function()
{
    return new \SusRpp\Controller\ResourceController(
        App::make('itemService'),
        App::make('resourceService'),
        App::make('resourceFactory')
    );
});

App::bind('uiController', function()
{
    return new \SusRpp\Controller\UiController();
});

App::bind('adminController', function()
{
    return new \SusRpp\Controller\AdminController();
});
