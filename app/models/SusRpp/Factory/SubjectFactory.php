<?php
namespace SusRpp\Factory;

use SusRpp\Entity\Subject;
use SusRpp\Entity\Subject\Competence;

class SubjectFactory
{
    public function fromJson($json)
    {
        $assoc = json_decode($json, true);
        return $this->fromArray($assoc);
    }

    public function fromArray($assoc)
    {
        $subject = new Subject();

        // MongoId
        if (array_key_exists('_id', $assoc)) {
            if ($assoc['_id'] instanceof \MongoId) {
                $subject->_id = $assoc['_id']->__toString();
            } else {
                $subject->_id = $assoc['_id'];
            }
        }

        $subject->subjectName = $assoc['subjectName'];

        $coreArr = array();
        foreach ($assoc['coreCompetences'] as $coreCompetence) {
            $basicArr = array();
            foreach ($coreCompetence['basicCompetences'] as $basicCompetence) {
                $basicArr[] = new Competence($basicCompetence);
            }
            $coreArr[] = new Competence(array(
                'number' => $coreCompetence['number'],
                'description' => $coreCompetence['description'],
                'basicCompetences' => $basicArr,
            ));
        }

        $subject->coreCompetences = $coreArr;

        return $subject;
    }
}
