<?php
namespace SusRpp\Factory;

use SusRpp\Entity\Comment;

class CommentFactory
{
    public function fromJson($json)
    {
        $assoc = json_decode($json, true);
        return $this->fromArray($assoc);
    }

    public function fromArray($assoc)
    {
        $comment = new Comment();

        // MongoId
        if (array_key_exists('_id', $assoc)) {
            if ($assoc['_id'] instanceof \MongoId) {
                $comment->_id = $assoc['_id']->__toString();
            } else {
                $comment->_id = $assoc['_id'];
            }
        }

        $comment->commenterUsername = $assoc['commenterUsername'];
        $comment->commenterName     = $assoc['commenterName'];
        $comment->comment           = $assoc['comment'];
        $comment->timeCreated       = $assoc['timeCreated'];

        if (array_key_exists('itemId', $assoc)) {
            $comment->itemId = $assoc['itemId'];
        }

        return $comment;
    }
}
