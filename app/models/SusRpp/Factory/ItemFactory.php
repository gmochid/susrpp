<?php
namespace SusRpp\Factory;

use SusRpp\Entity\Item;
use SusRpp\Entity\Item\Content;
use SusRpp\Entity\Item\SubjectItem;

use SusRpp\Entity\Item\Activity;
use SusRpp\Entity\Item\ActivityItem;

use SusRpp\Entity\Item\Assessment;
use SusRpp\Entity\Item\Assessment\MultipleChoiceAssessmentItem;
use SusRpp\Entity\Item\Assessment\EssayAssessmentItem;
use SusRpp\Entity\Item\Assessment\SimpleQuizAssessmentItem;
use SusRpp\Entity\Item\Assessment\MatchingQuizAssessmentItem;

class ItemFactory
{
    public function fromJson($json)
    {
        $assoc = json_decode($json, true);
        return $this->fromArray($assoc);
    }

    public function fromArray($assoc)
    {
        $contentArray = $assoc['data'];

        $item = new Item();

        // MongoId
        if (array_key_exists('_id', $assoc)) {
            if ($assoc['_id'] instanceof \MongoId) {
                $item->_id = $assoc['_id']->__toString();
            } else {
                $item->_id = $assoc['_id'];
            }
        }

        $item->title       = $assoc['title'];
        $item->status      = $assoc['status'];
        $item->authors     = $assoc['authors'];
        $item->description = $assoc['description'];
        $item->lookup      = $assoc['lookup'];
        $item->tags        = $assoc['tags'];

        if (isset($assoc['authorIds'])) {
            $item->authorIds = $assoc['authorIds'];
        }
        // (+ 16/02/2014): lastUpdated
        if (isset($assoc['lastUpdated'])) {
            $item->lastUpdated = $assoc['lastUpdated'];
        }
        // (+ 16/03/2014): ownerGroup
        if (isset($assoc['ownerGroup'])) {
            $item->ownerGroup = $assoc['ownerGroup'];
        }

        // Content ------------------------------------------------------------
        $content = new Content();
        $content->school    = $contentArray['school'];
        $content->topic     = $contentArray['topic'];
        $content->theme     = $contentArray['theme'];
        $content->class     = $contentArray['class'];
        $content->term      = $contentArray['term'];

        $arr = array();
        foreach($contentArray['relatedSubjects'] as $subject) {
            $arr[] = new SubjectItem($subject);
        }
        $content->relatedSubjects = $arr;

        $content->goals     = $contentArray['goals'];
        $content->materials = $contentArray['materials'];
        $content->resources = $contentArray['resources'];
        $content->workAssessment = $contentArray['workAssessment'];
        $content->observationAssessment = $contentArray['observationAssessment'];

        // Content/Activities -------------------------------------------------
        $activity = new Activity();
        $activity->learnApproach = $contentArray['activities']['learnApproach'];
        $activity->learnMethods  = $contentArray['activities']['learnMethods'];
        $activity->learnModels   = $contentArray['activities']['learnModels'];

        $activity->startActivity = $contentArray['activities']['startActivity'];
        $activity->coreActivity = $contentArray['activities']['coreActivity'];
        $activity->closingActivity = $contentArray['activities']['closingActivity'];

        $activity->startDuration   = $contentArray['activities']['startDuration'];
        $activity->coreDuration    = $contentArray['activities']['coreDuration'];
        $activity->closingDuration = $contentArray['activities']['closingDuration'];

        $content->activities = $activity;

        // Content/Assessment -------------------------------------------------
        $assessment = new Assessment();

        $arr = array();
        foreach ($contentArray['assessment']['multipleChoice'] as $asmtItem) {
            $arr[] = new MultipleChoiceAssessmentItem($asmtItem);
        }
        $assessment->multipleChoice = $arr;

        $arr = array();
        foreach($contentArray['assessment']['essay'] as $asmtItem) {
            $arr[] = new EssayAssessmentItem($asmtItem);
        }
        $assessment->essay = $arr;

        $arr = array();
        foreach($contentArray['assessment']['simpleQuiz'] as $asmtItem) {
            $arr[] = new SimpleQuizAssessmentItem($asmtItem);
        }
        $assessment->simpleQuiz = $arr;

        $arr = array();
        foreach($contentArray['assessment']['matching'] as $asmtItem) {
            $arr[] = new MatchingQuizAssessmentItem($asmtItem);
        }
        $assessment->matching = $arr;

        $content->assessment = $assessment;
        $item->data = $content;

        return $item;
    }
}
