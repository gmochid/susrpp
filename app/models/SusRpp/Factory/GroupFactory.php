<?php
namespace SusRpp\Factory;

use SusRpp\Entity\Group;

class GroupFactory
{
    public function fromJson($json)
    {
        $assoc = json_decode($json, true);
        return $this->fromArray($assoc);
    }

    public function fromArray($assoc)
    {
        $group = new Group();

        // MongoId
        if (array_key_exists('_id', $assoc)) {
            if ($assoc['_id'] instanceof \MongoId) {
                $group->_id = $assoc['_id']->__toString();
            } else {
                $group->_id = $assoc['_id'];
            }
        }

        $group->groupName = $assoc['groupName'];
        $userIds = array();

        foreach($assoc['userIds'] as $username) {
            $userIds[] = $username;
        }

        $group->userIds = $userIds;

        return $group;
    }
}
