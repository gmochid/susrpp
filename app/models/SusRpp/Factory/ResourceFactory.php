<?php
namespace SusRpp\Factory;

use SusRpp\Entity\Resource;

class ResourceFactory
{
    public function fromJson($json)
    {
        $assoc = json_decode($json, true);
        return $this->fromArray($assoc);
    }

    public function fromArray($assoc)
    {
        $resource = new Resource();

        // MongoId
        if (array_key_exists('_id', $assoc)) {
            if ($assoc['_id'] instanceof \MongoId) {
                $resource->_id = $assoc['_id']->__toString();
            } else {
                $resource->_id = $assoc['_id'];
            }
        }

        $resource->itemId = $assoc['itemId'];
        $resource->storedFilename = $assoc['storedFilename'];
        $resource->originalFilename = $assoc['originalFilename'];
        $resource->fileExtension = $assoc['fileExtension'];

        return $resource;
    }
}
