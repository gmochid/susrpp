<?php
namespace SusRpp\Entity;

class BaseEntity
{
    function __construct($argsKeyVal = null)
    {
        if ($argsKeyVal != null) {
            foreach ($argsKeyVal as $key => $val) {
                $this->__set($key, $val);
            }
        }
    }

    function __get($property)
    {
        $method = "get" . ucfirst($property);
        if (method_exists($this, $method)) {
            return $this->{$method}();
        } else {
            return $this->{$property};
        }
    }

    function __set($property, $value)
    {
        $method = "set" . ucfirst($property);
        if (method_exists($this, $method)) {
            $this->{$method}($value);
        } else {
            $this->{$property} = $value;
        }
    }

    function toArray()
    {
        $array = array();

        $rflObject = new \ReflectionObject($this);
        $rflProperties = $rflObject->getProperties();

        foreach ($rflProperties as $rflProperty) {
            $rflProperty->setAccessible(true);

            $name  = $rflProperty->getName();
            $value = $rflProperty->getValue($this);

            if (is_array($value)) {
                for ($i = 0; $i < count($value); $i++) {
                    if ($value[$i] instanceof \SusRpp\Entity\BaseEntity) {
                        $value[$i] = $value[$i]->toArray();
                    }
                }
            }

            if ($value instanceof \SusRpp\Entity\BaseEntity) {
                $value = $value->toArray();
            }

            $array[$name] = $value;
        }

        return $array;
    }

    function toJson($prettyPrint = false)
    {
        if (version_compare(phpversion(), '5.4.0', '>=') && $prettyPrint) {
            return json_encode($this->toArray(), JSON_PRETTY_PRINT);
        } else {
            return json_encode($this->toArray());
        }
    }
}
