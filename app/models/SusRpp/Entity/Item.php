<?php
namespace SusRpp\Entity;

/**
 * Item Entity
 *
 * @property string     $_id
 * @property string     $title
 * @property string     $status (values: draft|published|final)
 * @property string[]   $authors
 * @property string[]   $authorIds
 * @property string     $ownerGroup
 * @property string     $description
 * @property string     $lookup
 * @property string[]   $tags
 * @property \SusRpp\Entity\Item\Content $data
 * @property datetime   $lastUpdated;
 */
class Item extends \SusRpp\Entity\BaseEntity
{
    protected $title;
    protected $status;
    protected $authors;
    protected $authorIds;
    protected $ownerGroup = 'nogroup';
    protected $description;
    protected $lookup;
    protected $tags;
    protected $data;
    protected $lastUpdated;
}
