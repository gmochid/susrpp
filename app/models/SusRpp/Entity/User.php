<?php
namespace SusRpp\Entity;

/**
 * User Entity
 *
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $lookupName
 * @property string $email
 * @property string $sesstoken
 * @property \SusRpp\Entity\Item[] $items
 * @property string $role
 * @property string $groupId
 * @property string $groupName
 * @property int $status
 */
class User extends \SusRpp\Entity\BaseEntity
{
    protected $username;
    protected $password;
    protected $name;
    protected $email;
    protected $sesstoken = null;
    protected $items     = null;

    protected $groupId   = null;
    protected $groupName = 'nogroup';
    // "user" role => a normal member in a group; "tutor" role => a tutor in a group;
    protected $role = "user";
    protected $status = 1;

    // Additional Getters & Setters
    // -- which is actually not used
    public function getLookupName()
    {
        return strtolower($this->name);
    }
}
