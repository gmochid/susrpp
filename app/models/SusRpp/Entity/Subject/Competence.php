<?php
namespace SusRpp\Entity\Subject;

/**
 * CoreCompetence Entity
 *
 * @property string   $number
 * @property string   $description
 * @property \SusRpp\Entity\Subject\Competence $basicCompetences
 */
class Competence extends \SusRpp\Entity\BaseEntity
{
    protected $number;
    protected $description;
    protected $basicCompetences = null;
}
