<?php
namespace SusRpp\Entity;

/**
 * Subject Entity
 *
 * @property string   $_id
 * @property string   $subjectName
 * @property \SusRpp\Entity\Subject\Competence[] $coreCompetences
 */
class Subject extends \SusRpp\Entity\BaseEntity
{
    protected $subjectName;
    protected $coreCompetences;
}
