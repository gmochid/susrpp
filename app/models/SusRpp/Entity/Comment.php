<?php
namespace SusRpp\Entity;

/**
 * Comment
 *
 * @property string $_id
 * @property string $commenterUsername
 * @property string $commenterName
 * @property string $itemId
 * @property string $timeCreated
 */
class Comment extends BaseEntity
{
    public $commenterUsername;
    public $commenterName;

    // A comment belongs to an item
    public $itemId;
    public $comment;

    public $timeCreated;
}
