<?php
namespace SusRpp\Entity;

/**
 * Resource Entity
 *
 * @property string $_id
 * @property string $itemId
 * @property string $storedFilename
 * @property string $originalFilename
 * @property string $fileExtension
 */
class Resource extends \SusRpp\Entity\BaseEntity
{
	// A comment belongs to an item
    protected $itemId;
    protected $storedFilename;
    protected $originalFilename;
    protected $fileExtension;
}
