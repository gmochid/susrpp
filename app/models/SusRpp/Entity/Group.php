<?php
namespace SusRpp\Entity;

/**
 * User Entity
 *
 * @property string $_id
 * @property string $groupName
 * @property string[] $userIds
 */
class Group extends \SusRpp\Entity\BaseEntity
{
    protected $groupName;
    protected $userIds;
}
