<?php
namespace SusRpp\Entity\Item\Assessment;

/**
 * @property string  $indicator
 * @property string  $question
 * @property mixed[] $solutions (Format: {keyPoint, score})
 * @property string  $cognitiveLevel;
 */
class EssayAssessmentItem extends \SusRpp\Entity\BaseEntity
{
    protected $indicator;
    protected $question;
    protected $solutions;
    protected $cognitiveLevel;
}
