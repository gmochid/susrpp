<?php
namespace SusRpp\Entity\Item\Assessment;

/**
 * @property string $indicator
 * @property string $question
 * @property string $solution
 * @property int    $score
 * @property string $cognitiveLevel
 */
class SimpleQuizAssessmentItem extends \SusRpp\Entity\BaseEntity
{
    protected $indicator;
    protected $question;
    protected $solution;
    protected $score;
    protected $cognitiveLevel;
}
