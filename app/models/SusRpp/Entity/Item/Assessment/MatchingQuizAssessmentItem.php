<?php
namespace SusRpp\Entity\Item\Assessment;

/**
 * @property string $indicator
 * @property string $question
 * @property string $pairA
 * @property string $pairB
 * @property string $solution
 * @property int    $score
 * @property string $cognitiveLevel
 */
class MatchingQuizAssessmentItem extends \SusRpp\Entity\BaseEntity
{
    protected $indicator;
    protected $question;
    protected $pairA;
    protected $pairB;
    protected $solution;
    protected $score;
    protected $cognitiveLevel;
}
