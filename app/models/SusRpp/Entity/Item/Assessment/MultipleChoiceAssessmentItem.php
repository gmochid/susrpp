<?php
namespace SusRpp\Entity\Item\Assessment;

/**
 * @property string   $indicator
 * @property string   $question
 * @property string[] $options
 * @property string   $solution
 * @property int      $score
 * @property string   $cognitiveLevel
 */
class MultipleChoiceAssessmentItem extends \SusRpp\Entity\BaseEntity
{
    protected $indicator;
    protected $question;
    protected $options;
    protected $solution;
    protected $score;
    protected $cognitiveLevel;
}
