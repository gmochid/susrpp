<?php
namespace SusRpp\Entity\Item;

/**
 * Activity Entity
 *
 * @property string[] $learnApproach
 * @property string[] $learnMethods
 * @property string[] $learnModels
 * @property \SusRpp\Entity\Item\ActivityItem[] $startActivities
 * @property \SusRpp\Entity\Item\ActivityItem[] $coreActivites
 * @property \SusRpp\Entity\Item\ActivityItem[] $closingActivites
 * @property int      $startDuration
 * @property int      $coreDuration
 * @property int      $closingDuration
 */
class Activity extends \SusRpp\Entity\BaseEntity
{
    protected $learnApproach;
    protected $learnMethods;
    protected $learnModels;
    protected $startActivity;
    protected $coreActivity;
    protected $closingActivity;
    protected $startDuration;
    protected $coreDuration;
    protected $closingDuration;
}
