<?php
namespace SusRpp\Entity\Item;

/**
 * Assessment Entity
 *
 * @property \SusRpp\Entity\Item\Assessment\MultipleChoiceAssessmentItem[] $multipleChoice
 * @property \SusRpp\Entity\Item\Assessment\EssayAssessmentItem[]          $essay
 * @property \SusRpp\Entity\Item\Assessment\SimpleQuizAssessmentItem[]     $simpleQuiz
 * @property \SusRpp\Entity\Item\Assessment\MatchingAssessmentItem[]       $matching
 */
class Assessment extends \SusRpp\Entity\BaseEntity
{
    protected $multipleChoice;
    protected $essay;
    protected $simpleQuiz;
    protected $matching;
}
