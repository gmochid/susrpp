<?php
namespace SusRpp\Entity\Item;

/**
 * Subject Entity
 *
 * @property int      $subjectId
 * @property string   $subjectName
 * @property string   $coreCompetence
 * @property string[] $basicCompetence
 * @property string[] $indicator
 */
class SubjectItem extends \SusRpp\Entity\BaseEntity
{
    protected $subjectId;
    protected $subjectName;
    protected $coreCompetence;
    protected $basicCompetence;
    protected $indicator;
}
