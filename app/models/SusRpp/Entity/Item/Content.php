<?php
namespace SusRpp\Entity\Item;

/**
 * Content Entity (Item's $data)
 *
 * @property string $school
 * @property string $topic
 * @property string $theme
 * @property int    $class
 * @property int    $term
 * @property \SusRpp\Entity\Item\SubjectItem[] $relatedSubjects
 * @property string[] $goals
 * @property string   $materials
 * @property \SusRpp\Entity\Item\Activity $activities
 * @property string[] $resources
 * @property \SusRpp\Entity\Item\Assessment $assessment
 */
class Content extends \SusRpp\Entity\BaseEntity
{
    protected $school;
    protected $topic;
    protected $theme;
    protected $term;
    protected $class;
    protected $relatedSubjects;
    protected $goals;
    protected $materials;
    protected $activities;
    protected $resources;
    protected $assessment;
    protected $workAssessment;
    protected $observationAssessment;
}
