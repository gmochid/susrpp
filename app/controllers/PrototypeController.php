<?php

class PrototypeController extends BaseController
{
    public function showWelcome()
    {
        return View::make('prototype/home');
    }

    public function showDashboard()
    {
        return View::make('prototype/dashboard');
    }

    public function showViewer()
    {
        return View::make('prototype/viewer');
    }

    public function showEditor()
    {
        return View::make('prototype/editor');
    }

    public function showSearch()
    {
        return View::make('prototype/search');
    }

    public function showSearchResult()
    {
        return View::make('prototype/searchresult');
    }
}
