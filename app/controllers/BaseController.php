<?php

class BaseController extends Controller
{
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

    protected function useFormatting()
    {
        if (version_compare(phpversion(), '5.4.0', '>=')) {
            return \Input::get('formatting', 'on') == 'on' ? true : false;
        } else {
            return false;
        }
    }

    protected function makeJsonResponseFromEntityObject($obj)
    {
        $response = \Response::make($obj->toJson($this->useFormatting()), 200);
        $response->header('Content-Type', 'application/json');

        return $response;
    }

    protected function makeJsonResponseFromEntityObjectArray($arr)
    {
        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$i] instanceof \SusRpp\Entity\BaseEntity) {
                $arr[$i] = $arr[$i]->toArray();
            }
        }

        $content  = $this->useFormatting()
            ? json_encode($arr, JSON_PRETTY_PRINT) : json_encode($arr);
        $response = \Response::make($content, 200);
        $response->header('Content-Type', 'application/json');

        return $response;
    }
}
