<?php
namespace SusRpp\Controller;

use SusRpp\Service\SubjectService;
use SusRpp\Factory\SubjectFactory;

use SusRpp\Exception\SubjectNotFoundException;

class SubjectController extends \BaseController
{
    /**
     * #dependency
     * @var \SusRpp\Service\SubjectService
     */
    protected $subjectService;

    /**
     * #dependency
     * @var \SusRpp\Lib\Factory\SubjectFactory
     */
    protected $subjectFactory;

    /**
     * Constructor
     *
     * @param SubjectService $subjectService
     * @param SubjectFactory $subjectFactory
     */
    public function __construct(SubjectService $subjectService,
            SubjectFactory $subjectFactory)
    {
        $this->subjectService = $subjectService;
        $this->subjectFactory = $subjectFactory;

        $this->beforeFilter('controllerAuthFilter');
    }

    /**
     *
     */
    public function all()
    {
        $subjects = $this->subjectService->findAll();

        return $this->makeJsonResponseFromEntityObjectArray($subjects);
    }

    /**
     *
     */
    public function find()
    {
        $subjectName = \Input::get('subjectname', '8io*##s^afj8~ij!@@d');
        $subjects = $this->subjectService->findAllBySubjectName($subjectName);

        return $this->makeJsonResponseFromEntityObjectArray($subjects);
    }

    /**
     *
     */
    public function get($id)
    {
        try {
            $subject = $this->subjectService->findById($id);
        } catch (SubjectNotFoundException $e) {
            return \Response::json(array('error' => 'Subject not found.'), 404);
        }

        return $this->makeJsonResponseFromEntityObject($subject);
    }

    /**
     *
     */
    public function post()
    {
        $jsonData = \Request::instance()->getContent();

        try {
            $subject = $this->subjectFactory->fromJson($jsonData);
            $newSubject = $this->subjectService->createSubject($subject);
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return \Response::json(array('error' => "Couldn't create subject."), 400);
        }

        return \Response::json(array('success' => 'Subject created.', 'id' => $newSubject->_id));
    }

    /**
     *
     */
    public function update()
    {
        $jsonData = \Request::instance()->getContent();

        try {
            $subject = $this->subjectFactory->fromJson($jsonData);
            $this->subjectService->updateSubject($subject);
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return \Response::json(array('error' => "Couldn't update item"), 400);
        }

        return \Response::json(array('success' => 'Subject updated.', 'id' => $subject->_id));
    }

    /**
     *
     */
    public function remove($id)
    {
        try {
            $subject = $this->subjectService->findById($id);
            $this->subjectService->removeSubject($subject);
        } catch (SubjectNotFoundException $e) {
            return \Response::json(array('error' => 'Subject not found.'), 404);
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return \Response::json(array('error' => "Couldn't remove subject."), 400);
        }

        return \Response::json(array('success' => 'Subject has been deleted.'));
    }
}
