<?php
namespace SusRpp\Controller;

use SusRpp\Entity\User;

use SusRpp\Exception\BadCredentialsException;
use SusRpp\Exception\InvalidTokenException;
use SusRpp\Entity\Group;
use SusRpp\Service\UserService;
use SusRpp\Service\GroupService;
use SusRpp\Service\AuthService;
use SusRpp\Exception\UserNotFoundException;

class UserController extends \BaseController
{
    /**
     * #dependency
     * @var \SusRpp\Service\UserService
     */
    protected $userService;

    /**
     * #dependency
     * @var \SusRpp\Service\GroupService
     */
    protected $groupService;

    /**
     * #dependency
     * @var \SusRpp\Service\AuthService
     */
    protected $authService;

    /**
     * Constructor
     *
     * @param UserService $userService
     * @param GroupService $groupService
     * @param AuthService $authService
     */
    public function __construct(UserService $userService,
            GroupService $groupService,
            AuthService $authService)
    {
        $this->userService = $userService;
        $this->groupService = $groupService;
        $this->authService = $authService;

        $this->beforeFilter('controllerAuthFilter', array(
            'only' => array(
                'getUser', 'findUsersByGroup', 'updateUser', 'removeUser'
            ),
        ));
    }

    /**
     * Login. Only returns sesstoken
     */
    public function auth()
    {
        $username = \Input::get('username', null);
        $password = \Input::get('password', null);

        if ($username == null || $password == null) {
            return \Response::json(array('error' => "Username or password couldn't be blank."), 400);
        }

        try {
            $user = $this->authService->authenticate($username, $password);
        } catch (BadCredentialsException $e) {
            return \Response::json(array('error' => "Bad username/password."), 400);
        }

        return \Response::json(array('success' => 'Authentication success.', 'sesstoken' => $user->sesstoken));
    }

    /**
     * Login
     */
    public function authSession()
    {
        $username = \Input::get('username', null);
        $password = \Input::get('password', null);

        if ($username == null || $password == null) {
            return \Response::json(array('error' => "Username dan password tidak boleh kosong."), 400);
        }

        try {
            $user = $this->authService->authenticate($username, $password);
        } catch (BadCredentialsException $e) {
            return \Response::json(array('error' => "Terjadai Kesalahan pada username atau password."), 400);
        } catch (UserNotFoundException $e) {
            return \Response::json(array('error' => "User tidak ditemukan atau masih harus menunggu proses approval."), 400);
        }

        \Session::set('auth', $user->toArray());
        return \Response::json(array('success' => 'Authentication success.', 'sesstoken' => $user->sesstoken));
    }

    /**
     * Forget logged user session AKA logout
     */
    public function forgetSession()
    {
        if (\Session::has('auth')) {
            \Session::forget('auth');
            return \Response::json(array('success' => 'Auth session revoked.'));
        }

        return \Response::json(array('error' => 'Just fail. Period.'), 400);
    }

    /**
     * Validate a sesstoken
     */
    public function validateToken($token)
    {
        try {
            $this->authService->validateToken($token);
        } catch (InvalidTokenException $e) {
            return \Response::json(array('valid' => false));
        }

        return \Response::json(array('valid' => true));
    }

    /**
     * Gets user information
     */
    public function get($username = null)
    {
        // If $username == null, returns the logged user
        if ($username == null) {
            $sessAuth = \Session::get('auth');
            $username = $sessAuth['username'];
        }

        $user = $this->userService->findByUsername($username);
        if ($user == null) {
            return \Response::json(array('error' => 'User not found.'));
        }

        $userAssoc = $user->toArray();

        unset($userAssoc['password'], $userAssoc['sesstoken'], $userAssoc['items'], $userAssoc['role']);
        return \Response::json($userAssoc);
    }

    /**
     *
     */
    public function find()
    {
        $name = \Input::get('name', '');
        // Only approved user by default.
        $status = intval(\Input::get('status', '1'));

        $users = $this->userService->findUserByName($name, $status);

        $userAssocs = array();
        foreach ($users as $user) {
            $userAssoc = $user->toArray();
            unset($userAssoc['password'], $userAssoc['sesstoken'], $userAssoc['items']);

            $userAssocs[] = $userAssoc;
        }

        return $this->makeJsonResponseFromEntityObjectArray($userAssocs);
    }

    /**
     *
     * @param string $groupName
     */
    public function findAllByGroup($groupName)
    {
        $status = intval(\Input::get('status', '1'));

        try {
            $group = $this->groupService->findByName($groupName);
        } catch (\Exception $e) {
            return \Response::json(array('error' => 'Group not found.'), 404);
        }

        $users = $this->groupService->getUsersFromGroup($group, $status);

        $userAssocs = array();
        foreach ($users as $user) {
            $userAssoc = $user->toArray();
            unset($userAssoc['password'], $userAssoc['sesstoken'], $userAssoc['items']);

            $userAssocs[] = $userAssoc;
        }

        return $this->makeJsonResponseFromEntityObjectArray($userAssocs);
    }

    /**
     * Registers a user
     */
    public function register()
    {
        $json = \Request::instance()->getContent();
        $data = json_decode($json, true);

        $user = new User(array(
            'username' => $data['username'],
            'password' => $data['password'],
            'name'     => $data['name'],
            'email'    => $data['email'],
            'status'   => 0,
        ));

        if ($user->username == null ||
            $user->password == null ||
            $user->name     == null ||
            $user->email    == null) {
            return \Response::json(array('error' => 'Bad JSON data.'), 400);
        }

        $user->password = $this->userService->hashPassword($user->password);
        $user = $this->userService->createUser($user);

        // With groupName
        if (isset($data['groupName']) && $data['groupName'] != '') {
            $groupName = $data['groupName'];

            try {
                $group = $this->groupService->findByName($groupName);
            } catch (\Exception $e) {
                return \Response::json(array('error' => "Group {$groupName} not found."), 404);
            }

            $this->groupService->removeUserFromItsGroup($user);
            $this->groupService->addUserToGroup($user, $group);

            \Session::flash('register-success', "Registrasi anda berhasil! Harap mengunggu approval dari admin.");
        }

        return \Response::json(array('success' => 'User registration success.'));
    }

    /**
     * Updates a user
     */
    public function update()
    {
        $json = \Request::instance()->getContent();
        $data = json_decode($json, true);

        if (!isset($data['username'])) {
            return \Response::json(array('error' => 'Bad JSON data.'), 400);
        }

        $user = $this->userService->findByUsername($data['username']);
        if ($user == null) {
            return \Response::json(array('error' => 'Bad user.'), 400);
        }

        if (isset($data['password'])) {
            $user->password = $this->userService->hashPassword($data['password']);
        }
        if (isset($data['name'])) {
            $user->name = $data['name'];
        }
        if (isset($data['email'])) {
            $user->email = $data['email'];
        }
        if (isset($data['status'])) {
            $user->status = intval($data['status']);
        }

        $user = $this->userService->updateUser($user);
        return \Response::json(array('success' => 'User updated.'));
    }

    /**
     * Removes a user
     */
    public function remove($username)
    {
        $user = $this->userService->findByUsername($username);
        if ($user == null) {
            return \Response::json(array('error' => 'User not found.'));
        }

        $this->groupService->removeUserFromItsGroup($user);
        $this->userService->removeUser($user);
        return \Response::json(array('success' => 'User removed.'));
    }

    // Group specific APIs ----------------------------------------------------
    /**
     *
     */
    public function group_create()
    {
        $groupName = \Input::get('groupName', null);
        if ($groupName == null) {
            return \Response::json(array('error' => 'Bad data.'), 400);
        }

        $group = new Group(array(
                'groupName' => $groupName,
                'userIds' => array()
        ));

        $this->groupService->createGroup($group);
        return \Response::json(array('success' => 'Group creation success.'));
    }

    /**
     *
     */
    public function group_moveInUser()
    {
        $username = \Input::get('username', '');
        $groupName = \Input::get('groupname', '');

        try {
            $group = $this->groupService->findByName($groupName);
        } catch (\Exception $e) {
            return \Response::json(array('error' => "Group {$groupName} not found."), 404);
        }

        $user = $this->userService->findByUsername($username);
        if ($user == null) {
            return \Response::json(array('error' => "User {$username} not found."));
        }

        $this->groupService->removeUserFromItsGroup($user);
        $this->groupService->addUserToGroup($user, $group);
        return \Response::json(array('success' => 'User successfully moved to group "' . $group->groupName . '"'));
    }
}
