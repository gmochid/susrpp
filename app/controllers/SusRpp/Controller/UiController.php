<?php
namespace SusRpp\Controller;

class UiController extends \BaseController
{
    /**
     * Controller's constructor. Contains filters. No dependency.
     */
    public function __construct()
    {
        // Only accepts HTTP Session authentication.

        $this->beforeFilter(function() {
            if (!\Session::has('auth')) {
                return \Redirect::to('/login');
            }
        }, array('except' => array('showLogin', 'showRegister')));

        $this->beforeFilter(function() {
            if (\Session::has('auth')) {
                return \Redirect::to('/');
            }
        }, array('only' => array('showLogin', 'showRegister')));

        $this->beforeFilter(function() {
            $user = \Session::get('auth');
            if ($user['role'] == 'admin') {
                return \Redirect::to('/admin');
            }
        }, array('except' => array('showLogout')));
    }

    /**
     * Shows the RPP dashboard.
     */
    public function showRPPDashboard()
    {
        return \View::make('ui/dashboardRpp');
    }

    /**
     * Shows the dashboard. It is the default index ("/").
     */
    public function showDashboard()
    {
        return \View::make('ui/dashboard');
    }

    /**
     * Shows the editor ("/editor").
     */
    public function showEditor()
    {
        $action = \Input::get('action', 'new');
        $itemId = \Input::get('id', '');

        return \View::make('ui/editor', array(
            'action' => $action,
            'id' => $itemId,
        ));
    }

    /**
     * Shows the register form ("/register")
     */
    public function showRegister()
    {
        $groupService = \App::make('groupService');
        $groups = $groupService->findAll();

        return \View::make('ui/register', array(
            'groups' => $groups,
        ));
    }

    /**
     * Shows the login form ("/login").
     */
    public function showLogin()
    {
        return \View::make('ui/login');
    }

    /**
     * Shows the logout page ("/logout"). Logout done via AJAX request.
     */
    public function showLogout()
    {
        return \View::make('ui/logout');
    }

    /**
     * Shows the selected RPP ("/view").
     */
    public function showView($id)
    {
        return \View::make('ui/view', array(
            'action' => 'view', 'id' => $id
        ));
    }
}
