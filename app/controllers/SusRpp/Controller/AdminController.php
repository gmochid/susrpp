<?php
namespace SusRpp\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminController extends \BaseController
{
    public function __construct()
    {
        // Only accept HTTP Session authentication.

        $this->beforeFilter(function() {
            if (!\Session::has('auth')) {
                return \Redirect::to('/login');
            }
        });

        // ROLE == ADMIN
        $this->beforeFilter(function() {
            if (\Session::has('auth')) {
                $user = \Session::get('auth');

                if ($user['role'] != 'admin') {
                    throw new NotFoundHttpException();
                }
            }
        });
    }

    public function showAdminPage()
    {
        return \View::make('ui/admin');
    }
}
