<?php
namespace SusRpp\Controller;

use SusRpp\Service\UserService;
use SusRpp\Service\AuthService;
use SusRpp\Service\ItemService;
use SusRpp\Factory\ItemFactory;

use SusRpp\Exception\ItemNotFoundException;
use SusRpp\Exception\InvalidTokenException;

class RppController extends \BaseController
{
    /**
     * #dependency
     * @var \SusRpp\Service\UserService
     */
    protected $userService;

    /**
     * #dependency
     * @var \SusRpp\Service\AuthService
     */
    protected $authService;

    /**
     * #dependency
     * @var \SusRpp\Service\ItemService
     */
    protected $itemService;

    /**
     * #dependency
     * @var \SusRpp\Lib\Factory\ItemFactory
     */
    protected $itemFactory;

    /**
     * Constructor
     *
     * @param UserService $userService
     * @param AuthService $authService
     * @param ItemService $itemService
     * @param ItemFactory $itemFactory
     */
    public function __construct(UserService $userService,
            AuthService $authService,
            ItemService $itemService,
            ItemFactory $itemFactory)
    {
        $this->userService = $userService;
        $this->authService = $authService;
        $this->itemService = $itemService;
        $this->itemFactory = $itemFactory;

        $this->beforeFilter('controllerAuthFilter');
    }

    /**
     * Gets an item.
     */
    public function get($id = null)
    {
        try {
            $item = $this->itemService->findById($id);
        } catch (ItemNotFoundException $e) {
            return \Response::json(array('error' => 'Item not found.'), 404);
        }

        return $this->makeJsonResponseFromEntityObject($item);
    }

    /**
     * Posts (Creates) and item.
     */
    public function post()
    {
        $jsonData = \Request::instance()->getContent();

        try {
            $item = $this->itemFactory->fromJson($jsonData);
            $newItem = $this->itemService->createItem($item);
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return \Response::json(array('error' => "Couldn't create item."), 400);
        }

        return \Response::json(array('success' => 'Item created.', 'id' => $newItem->_id));
    }

    /**
     * Updates an item.
     */
    public function update()
    {
        $jsonData = \Request::instance()->getContent();

        try {
            $item = $this->itemFactory->fromJson($jsonData);
            $this->itemService->updateItem($item);
        } catch(\Exception $e) {
            \Log::error($e->getTraceAsString());
            return \Response::json(array('error' => "Couldn't update item."), 400);
        }

        return \Response::json(array('success' => 'Item updated.', 'id' => $item->_id));
    }

    /**
     * Removes an item.
     */
    public function remove($id)
    {
        try {
            $item = $this->itemService->findById($id);
            $this->itemService->removeItem($item);
        } catch (ItemNotFoundException $e) {
            return \Response::json(array('error' => 'Item not found.'), 404);
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return \Response::json(array('error' => "Couldn't remove item."), 400);
        }

        return \Response::json(array('success' => "Item has been deleted."));
    }

    /**
     * Find items by (author|description|tag|theme|title). Returns an array of item.
     */
    public function find()
    {
        $by    = \Input::get('by', '');
        $value = \Input::get('value', '');

        $items = array();

        if ($by == 'authorName' || $by == 'author') {
            $items = $this->itemService->findAllByAuthorName($value);
        }
        else if ($by == 'authorId') {
            $items = $this->itemService->findAllByAuthorId($value);
        }
        else if ($by == 'description') {
            $items = $this->itemService->findAllByDescription($value);
        }
        else if ($by == 'tag') {
            $items = $this->itemService->findAllByTag($value);
        }
        else if ($by == 'theme') {
            $items = $this->itemService->findAllByTheme($value);
        }
        else if ($by == 'title') {
            $items = $this->itemService->findAllByTitle($value);
        }
        else if ($by == 'groupName') {
            $items = $this->itemService->findAllByOwnerGroup($value);
        }

        return $this->makeJsonResponseFromEntityObjectArray($items);
    }

    /**
     * Find exactly one item, by (title)
     */
    public function findOne()
    {
        $by    = \Input::get('by', '');
        $value = \Input::get('value', '');

        if ($by == 'title') {
            try {
                $item = $this->itemService->findByTitle($value);
            } catch (ItemNotFoundException $e) {
                return \Response::json(array('error' => 'Item not found.'), 404);
            }
        } else {
            return \Response::json(array('error' => 'Item not found.'), 404);
        }

        return $this->makeJsonResponseFromEntityObject($item);
    }
}
