<?php
namespace SusRpp\Controller;

use SusRpp\Service\ItemService;
use SusRpp\Service\ResourceService;
use SusRpp\Factory\ResourceFactory;
use SusRpp\Entity\Resource;

use SusRpp\Exception\ResourceNotFoundException;

class ResourceController extends \BaseController
{
    /**
     * #dependency
     * @var \SusRpp\Lib\Factory\ItemService
     */
    protected $itemService;

	/**
     * #dependency
     * @var \SusRpp\Service\ResourceService
     */
    protected $resourceService;

    /**
     * #dependency
     * @var \SusRpp\Lib\Factory\ResourceFactory
     */
    protected $resourceFactory;

    const UPLOADS_FOLDER = 'uploads/';

    /**
     * Constructor
     *
     * @param ResourceService $resourceService
     * @param ResourceFactory $resourceFactory
     */
    public function __construct(ItemService $itemService,
            ResourceService $resourceService,
            ResourceFactory $resourceFactory)
    {
        $this->itemService = $itemService;
        $this->resourceService = $resourceService;
        $this->resourceFactory = $resourceFactory;

        $this->beforeFilter('controllerAuthFilter');
    }

    public function get($id = null)
    {
    	try {
            $resource = $this->resourceService->findById($id);
        } catch (ResourceNotFoundException $e) {
            return \Response::json(array('error' => 'Resource not found.'), 404);
        }

        return \Response::download(self::UPLOADS_FOLDER.$resource->storedFilename, $resource->originalFilename);
    }

    public function getBase64($id = null)
    {
        try {
            $resource = $this->resourceService->findById($id);
        } catch (ResourceNotFoundException $e) {
            return \Response::json(array('error' => 'Resource not found.'), 404);
        }

        $file = file_get_contents(self::UPLOADS_FOLDER.$resource->storedFilename);
        $fileBase64 = base64_encode($file);

        return \Response::json(array('base64' => $fileBase64));
    }

    public function post()
    {
        $itemId = \Input::get('itemId');
    	$file = \Input::file('resource');

        // check if itemId exist
        try {
            $this->itemService->findById($itemId);
        } catch (ItemNotFoundException $e) {
            return \Response::json(array('error' => 'Item not found.'), 404);
        }

        // build new resource
    	$resource = new Resource();
        $resource->itemId = $itemId;
    	$resource->originalFilename = $file->getClientOriginalName();
    	$resource->fileExtension = $file->getClientOriginalExtension();

    	// check existing filename
    	$resource->storedFilename = str_random(20);
    	while(file_exists($resource->storedFilename)) {
    		$resource->storedFilename = str_random(20);
    	}

        // create resource
    	try {
    		$newResources = $this->resourceService->createResource($resource);
    	} catch (Exception $e) {
    		\Log::error($e->getTraceAsString());
            return \Response::json(array('error' => "Couldn't create resource."), 400);
    	}

    	// move file to upload directory
    	$file->move(self::UPLOADS_FOLDER, $resource->storedFilename);

    	return \Response::json(array('success' => 'Resource saved', 'id' => $newResources->_id));
    }

    public function update($id = null)
    {
    	try {
            $resource = $this->resourceService->findById($id);
        } catch (ResourceNotFoundException $e) {
            return \Response::json(array('error' => 'Resource not found.'), 404);
        }

        // update original filename
        $file = \Input::file('resource');
        $resource->originalFilename = $file->getClientOriginalName();
        $resource->fileExtension = $file->getClientOriginalExtension();

        // apply update to database
        try {
    		$this->resourceService->updateResource($resource);
    	} catch (Exception $e) {
    		\Log::error($e->getTraceAsString());
            return \Response::json(array('error' => "Couldn't update resource."), 400);
    	}

    	// move file to upload directory
    	$file->move(self::UPLOADS_FOLDER, $resource->storedFilename);

    	return \Response::json(array('success' => 'Resource saved', 'id' => $resource->_id));
    }

}
