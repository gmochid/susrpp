<?php
namespace SusRpp\Controller;

use SusRpp\Factory\CommentFactory;
use SusRpp\Service\CommentService;
use SusRpp\Entity\Comment;

class CommentController extends \BaseController
{
    /**
     * #dependency
     * @var CommentService
     */
    protected $commentService;

    /**
     * #dependency
     * @var CommentFactory
     */
    protected $commentFactory;

    public function __construct(CommentService $commentService,
        CommentFactory $commentFactory)
    {
        $this->commentService = $commentService;
        $this->commentFactory = $commentFactory;

        $this->beforeFilter('controllerAuthFilter');
    }

    public function findByItemId($itemId)
    {
        $comments = $this->commentService->findAllByItemId($itemId);

        return $this->makeJsonResponseFromEntityObjectArray($comments);
    }

    public function get($id)
    {
        try {
            $comment = $this->commentService->findById($id);
        } catch (CommentNotFoundException $e) {
            return \Response::json(array('error' => 'Comment not found.'), 404);
        }

        return $this->makeJsonResponseFromEntityObject($comment);
    }

    public function post()
    {
        $comment = new Comment();

        $itemId = \Input::get('itemId', null);
        $commentMessage = \Input::get('comment', '');

        $user = \Session::get('auth');

        $comment->commenterUsername = $user['username'];
        $comment->commenterName     = $user['name'];
        $comment->itemId  = $itemId;
        $comment->comment = $commentMessage;

        try {
            $newComment = $this->commentService->createComment($comment);
        } catch (\Exception $e) {
            return \Response::json(array('error' => "Couldn't create comment."));
        }

        return \Response::json(array('success' => 'Comment created.', 'id' => $newComment->_id));
    }

    public function delete($id)
    {
        try {
            $comment = $this->commentService->findById($id);
            $this->commentService->removeCommment($comment);
        } catch (CommentNotFoundException $e) {
            return \Response::json(array('error' => 'Comment not found.'), 404);
        } catch (\Exception $e) {
            return \Response::json(array('error' => "Couldn't remove comment"), 400);
        }

        return \Response::json(array('success' => "Comment has been deleted."));
    }
}
