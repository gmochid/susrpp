<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {

    public static function logEcho($logMsg, $display = true, $logId = "")
    {
        if ($display) {
            echo "\n--- LOG \"{$logId}\" ---\n";
            echo $logMsg;
            echo "\n--- END LOG ---\n";
        }
    }

    public static function logDump($logItem, $display = true, $logId = "")
    {
        if ($display) {
            echo "\n--- LOG {$logId}---\n";
            var_dump($logItem);
            echo "\n--- END LOG ---\n";
        }
    }

    /**
     * Creates the application.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $unitTesting = true;
        $testEnvironment = 'testing';

        $app = require __DIR__.'/../../bootstrap/start.php';

        \Config::set('app.susenv', $testEnvironment);
        return $app;
    }

}
