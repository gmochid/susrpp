<?php
namespace SusRpp\Test\Controller;

class RppControllerTest extends \TestCase
{
    /**
     * @var \SusRpp\Lib\Factory\ItemFactory
     */
    protected $itemFactory;

    public function setUp()
    {
        parent::setUp();
        \Route::enableFilters();
        \Session::set('auth', array());
        $this->itemFactory = \App::make('itemFactory');
    }

    public function testGetNoItem()
    {
        $this->client->request('GET', '/api/item/NoItemWithIdLikeThis');
        $this->assertTrue($this->client->getResponse()->isNotFound());

        $content = json_decode($this->client->getResponse()->getContent());
        $this->assertTrue(array_key_exists('error', $content));
    }

    public function testGetSeededItem()
    {
        // Find by title
        $this->client->request('GET', '/api/item/findone?by=title&value=Pemanasan Global');
        $this->assertTrue($this->client->getResponse()->isOk());

        $item = $this->itemFactory->fromJson($this->client->getResponse()->getContent());
        $this->assertEquals('Pemanasan Global', $item->title);
        $this->assertNotNull($item->_id);

        // Find by id
        $id = $item->_id;
        $this->client->request('GET', "/api/item/{$id}");
        $this->assertTrue($this->client->getResponse()->isOk());

        $itemById = $this->itemFactory->fromJson($this->client->getResponse()->getContent());
        $this->assertEquals($item->title, $itemById->title);
        $this->assertEquals($item->_id, $itemById->_id);
    }

    /**
     * @depends testGetSeededItem
     */
    public function testPostItem()
    {
        $this->client->request('GET', '/api/item/find?by=author&value=frodo');
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertGreaterThanOrEqual(1, count($content));

        $this->client->request('GET', '/api/item/findone?by=title&value=Pemanasan Global');
        $item = $this->itemFactory->fromJson($this->client->getResponse()->getContent());

        $item->title = 'New Item Foo Bar';
        $newItemArray = $item->toArray();
        unset($newItemArray['_id']);

        $newItemJson = json_encode($newItemArray);
        $this->client->request('POST', '/api/item', array(), array(), array(), $newItemJson);
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(array_key_exists('success', $content));
        $this->assertTrue(array_key_exists('id', $content));
        static::logDump($content, false);

        $id = $content['id'];
        return $id;
    }

    /**
     * @depends testPostItem
     */
    public function testUpdateItem($id)
    {
        // Get item
        $this->client->request('GET', "/api/item/{$id}");
        $this->assertTrue($this->client->getResponse()->isOk());

        // Update item
        $item = $this->itemFactory->fromJson($this->client->getResponse()->getContent());
        $item->title = 'Updated New Item!';

        $this->client->request('PUT', "/api/item", array(), array(), array(), $item->toJson());
        $this->assertTrue($this->client->getResponse()->isOk());

        // Get item again
        $this->client->request('GET', "/api/item/{$id}");
        $this->assertTrue($this->client->getResponse()->isOk());

        $item = $this->itemFactory->fromJson($this->client->getResponse()->getContent());
        $this->assertEquals('Updated New Item!', $item->title);

        return $id;
    }

    /**
     * @depends testUpdateItem
     */
    public function testRemoveItem($id)
    {
        $this->client->request('GET', "/api/item/{$id}");
        $this->assertTrue($this->client->getResponse()->isOk());

        $this->client->request('DELETE', "/api/item/{$id}");
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(array_key_exists('success', $content));
        static::logDump($content, false);

        $this->client->request('GET', "/api/item/{$id}");
        $this->assertTrue($this->client->getResponse()->isNotFound());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(array_key_exists('error', $content));
    }
}
