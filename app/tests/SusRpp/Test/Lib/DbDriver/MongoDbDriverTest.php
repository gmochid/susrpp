<?php
namespace SusRpp\Test\Lib\DbDriver;

use SusRpp\Lib\DbDriver\MongoDbDriver;

class MongoDbDriverTest extends \TestCase
{
    public function testConfiguration()
    {
        $this->assertEquals('testing', \Config::get('app.susenv'));
        $this->assertNotNull(\Config::get('mongodbdriver.testing.connection_string'));
        $this->assertNotNull(\Config::get('mongodbdriver.testing.database'));
    }

    /**
     * @depends testConfiguration
     */
    public function testWrongConnection()
    {
        $this->setExpectedException('\MongoConnectionException');

        // This should throws a MongoConnectionException
        $dbDriver = new MongoDbDriver('mongodb://noserver:12345', 'nodb');
    }

    /**
     * @depends testConfiguration
     */
    public function testAppMakeDbDriver()
    {
        $dbDriver = \App::make('mongoDbDriver');

        $this->assertNotNull($dbDriver);
        $this->assertTrue($dbDriver->getMongoClient()->connected);
    }
}
