<?php
namespace SusRpp\Test\Service;

use SusRpp\Entity\User;
use SusRpp\Entity\Group;
use SusRpp\Entity\Comment;

class CommentServiceTest extends \TestCase
{
    /**
     * @var \SusRpp\Service\CommentService
     */
    protected $commentService;
    /**
     * @var \SusRpp\Service\UserService
     */
    protected $userService;
    /**
     * @var \SusRpp\Service\ItemService
     */
    protected $itemService;

    public function setUp()
    {
        $this->commentService = \App::make('commentService');
        $this->userService = \App::make('userService');
        $this->itemService = \App::make('itemService');
    }

    public function testNotNull()
    {
        $this->assertNotNull($this->commentService);
        $this->assertNotNull($this->userService);
        $this->assertNotNull($this->itemService);
    }

    /**
     * @depends testNotNull
     */
    public function testAll()
    {
        $this->assertTrue(true);

        $user = $this->userService->findByUsername("frodo");
        //$this->logDump($user);

        $items = $this->itemService->findAllByAuthorId($user->username);
        $item = $items[0];

        $itemId = $item->_id;

        // Comment 1
        $comment1 = new Comment();
        $comment1->commenterUsername = $user->username;
        $comment1->commenterName = $user->name;
        $comment1->itemId = $itemId;
        $comment1->comment = "Hello, world! This is first comment!";
        $comment1 = $this->commentService->createComment($comment1);
        $this->assertNotNull($comment1->_id);

        // Comment 2
        $comment2 = new Comment();
        $comment2->commenterUsername = $user->username;
        $comment2->commenterName = $user->name;
        $comment2->itemId = $itemId;
        $comment2->comment = "The quick brown fox jumps over the lazy dog. Second comment.";
        $comment2 = $this->commentService->createComment($comment2);
        $this->assertNotNull($comment2->_id);

        // Comment 3
        $comment3 = new Comment();
        $comment3->commenterUsername = $user->username;
        $comment3->commenterName = $user->name;
        $comment3->itemId = $itemId;
        $comment3->comment = "THIRD COMMENT!";
        $comment3 = $this->commentService->createComment($comment3);
        $this->assertNotNull($comment3->_id);

        $comments = $this->commentService->findAllByItemId($itemId);
        $this->assertEquals(3, count($comments));
        $this->assertEquals("The quick brown fox jumps over the lazy dog. Second comment.", $comments[1]->comment);

        $this->commentService->removeCommment($comment1);
        $this->commentService->removeCommment($comment2);
        $this->commentService->removeCommment($comment3);

        $commentsx = $this->commentService->findAllByItemId($itemId);
        $this->assertEquals(0, count($commentsx));
    }
}
