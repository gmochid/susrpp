<?php
namespace SusRpp\Test\Service;

use SusRpp\Entity\User;

class UserServiceTest extends \TestCase
{
    /**
     * @var \SusRpp\Service\UserService
     */
    protected $userService;

    public function setUp()
    {
        $this->userService = \App::make('userService');
    }

    public function testNotNull()
    {
        $this->assertNotNull($this->userService);
    }

    /**
     * @depends testNotNull
     */
    public function testCreateAndRetrieve()
    {
        $user = new User(array(
            'username' => 'foobar',
            'password' => 'qux',
            'name'     => 'Foo Bar',
            'email'    => 'foobar@example.com',
        ));

        $this->assertNull($user->sesstoken);
        $this->userService->createUser($user);

        $user = $this->userService->findByUsername('foobar');
        $this->assertEquals('foobar', $user->username);
        $this->assertNotNull($user->sesstoken);

        static::logEcho($user->sesstoken, false, "User's sesstoken");

        $this->userService->removeUser($user);

        $this->setExpectedException('\SusRpp\Exception\UserNotFoundException');

        try {
            $user = $this->userService->findByUsername('foobar');
        } catch (UserNotFoundException $e) {
            $this->assertInstanceOf('\SusRpp\Exception\UserNotFoundException', $e);
            $this->assertNull($user);
        }
    }

    /**
     * @depends testNotNull
     */
    public function testRetrieveSeededUser()
    {
        $user = $this->userService->findByUsername('frodo');
        $this->assertNotNull($user);
    }

    /**
     * @depends testRetrieveSeededUser
     */
    public function testRetrieveUserWithItems()
    {
        $user = $this->userService->findByUsernameWithItems('frodo');
        $this->assertTrue(count($user->items) >= 2);

        static::logEcho($user->toJson(true), false);

        $users = $this->userService->findUserByName('son of Thranduil');
        $this->assertEquals('legolas', $users[0]->username);
    }

    /**
     * @depends testRetrieveSeededUser
     */
    public function testUpdate()
    {
        $user = $this->userService->findByUsername('frodo');
        $this->assertNotNull($user);

        $originalName = $user->name;

        $user->name = 'Sam';
        $this->userService->updateUser($user);

        $user = $this->userService->findByUsername('frodo');
        $this->assertEquals('Sam', $user->name);

        $user->name = $originalName;
        $this->userService->updateUser($user);

        $user = $this->userService->findByUsername('frodo');
        $this->assertEquals($originalName, $user->name);
    }
}
