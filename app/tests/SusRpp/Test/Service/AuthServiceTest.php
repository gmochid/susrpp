<?php
namespace SusRpp\Test\Service;

use SusRpp\Entity\User;
use SusRpp\Exception\BadCredentialsException;
use SusRpp\Exception\InvalidTokenException;

class AuthServiceTest extends \TestCase
{
    /**
     * @var \SusRpp\Service\UserService
     */
    protected $userService;
    /**
     * @var \SusRpp\Service\AuthService
     */
    protected $authService;

    public function setUp()
    {
        $this->userService = \App::make('userService');
        $this->authService = \App::make('authService');
    }

    public function testNotNull()
    {
        $this->assertNotNull($this->userService);
        $this->assertNotNull($this->authService);
    }

    /**
     * @depends testNotNull
     */
    public function testAuthenticateNewUser()
    {
        // Create new user
        $user = new User(array(
            'username' => 'foobar',
            'password' => 'qux',
            'name'     => 'Foo Bar',
            'email'    => 'foobar@example.com',
        ));

        $this->assertNull($user->sesstoken);
        $this->userService->createUser($user);

        // Authenticate user
        $user = $this->authService->authenticate('foobar', 'qux');
        $this->assertNotNull($user);
        $this->assertNotNull($user->sesstoken);

        // Token Validation
        $validtoken = $this->authService->validateToken($user->sesstoken);
        $this->assertEquals($user->sesstoken, $validtoken);

        try {
            $validtoken = $this->authService->validateToken('ThisIsInvalidToken');
        } catch (InvalidTokenException $e) {
            $validtoken = null;
            $this->assertInstanceOf('\SusRpp\Exception\InvalidTokenException', $e);
        }
        $this->assertNull($validtoken);

        // Bad credentials
        try {
            $user = $this->authService->authenticate('foobar', 'wrongpassword');
        } catch (BadCredentialsException $e) {
            $user = null;
            $this->assertInstanceOf('\SusRpp\Exception\BadCredentialsException', $e);
        }
        $this->assertNull($user);

        // Remove 'foobar'
        $user = $this->userService->findByUsername('foobar');
        $this->userService->removeUser($user);

        $this->setExpectedException('\SusRpp\Exception\UserNotFoundException');

        try {
            $user = $this->userService->findByUsername('foobar');
        } catch (UserNotFoundException $e) {
            $this->assertInstanceOf('\SusRpp\Exception\UserNotFoundException', $e);
            $this->assertNull($user);
        }
    }
}
