<?php
namespace SusRpp\Test\Service;

use SusRpp\Entity\Item;
use SusRpp\Entity\Item\Content;
use SusRpp\Entity\Item\Activity;
use SusRpp\Entity\Item\Assessment;

class ItemServiceTest extends \TestCase
{
    /**
     * @var \SusRpp\Service\ItemService
     */
    protected $itemService;

    public function setUp()
    {
        $this->itemService = \App::make('itemService');
    }

    public function testNotNull()
    {
        $this->assertNotNull($this->itemService);
    }

    /**
     * @depends testNotNull
     */
    public function testCreateRetrieveAndRemove()
    {
        $item = new Item(array(
            'title'       => 'TestCreate',
            'status'      => 'draft',
            'authors'     => array(),
            'description' => 'funny',
            'lookup'      => 'funny',
            'tags'        => array('funny'),
            'data'        => new Content(array(
                'relatedSubjects' => array(),
                'activities' => new Activity(array(
                    'startActivities' => array(),
                    'coreActivities' => array(),
                    'closingActivities' => array(),
                )),
                'assessment' => new Assessment(array(
                    'multipleChoice' => array(),
                    'essay' => array(),
                    'simpleQuiz' => array(),
                    'matching' => array(),
                )),
            )),
        ));

        $item = $this->itemService->createItem($item);
        $this->assertNotNull($item->_id);

        $item = $this->itemService->findById($item->_id);
        $this->assertNotNull($item);
        $this->assertEquals($item->status, 'draft');

        $this->itemService->removeItem($item);

        $this->setExpectedException('\SusRpp\Exception\ItemNotFoundException');
        $item = $this->itemService->findByTitle('TestCreate');
    }

    /**
     * @depends testNotNull
     */
    public function testSeededData()
    {
        $item = $this->itemService->findByTitle('Pemanasan Global');
        $this->assertNotNull($item);

        static::logEcho($item->toJson(true), false);
    }

    /**
     * @depends testSeededData
     */
    public function testFindByAuthor()
    {
        $items = $this->itemService->findAllByAuthorId('frodo');
        $this->assertGreaterThanOrEqual(2, count($items));

        $this->assertNotNull($items[0]->_id);
        $this->assertNotNull($items[1]->_id);

        static::logDump($items, false);

        $items = $this->itemService->findAllByAuthorName('NotFoundNotFoundNotFound');
        $this->assertEquals(0, count($items));
    }

    /**
     * @depends testSeededData
     */
    public function testFindByTag()
    {
        $items = $this->itemService->findAllByTag('testitem');
        $this->assertGreaterThanOrEqual(3, count($items));

        $items = $this->itemService->findAllByTag('there is no tag like this');
        $this->assertEquals(0, count($items));
    }

    /**
     * @depends testSeededData
     */
    public function testFindByDescription()
    {
        $items = $this->itemService->findAllByDescription('two');
        $this->assertGreaterThanOrEqual(1, count($items));
    }

    /**
     * @depends testSeededData
     */
    public function testFindByTheme()
    {
        $items = $this->itemService->findAllByTheme('Pemanasan Global');
        $this->assertGreaterThanOrEqual(1, count($items));
    }

    /**
     * @depends testSeededData
     */
    public function testUpdate()
    {
        $item = $this->itemService->findByTitle('Pemanasan Global');
        $this->assertNotNull($item->description);

        $originalDescription = $item->description;

        $item->description = "Description changed!";
        $this->itemService->updateItem($item);

        $item = $this->itemService->findByTitle('Pemanasan Global');
        $this->assertEquals("Description changed!", $item->description);

        $item->description = $originalDescription;
        $this->itemService->updateItem($item);

        $item = $this->itemService->findByTitle('Pemanasan Global');
        $this->assertEquals($originalDescription, $item->description);
    }
}
