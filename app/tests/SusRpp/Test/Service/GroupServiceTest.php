<?php
namespace SusRpp\Test\Service;

use SusRpp\Entity\User;
use SusRpp\Entity\Group;

class GroupServiceTest extends \TestCase
{
    /**
     * @var \SusRpp\Service\GroupService
     */
    protected $groupService;
    /**
     * @var \SusRpp\Service\UserService
     */
    protected $userService;

    public function setUp()
    {
        $this->groupService = \App::make('groupService');
        $this->userService = \App::make('userService');
    }

    public function testNotNull()
    {
        $this->assertNotNull($this->groupService);
        $this->assertNotNull($this->userService);
    }

    /**
     * @depends testNotNull
     */
    public function testCreateAndRetrieve()
    {
        $group = new Group(array(
            'groupName' => 'tester',
            'userIds' => array('legolas'),
        ));

        $group = $this->groupService->createGroup($group);
        $this->assertNotNull($group->_id);

        static::logEcho($group->_id, false, "Group Id");

        $gandalf = $this->userService->findByUsername("gandalf");
        $this->assertNotNull($gandalf);

        $this->groupService->addUserToGroup($gandalf, $group);

        $group = $this->groupService->findByName("tester");
        $this->assertNotNull($group);
        $this->assertEquals("tester", $group->groupName);

        $this->assertContains("legolas", $group->userIds);
        $this->assertContains("gandalf", $group->userIds);

        $gandalf = $this->userService->findByUsername("gandalf");
        $this->assertNotNull($gandalf->groupId);
        $this->assertEquals($gandalf->groupId, $group->_id);

        $this->groupService->removeGroup($group);

        $this->setExpectedException('\SusRpp\Exception\GroupNotFoundException');
        $group = $this->groupService->findByName("tester");
    }

    /**
     * @depends testCreateAndRetrieve
     */
    public function testSeededData()
    {
        $group = $this->groupService->findByName("Fellowship of The Ring");
        $this->assertNotNull($group);

        $this->assertContains("legolas", $group->userIds);

        $users = $this->groupService->getUsersFromGroup($group);
        $this->assertTrue(count($users) > 3);
    }
}
