<?php
namespace SusRpp\Test\Service;

use SusRpp\Entity\Item;
use SusRpp\Entity\Resource;

class ResourceServiceTest extends \TestCase
{
	/**
     * @var \SusRpp\Service\ResourceService
     */
    protected $resourceService;
    /**
     * @var \SusRpp\Service\ItemService
     */
    protected $itemService;

    public function setUp()
    {
        $this->resourceService = \App::make('resourceService');
        $this->itemService = \App::make('itemService');
    }

    public function testNotNull()
    {
        $this->assertNotNull($this->resourceService);
        $this->assertNotNull($this->itemService);
    }

	/**
     * @depends testNotNull
     */
    public function testCreateDeleteResource()
    {
    	// create resource
    	$resource = new Resource();
    	$resource->itemId = 'itemId';
    	$resource->originalFilename = 'original.orig';
    	$resource->fileExtension = 'orig';
    	$resource->storedFilename = str_random(20);
    	$resource = $this->resourceService->createResource($resource);

    	// check whether resource created
    	$this->assertNotNull($resource->_id);

    	// delete resource
    	$deleteOk = $this->resourceService->removeResource($resource);

    	// check whether resource deleted
    	$this->assertTrue($deleteOk);
    }

    /**
     * @depends testFindIdResource
     */
    public function testUpdateResource()
    {
    	// create resource
    	$resource = new Resource();
    	$resource->itemId = 'itemId';
    	$resource->originalFilename = 'original.orig';
    	$resource->fileExtension = 'orig';
    	$resource->storedFilename = str_random(20);
    	$resource = $this->resourceService->createResource($resource);

    	// update resource
    	$resource->originalFilename = 'test.res';
    	$resource->fileExtension = 'res';
    	$updateOk = $this->resourceService->updateResource($resource);

    	// retrieve updated resource

    	// check whether resource updated and id not changed
    	$this->assertTrue($updateOk);
    	$this->assertEquals($id, $resource->_id);
    	
    	// delete resource
    	$this->resourceService->removeResource($resource);
    }

    /**
     * @depends testCreateDeleteResource
     */
    public function testFindIdResource()
    {
    	// create resource
    	$resource = new Resource();
    	$resource->itemId = 'itemId';
    	$resource->originalFilename = 'original.orig';
    	$resource->fileExtension = 'orig';
    	$resource->storedFilename = str_random(20);
    	$resource = $this->resourceService->createResource($resource);

    	// find the created resource
    	$foundResource = $this->resourceService->findById($resource->_id);

    	// check wheter it is the same resource
    	$this->assertEquals($resource->_id, $resource->_id);
    	$this->assertEquals($resource->itemId, $resource->itemId);
    	$this->assertEquals($resource->originalFilename, $resource->originalFilename);
    	$this->assertEquals($resource->storedFilename, $resource->storedFilename);
    	$this->assertEquals($resource->fileExtension, $resource->fileExtension);

    	// delete resource
    	$this->resourceService->removeResource($resource);
    }

    /**
     * @depends testCreateDeleteResource
     */
    public function testFindItemIdResource()
    {
    	$resources = array();

    	// create resource 1
    	$resources[0] = new Resource();
    	$resources[0]->itemId = 'itemId';
    	$resources[0]->originalFilename = 'original.orig';
    	$resources[0]->fileExtension = 'orig';
    	$resources[0]->storedFilename = 'filename1';
    	$resources[0] = $this->resourceService->createResource($resources[0]);

    	// create resource 2
    	$resources[1] = new Resource();
    	$resources[1]->itemId = 'itemId';
    	$resources[1]->originalFilename = 'image.img';
    	$resources[1]->fileExtension = 'img';
    	$resources[1]->storedFilename = 'filename2';
    	$resources[1] = $this->resourceService->createResource($resources[1]);

    	// create resource 3
    	$resources[2] = new Resource();
    	$resources[2]->itemId = 'itemId';
    	$resources[2]->originalFilename = 'doc.docx';
    	$resources[2]->fileExtension = 'docx';
    	$resources[2]->storedFilename = 'filename3';
    	$resources[2] = $this->resourceService->createResource($resources[2]);

    	// find the created resource
    	$foundResources = $this->resourceService->findAllByItemId('itemId');
    	
    	// check
    	for ($i=0; $i < 3; $i++) { 
    		$this->assertEquals($resources[$i]->_id, $foundResources[$i]->_id);
    		$this->assertEquals($resources[$i]->itemId, $foundResources[$i]->itemId);
    		$this->assertEquals($resources[$i]->originalFilename, $foundResources[$i]->originalFilename);
    		$this->assertEquals($resources[$i]->fileExtension, $foundResources[$i]->fileExtension);
    		$this->assertEquals($resources[$i]->storedFilename, $foundResources[$i]->storedFilename);
    	}

    	// delete resource
    	$this->resourceService->removeResource($resources[0]);
    	$this->resourceService->removeResource($resources[1]);
    	$this->resourceService->removeResource($resources[2]);
    }
}
