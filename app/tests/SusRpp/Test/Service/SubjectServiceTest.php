<?php
namespace SusRpp\Test\Service;

use SusRpp\Entity\Subject;
use SusRpp\Entity\Subject\Competence;

class SubjectServiceTest extends \TestCase
{
    /**
     * @var \SusRpp\Service\SubjectService
     */
    protected $subjectService;

    public function setUp()
    {
        $this->subjectService = \App::make('subjectService');
    }

    public function testSeededSubjects()
    {
        $subjects = $this->subjectService->findAll();

        $this->assertGreaterThan(0, count($subjects));
        $this->assertNotNull($subjects[0]->subjectName);
        $this->assertNotNull($subjects[1]->subjectName);

        static::logDump($subjects, false);
        static::logEcho($subjects[0]->toJson(true), false);
        static::logEcho($subjects[1]->toJson(true), false);
    }

    /**
     * @depends testSeededSubjects
     */
    public function testUpdateSubject()
    {
        $subject = $this->subjectService->findBySubjectName('matematika');
        $this->assertNotNull($subject);
        $this->assertNotNull($subject->_id);
        $this->assertEquals('Matematika', $subject->subjectName);

        $subjectId = $subject->_id;
        $subject->subjectName = 'Hello, world!';
        $coreComp = $subject->coreCompetences;
        $originalCoreCompDesc = $coreComp[0]->description;
        $coreComp[0]->description = 'Changed!';

        $subject->coreCompetences = $coreComp;
        $this->subjectService->updateSubject($subject);

        $subject = $this->subjectService->findById($subjectId);
        $this->assertEquals('Hello, world!', $subject->subjectName);
        $this->assertEquals('Changed!', $subject->coreCompetences[0]->description);

        $subject->subjectName = 'Matematika';
        $coreComp = $subject->coreCompetences;
        $coreComp[0]->description = $originalCoreCompDesc;
        $subject->coreCompetences = $coreComp;
        $this->subjectService->updateSubject($subject);

        $subject = $this->subjectService->findById($subjectId);
        $this->assertEquals('Matematika', $subject->subjectName);
        $this->assertEquals($originalCoreCompDesc, $subject->coreCompetences[0]->description);
    }
}
