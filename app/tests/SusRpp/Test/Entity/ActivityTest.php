<?php
namespace SusRpp\Test\Entity;

use SusRpp\Entity\Item\Activity;
use SusRpp\Entity\Item\ActivityItem;

class ActivityTest extends \TestCase
{
    protected static $activity;

    protected static $startActivities;
    protected static $coreActivities;
    protected static $closingActivities;

    public static function setUpBeforeClass()
    {
        self::$activity = new Activity(array(
            "learnApproach" => "Music",
            "learnMethods" => array("Using musical instruments", "Singing together"),
            "activity" => "Learning guitar",
            "startDuration" => 30,
            "coreDuration" => 60,
            "closingDuration" => 30,
        ));
    }

    public function testCreateActivity()
    {
        $this->assertEquals("Music", self::$activity->learnApproach);
        $this->assertEquals("Singing together", self::$activity->learnMethods[1]);
        $this->assertEquals(30, self::$activity->startDuration);
        $this->assertEquals(60, self::$activity->coreDuration);
        $this->assertEquals(30, self::$activity->closingDuration);

        $this->assertEquals("Learning guitar", self::$activity->activity);

        static::logDump(self::$activity, false);
    }

    /**
     * @depends testCreateActivity
     */
    public function testToArray()
    {
        $activityArray = self::$activity->toArray();
        $activityJson = self::$activity->toJson(true);

        $this->assertNotNull($activityArray);
        $this->assertNotNull($activityJson);

        $decoded = json_decode($activityJson, true);

        static::logDump($activityArray, false);
        static::logEcho($activityJson, false, "toArray's JSON");
        static::logDump(json_decode($activityJson, true), false);
    }
}
