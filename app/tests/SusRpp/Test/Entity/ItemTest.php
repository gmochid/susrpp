<?php
namespace SusRpp\Test\Entity;

use SusRpp\Entity\Item;
use SusRpp\Entity\Item\Content;
use SusRpp\Entity\Item\SubjectItem;

use SusRpp\Entity\Item\Activity;
use SusRpp\Entity\Item\ActivityItem;

use SusRpp\Entity\Item\Assessment;
use SusRpp\Entity\Item\Assessment\MultipleChoiceAssessmentItem;
use SusRpp\Entity\Item\Assessment\EssayAssessmentItem;
use SusRpp\Entity\Item\Assessment\SimpleQuizAssessmentItem;
use SusRpp\Entity\Item\Assessment\MatchingQuizAssessmentItem;

class ItemTest extends \TestCase
{
    /**
     * @var \SusRpp\Entity\Item
     */
    public static $item;

    /**
     * @var \SusRpp\Entity\Item\Content
     */
    public static $content;

    /**
     * @var \SusRpp\Entity\Item\SubjectItem[]
     */
    public static $relatedSubjects;

    /**
     * @var \SusRpp\Entity\Item\Activity[]
     */
    public static $activities;

    /**
     * @var \SusRpp\Entity\Item\Assessment
     */
    public static $assessment;

    // ------------------------------------------------------------------------

    public static function createRelatedSubject()
    {
        self::$relatedSubjects = array(
            new SubjectItem(array(
                "subjectName" => "Programming",
                "coreCompetence" => "Functional and Imperative Programming",
                "basicCompetence" => array(
                        "Algorithm", "Data Structure",
                ),
                "indicator" => array(
                        "Small Tasks", "Final Project",
                ),
            )),
        );
    }

    public static function createActivities()
    {
        self::$activities = new Activity(array(
            "learnApproach" => "Music",
            "learnMethods" => array("Using musical instruments", "Singing together"),
            "activity" => "Learning guitar",
            "startDuration" => 30,
            "coreDuration" => 30,
            "closingDuration" => 30,
        ));
    }

    public static function createAssessment()
    {
        $multipleChoices = array(
            new MultipleChoiceAssessmentItem(array(
                "indicator" => "Know what is Foo Bar",
                "question"  => "What is Foo Bar?",
                "options"   => array("Nothing", "A superhero", "A madman", "Ghost"),
                "solution"  => "A superhero",
                "score"     => 50,
            )),
            new MultipleChoiceAssessmentItem(array(
                "indicator" => "Able to answer \"how are you\" question",
                "question"  => "How are you?",
                "options"   => array("Fine, thanks", "What?", "I don't understand", "WTF?"),
                "solution"  => "Fine, thanks",
                "score"     => 50,
            )),
        );

        $essays = array(
            new EssayAssessmentItem(array(
                "indicator" => "Understand the Quicksort Algorithm",
                "question"  => "Explain quicksort algorithm and write the functional pseudocode of it!",
                "solutions" => array(
                    array("keyPoint" => "Word explanation", "score" => 30),
                    array("keyPoint" => "Pseudocode", "score" => 50),
                    array("keyPoint" => "Algorithm Correctness", "score" => 20),
                )
            )),
        );

        $simpleQuizes = array(
            new SimpleQuizAssessmentItem(array(
                "indicator" => "Know the capital of Slovenia",
                "question" => "What is the capital of Slovenia?",
                "solution" => "Ljubljana",
                "score" => 100,
            )),
        );

        $matchingQuizes = array(
            new MatchingQuizAssessmentItem(array(
                "indicator" => "Have no idea",
                "question" => "Do you have any idea?",
                "solution" => "I have no idea!",
                "score" => 100,
            ))
        );

        self::$assessment = new Assessment(array(
            "multipleChoice" => $multipleChoices,
            "essay"          => $essays,
            "simpleQuiz"     => $simpleQuizes,
            "matching"       => $matchingQuizes,
        ));
    }

    public static function createContent()
    {
        self::createRelatedSubject();
        self::createActivities();
        self::createAssessment();

        self::$content = new Content(array(
            "school" => "The Hobbit School",
            "topic" => "Computer Science",
            "theme" => "Fantasy",
            "class" => 4,
            "term" => 1,
            "relatedSubjects" => self::$relatedSubjects,
            "goals" => array("Understands Hobbit's CS", "Foo Bar Qux"),
            "materials" => "The material ...",
            "activities" => self::$activities,
            "resources" => array("The Book of Hobbit", "Algorithm Slide"),
            "assessment" => self::$assessment,
        ));
    }

    public static function setUpBeforeClass()
    {
        self::createContent();

        self::$item = new Item(array(
            "title" => "Test Item",
            "status" => "draft",
            "authors" => array("Legolas", "Arwen", "Aragon"),
            "description" => "Hello, world!",
            "lookup" => "hello, world!",
            "tags" => array("test", "draft", "legolas"),
            "data" => self::$content,
        ));
    }

    // ------------------------------------------------------------------------

    public function testIsEverythingOkay()
    {
        $this->assertNotNull(self::$item);
        $this->assertNotNull(self::$relatedSubjects);
        $this->assertNotNull(self::$content);
        $this->assertNotNull(self::$activities);
        $this->assertNotNull(self::$assessment);

        $this->assertEquals(self::$item->data, self::$content);
        $this->assertEquals(self::$item->data->activities, self::$activities);
        $this->assertEquals(self::$item->data->assessment, self::$assessment);

        static::logDump(self::$item->toArray(), false);
        static::logEcho(self::$item->toJson(true), false);
    }

    /**
     * @depends testIsEverythingOkay
     */
    public function testReadItem()
    {
        $this->assertEquals('The Hobbit School', self::$item->data->school);

        $duration = self::$item->data->activities->startDuration +
            self::$item->data->activities->coreDuration +
            self::$item->data->activities->closingDuration;
        $this->assertEquals(90, $duration);
    }

    // ------------------------------------------------------------------------

    /*//
    public function testPersistItem(\SusRpp\Service\ItemServiceInterface $itemService = null)
    {
        if ($itemService == null) {
            $itemService = \App::make("itemService");
        }

        static::logDump($itemService->createItem(self::$item), true);
    }
    //*/

    /*//
    public function testRetrieveItem(\SusRpp\Service\ItemServiceInterface $itemService = null)
    {
        if ($itemService == null) {
            $itemService = \App::make("itemService");
        }

        $item = $itemService->findByTitle("Test Item");
        static::logEcho($item->toJson(true), true);
    }
    //*/
}
