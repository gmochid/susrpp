<?php
namespace SusRpp\Test\Entity;

use SusRpp\Entity\Item\Assessment;
use SusRpp\Entity\Item\Assessment\MultipleChoiceAssessmentItem;
use SusRpp\Entity\Item\Assessment\EssayAssessmentItem;
use SusRpp\Entity\Item\Assessment\SimpleQuizAssessmentItem;
use SusRpp\Entity\Item\Assessment\MatchingQuizAssessmentItem;

class AssessmentTest extends \TestCase
{
    public static $assessment;

    public static function setUpBeforeClass()
    {
        $multipleChoices = array(
            new MultipleChoiceAssessmentItem(array(
                "indicator" => "Know what is Foo Bar",
                "question"  => "What is Foo Bar?",
                "options"   => array("Nothing", "A superhero", "A madman", "Ghost"),
                "solution"  => "A superhero",
                "score"     => 50,
            )),
            new MultipleChoiceAssessmentItem(array(
                "indicator" => "Able to answer \"how are you\" question",
                "question"  => "How are you?",
                "options"   => array("Fine, thanks", "What?", "I don't understand", "WTF?"),
                "solution"  => "Fine, thanks",
                "score"     => 50,
            )),
        );

        $essays = array(
            new EssayAssessmentItem(array(
                "indicator" => "Understand the Quicksort Algorithm",
                "question"  => "Explain quicksort algorithm and write the functional pseudocode of it!",
                "solutions" => array(
                    array("keyPoint" => "Word explanation", "score" => 30),
                    array("keyPoint" => "Pseudocode", "score" => 50),
                    array("keyPoint" => "Algorithm Correctness", "score" => 20),
                )
            )),
        );

        $simpleQuizes = array(
            new SimpleQuizAssessmentItem(array(
                "indicator" => "Know the capital of Slovenia",
                "question" => "What is the capital of Slovenia?",
                "solution" => "Ljubljana",
                "score" => 100,
            )),
        );

        $matchingQuizes = array(
            new MatchingQuizAssessmentItem(array(
                "indicator" => "Have no idea",
                "question" => "Do you have any idea?",
                "solution" => "I have no idea!",
                "score" => 100,
            ))
        );

        self::$assessment = new Assessment(array(
            "multipleChoice" => $multipleChoices,
            "essay"          => $essays,
            "simpleQuiz"     => $simpleQuizes,
            "matching"       => $matchingQuizes,
        ));
    }

    public function testCreation()
    {
        $this->assertNotNull(self::$assessment);

        $multipleChoices = self::$assessment->multipleChoice;
        $this->assertEquals($multipleChoices[0]->indicator, "Know what is Foo Bar");
        $this->assertEquals($multipleChoices[1]->indicator, "Able to answer \"how are you\" question");

        $essays = self::$assessment->essay;
        $this->assertEquals("Understand the Quicksort Algorithm", $essays[0]->indicator);
        $this->assertEquals("Algorithm Correctness", $essays[0]->solutions[2]['keyPoint']);

        $simpleQuizes = self::$assessment->simpleQuiz;
        $this->assertEquals("What is the capital of Slovenia?", $simpleQuizes[0]->question);
        $this->assertEquals("Ljubljana", $simpleQuizes[0]->solution);

        $matchingQuizes = self::$assessment->matching;
        $this->assertEquals("Have no idea", $matchingQuizes[0]->indicator);

        static::logDump(self::$assessment, false);
        static::logEcho(self::$assessment->toJson(true), false);
    }
}
