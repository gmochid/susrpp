<?php
namespace SusRpp\Test\Entity;

use SusRpp\Entity\User;

class UserTest extends \TestCase
{
    public function testConstruct()
    {
        $user = new User(array(
            'username' => 'foo',
            'password' => 'foo123',
            'name'     => 'Foo Qux',
            'email'    => 'foo@example.com'
        ));

        $this->assertEquals('foo', $user->username);
        $this->assertEquals('foo123', $user->password);
        $this->assertEquals('Foo Qux', $user->name);
        $this->assertEquals('foo@example.com', $user->email);
        $this->assertEquals('foo qux', $user->lookupName);

        $this->assertNull($user->sesstoken);
        $this->assertNull($user->items);

        static::logEcho($user->toJson(true), false);
    }
}
