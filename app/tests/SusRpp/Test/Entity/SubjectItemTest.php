<?php
namespace SusRpp\Test\Entity;

use SusRpp\Entity\Item\SubjectItem;

class SubjectItemTest extends \TestCase
{
    public function testConstruct()
    {
        $programming = new SubjectItem(array(
            "subjectName" => "Programming",
            "coreCompetence" => "Functional and Imperative Programming",
            "basicCompetence" => array(
               "Algorithm", "Data Structure",
            ),
            "indicator" => array(
               "Small Tasks", "Final Project",
            ),
        ));

        $this->assertEquals("Programming", $programming->subjectName);
        $this->assertEquals("Functional and Imperative Programming", $programming->coreCompetence);
        $this->assertEquals(2, count($programming->basicCompetence));
        $this->assertEquals(2, count($programming->indicator));

        static::logDump($programming, false);
        static::logDump($programming->toJson(true), false);
    }
}
