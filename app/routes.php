<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// User Interface -------------------------------------------------------------
Route::get    ('/',                           'uiController@showDashboard');
Route::get    ('/rpp',                        'uiController@showRPPDashboard');
Route::get    ('/editor',                     'uiController@showEditor');
Route::get    ('/view/{id}',                  'uiController@showView');
Route::get    ('/login',                      'uiController@showLogin');
Route::get    ('/logout',                     'uiController@showLogout');
Route::get    ('/register',                   'uiController@showRegister');

// Admin
Route::get    ('/admin',                      'adminController@showAdminPage');

// User API -------------------------------------------------------------------
Route::post   ('/api/user/auth',              'userController@auth');
Route::post   ('/api/user/session/auth',      'userController@authSession');
Route::get    ('/api/user/session/forget',    'userController@forgetSession');

Route::get    ('/api/user/find',              'userController@find');
Route::get    ('/api/user/group/{groupName}', 'userController@findAllByGroup');
Route::get    ('/api/user/{id?}',             'userController@get');
Route::post   ('/api/user/register',          'userController@register');
Route::put    ('/api/user/update',            'userController@update');
Route::delete ('/api/user/{id}',              'userController@remove');
// Validate token is actually never used
Route::get    ('/api/user/validate/{token}',  'userController@validateToken');
// Group
Route::post   ('/api/user/creategroup',       'userController@group_create');
Route::put    ('/api/user/movegroup',         'userController@group_moveInUser');



// Subject API ----------------------------------------------------------------
Route::get    ('/api/subject/all',            'subjectController@all');
Route::get    ('/api/subject/find',           'subjectController@find');
Route::get    ('/api/subject/{id}',           'subjectController@get');
Route::post   ('/api/subject',                'subjectController@post');
Route::put    ('/api/subject',                'subjectController@update');
Route::delete ('/api/subject/{id}',           'subjectController@remove');
Route::get    ('/api/subject',                'subjectController@all');

// Item API -------------------------------------------------------------------
Route::get    ('/api/item/find',              'rppController@find');
Route::get    ('/api/item/findone',           'rppController@findOne');

Route::get    ('/api/item/{id?}',             'rppController@get');
Route::post   ('/api/item',                   'rppController@post');
Route::put    ('/api/item',                   'rppController@update');
Route::delete ('/api/item/{id}',              'rppController@remove');

// Comment API ----------------------------------------------------------------
Route::get    ('/api/comment/item/{itemId}',  'commentController@findByItemId');
Route::get    ('/api/comment/{id}',           'commentController@get');
Route::post   ('/api/comment',                'commentController@post');
Route::delete ('/api/comment/{id}',           'commentController@delete');

// Resource API ----------------------------------------------------------------
Route::get    ('/api/resource/{id}',          'resourceController@get');
Route::get    ('/api/resource/base64/{id}',   'resourceController@getBase64');
Route::post   ('/api/resource',               'resourceController@post');
Route::put    ('/api/resource/{id}',          'resourceController@update');
