<?php
namespace SusRpp\Service;

use SusRpp\Lib\DbDriver\MongoDbDriver;
use SusRpp\Entity\Comment;
use SusRpp\Factory\CommentFactory;

use SusRpp\Exception\CommentNotFoundException;

class CommentService
{
    const COLLECTION = 'comments';

    /**
     * #dependency
     * @var MongoDbDriver
     */
    protected $dbDriver;

    /**
     * #dependency
     * @var CommentFactory
     */
    protected $commentFactory;

    /**
     * @var \MongoCollection
     */
    protected $commentDb;

    /**
     * Constructor
     *
     * @param MongoDbDriver $dbDriver
     * @param CommentFactory $groupFactory
     */
    public function __construct(MongoDbDriver $dbDriver,
            CommentFactory $commentFactory)
    {
        $this->dbDriver       = $dbDriver;
        $this->commentFactory = $commentFactory;

        $this->commentDb = $dbDriver->getCollection(self::COLLECTION);
    }

    protected function buildObjectsArray($commentAssocs)
    {
        $comments = array();
        foreach ($commentAssocs as $commentAssoc) {
            $comments[] = $this->commentFactory->fromArray($commentAssoc);
        }

        return $comments;
    }

    public function findById($id)
    {
        try {
            $commentAssoc = $this->commentDb->findOne(array(
                '_id' => new \MongoId($id)
            ));
        } catch (\MongoException $e) {
            $commentAssoc = null;
        }

        if ($commentAssoc == null) {
            throw new CommentNotFoundException("Comment {$id} not found!");
        }

        return $this->commentFactory->fromArray($commentAssoc);
    }

    public function findAllByItemId($itemId)
    {
        $commentAssocs = $this->commentDb->find(array(
            '$query' => array(
               'itemId' => $itemId,
            ),
            '$orderBy' => array('_id' => 1),
        ));

        return $this->buildObjectsArray($commentAssocs);
    }

    public function createComment(Comment &$comment)
    {
        $commentAssoc = $comment->toArray();

        $date = new \DateTime();
        $commentAssoc['timeCreated'] = $date->format("Y-m-d H:i:s");

        $this->commentDb->insert($commentAssoc);
        $comment->_id = $commentAssoc['_id']->{'$id'};

        return $comment;
    }

    public function removeCommment(Comment &$comment)
    {
        return $this->commentDb->remove(array(
            '_id' => new \MongoId($comment->_id)
        ));
    }
}
