<?php
namespace SusRpp\Service;

use SusRpp\Lib\DbDriver\MongoDbDriver;

use SusRpp\Entity\User;

use SusRpp\Service\ItemService;
use SusRpp\Exception\UserNotFoundException;

class UserService
{
    const COLLECTION = 'user';

    /**
     * #dependency
     * @var \SusRpp\Lib\DbDriver\MongoDbDriver
     */
    protected $dbDriver;

    /**
     * #dependency
     * @var \SusRpp\Service\ItemService
     */
    protected $itemService;

    /**
     * @var MongoCollection
     */
    protected $userDb;

    /**
     * Constructor
     *
     * @param MongoDbDriver $dbDriver
     * @param ItemServiceInterface $itemService
     */
    public function __construct(MongoDbDriver $dbDriver,
            ItemService $itemService)
    {
        $this->dbDriver = $dbDriver;
        $this->userDb = $dbDriver->getCollection(self::COLLECTION);

        $this->itemService = $itemService;
    }

    /**
     * Returns a user with specified $username
     *
     * @param string $username
     * @return User
     */
    public function findByUsername($username, $status = -1)
    {
        $user = null;

        try {
            if ($status == -1) {
                $u = $this->userDb->findOne(array('_id' => $username));
            } else {
                $u = $this->userDb->findOne(array('_id' => $username, 'status' => $status));
            }
        } catch (\MongoException $e) {
            $u = null;
        }

        if ($u == null) {
            throw new UserNotFoundException("User {$username} not found!");
            return null;
        }

        $user = new User(array(
            // username mapped to _id
            'username'  => $u['_id'],

            'password'  => $u['password'],
            'name'      => $u['name'],
            'email'     => $u['email'],
            'sesstoken' => $u['sesstoken'],
            'role'      => $u['role'],
            'status'    => $u['status'],

            // new: Group
            'groupId'   => $u['groupId'],
            'groupName' => $u['groupName'],
        ));

        return $user;
    }

    /**
     * Returns a user with specified $username with
     * items populated
     *
     * @param string $username
     * @return User
     */
    public function findByUsernameWithItems($username)
    {
        $user = $this->findByUsername($username);
        if ($user != null) {
            $items = $this->itemService->findAllByAuthorId($user->username);
            $user->items = $items;
        }

        return $user;
    }

    /**
     * Finds users by its name. If name is empty, it returns
     * all of the users in the database.
     *
     * @param string $name
     * @return User[]
     */
    public function findUserByName($name, $status = -1)
    {
        if ($status == -1) {
            $userAssocs = $this->userDb->find(array(
                'name' => new \MongoRegex('/' . $name . '/i'),
                'role' => 'user',
            ));
        } else {
            $userAssocs = $this->userDb->find(array(
                'name' => new \MongoRegex('/' . $name . '/i'),
                'status' => $status,
                'role' => 'user',
            ));
        }

        $users = array();
        foreach ($userAssocs as $u) {
            $users[] = new User(array(
                // username mapped to _id
                'username'  => $u['_id'],

                'password'  => $u['password'],
                'name'      => $u['name'],
                'email'     => $u['email'],
                'sesstoken' => $u['sesstoken'],
                'role'      => $u['role'],
                'status'    => $u['status'],

                // new: Group
                'groupId'   => $u['groupId'],
                'groupName' => $u['groupName'],
            ));
        }

        return $users;
    }

    /**
     * Creates and persists a user
     *
     * @param User $user
     */
    public function createUser(User $user)
    {
        $user->sesstoken = $this->generateToken($user);

        $insert = $this->userDb->insert(array(
            // username mapped to _id
            '_id'       => $user->username,

            'password'  => $this->hashPassword($user->password),
            'name'      => $user->name,
            'email'     => $user->email,
            'sesstoken' => $user->sesstoken,
            'lookup'    => $user->lookupName,
            'role'      => $user->role,
            'status'    => $user->status,

            // new: Group
            'groupId'   => $user->groupId,
            'groupName' => $user->groupName,
        ));

        return $user;
    }

    /**
     * Updates and persists a user
     *
     * @param User $user
     */
    public function updateUser(User $user)
    {
        $update = $this->userDb->update(array('_id' => $user->username), array(
            // username mapped to _id
            '_id'        => $user->username,

            'password'  => $user->password,
            'name'      => $user->name,
            'email'     => $user->email,
            'sesstoken' => $user->sesstoken,
            'lookup'    => $user->lookupName,
            'role'      => $user->role,
            'status'    => $user->status,

            // new: Group
            'groupId'   => $user->groupId,
            'groupName' => $user->groupName,
        ));

        return $user;
    }

    /**
     * Removes a user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->userDb->remove(array(
            '_id' => $user->username,
        ));
    }

    /**
     * Creates a hash of a user's password
     *
     * @param string $password
     * @return string
     */
    public function hashPassword($password)
    {
        return md5($password);
    }

    /**
     * Generates user's session token
     *
     * @param User $user
     * @return string
     */
    public function generateToken(User $user)
    {
        return md5(substr($user->username, -4) . substr($user->password, -4));
    }
}
