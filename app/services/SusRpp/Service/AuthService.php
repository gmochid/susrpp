<?php
namespace SusRpp\Service;

use SusRpp\Lib\DbDriver\MongoDbDriver;

use SusRpp\Exception\InvalidTokenException;
use SusRpp\Exception\BadCredentialsException;

use SusRpp\Service\UserService;

class AuthService
{
    /**
     * #dependency
     * @var \SusRpp\Lib\DbDriver\MongoDbDriver
     */
    protected $dbDriver;

    /**
     * #dependency
     * @var \SusRpp\Service\UserService
     */
    protected $userService;

    /**
     * @var \MongoCollection
     */
    protected $userDb;

    /**
     * Constructor
     *
     * @param MongoDbDriver $dbDriver
     * @param UserServiceInterface $userService
     */
    public function __construct(MongoDbDriver $dbDriver,
            UserService $userService)
    {
        $this->dbDriver = $dbDriver;
        $this->userService = $userService;

        $this->userDb = $dbDriver->getCollection(UserService::COLLECTION);
    }

    /**
     * Authenticates a user
     *
     * @param string $username
     * @param string $password
     * @throws \SusRpp\Exception\BadCredentialsException
     * @return User
     */
    public function authenticate($username, $password)
    {
        $user = $this->userService->findByUsername($username, 1);

        if ($user != null) {
            // Retrieved user's password is already hashed
            if ($user->password == $this->userService->hashPassword($password)) {
                return $user;
            } else {
                throw new BadCredentialsException('Bad credentials.');
            }
        } else {
            throw new BadCredentialsException('Bad credentials.');
        }

        return $user;
    }

    /**
     * Validates a token
     *
     * @param string $token
     * @throws \SusRpp\Exception\InvalidTokenException
     * @return boolean
     */
    public function validateToken($token)
    {
        $userAssoc = $this->userDb->findOne(array(
            'sesstoken' => $token,
        ));

        if ($userAssoc == null) {
            throw new InvalidTokenException('Invalid token.');
        }

        else return $userAssoc['sesstoken'];
    }
}
