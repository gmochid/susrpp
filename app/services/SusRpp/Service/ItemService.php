<?php
namespace SusRpp\Service;

use SusRpp\Lib\DbDriver\MongoDbDriver;

use SusRpp\Entity\Item;
use SusRpp\Factory\ItemFactory;

use SusRpp\Exception\ItemNotFoundException;

class ItemService
{
    const COLLECTION = 'item';

    /**
     * #dependency
     * @var MongoDbDriver
     */
    protected $dbDriver;
    /**
     * #dependency
     * @var ItemFactory
     */
    protected $itemFactory;

    /**
     * @var MongoCollection
     */
    protected $itemDb;

    /**
     * Constructor
     *
     * @param MongoDbDriver $dbDriver
     */
    public function __construct(MongoDbDriver $dbDriver,
            ItemFactory $itemFactory)
    {
        $this->dbDriver    = $dbDriver;
        $this->itemDb      = $dbDriver->getCollection(self::COLLECTION);
        $this->itemFactory = $itemFactory;
    }

    /**
     * Builds array of item objects
     *
     * @param array $itemAssocs
     * @return Item[]
     */
    protected function buildItems($itemAssocs)
    {
        $items = array();
        foreach ($itemAssocs as $itemAssoc) {
            $items[] = $this->itemFactory->fromArray($itemAssoc);
        }

        return $items;
    }

    /**
     * Find an item by id
     *
     * @param mixed $id
     * @return Item
     */
    public function findById($id)
    {
        try {
            $itemAssoc = $this->itemDb->findOne(array(
                '_id' => new \MongoId($id),
            ));
        } catch (\MongoException $e) {
            $itemAssoc = null;
        }

        if ($itemAssoc == null) {
            throw new ItemNotFoundException("Item with id {$id} not found!");
        }
        return $this->itemFactory->fromArray($itemAssoc);
    }

    /**
     * Finds an item by title
     *
     * @param string $title
     * @return Item
     */
    public function findByTitle($title)
    {
        $itemAssoc = $this->itemDb->findOne(array(
            'title' => $title,
        ));

        if ($itemAssoc == null) {
            throw new ItemNotFoundException("Item with title \"{$title}\" not found!");
        }
        return $this->itemFactory->fromArray($itemAssoc);
    }

    /**
     * Finds items by author name
     *
     * @param string $authorName
     * @return Item[]
     */
    public function findAllByAuthorName($authorName)
    {
        $itemAssocs = $this->itemDb->find(array(
            'authors' => array(
                '$in' => array(new \MongoRegex('/' . $authorName . '/i')),
            ),
        ));

        return $this->buildItems($itemAssocs);
    }

    /**
     * Finds items by author id (username)
     *
     * @param string $authorId
     * @return Item[]
     */
    public function findAllByAuthorId($authorId)
    {
        $itemAssocs = $this->itemDb->find(array(
            '$query' => array(
                'authorIds' => array(
                    '$in' => array($authorId),
                ),
            ),
            '$orderby' => array('title' => 1),
        ));

        return $this->buildItems($itemAssocs);
    }

    /**
     * Finds items by owner group (groupName)
     *
     * @param string $groupName
     * @return Item[]
     */
    public function findAllByOwnerGroup($groupName)
    {
        $itemAssocs = $this->itemDb->find(array(
            '$query' => array(
               'ownerGroup' => array(
                    '$in' => array($groupName),
                ),
            ),
            '$orderby' => array('title' => 1),
        ));

        return $this->buildItems($itemAssocs);
    }

    /**
     * Finds items by theme name
     *
     * @param string $themeName
     * @return Item[]
     */
    public function findAllByTheme($themeName)
    {
        $itemAssocs = $this->itemDb->find(array(
            'data.theme' => new \MongoRegex('/^' . $themeName . '$/i'),
        ));

        return $this->buildItems($itemAssocs);
    }

    /**
     * Finds items by description
     *
     * @param string $description
     * @return Item[]
     */
    public function findAllByDescription($description)
    {
        $itemAssocs = $this->itemDb->find(array(
           'description' => new \MongoRegex('/' . $description . '/i'),
        ));

        return $this->buildItems($itemAssocs);
    }

    /**
     * Finds items by tag
     *
     * @param string $tag
     * @return Item[]
     */
    public function findAllByTag($tag)
    {
        $itemAssocs = $this->itemDb->find(array(
            'tags' => array(
                '$in' => array($tag),
            ),
        ));

        return $this->buildItems($itemAssocs);
    }

    /**
     * Finds items by title
     *
     * @param string $title
     * @return Item[]
     */
    public function findAllByTitle($title)
    {
        $itemAssocs = $this->itemDb->find(array(
            'title' => new \MongoRegex('/' . $title . '/i'),
        ));

        return $this->buildItems($itemAssocs);
    }

    /**
     * Creates and persists an item
     *
     * @param Item $item
     */
    public function createItem(Item $item)
    {
        $itemAssoc = $item->toArray();

        $date = new \DateTime();
        $itemAssoc['lastUpdated'] = $date->format("Y-m-d H:i:s");

        $this->itemDb->insert($itemAssoc);

        $item->_id = $itemAssoc['_id']->{'$id'};
        return $item;
    }

    /**
     * Updates and persists an item
     *
     * @param Item $item
     */
    public function updateItem(Item $item)
    {
        $itemAssoc = $item->toArray();

        $date = new \DateTime();
        $itemAssoc['lastUpdated'] = $date->format("Y-m-d H:i:s");

        // Unset _id, prevents re-creation
        unset($itemAssoc['_id']);
        $this->itemDb->update(array(
            '_id' => new \MongoId($item->_id)
        ), $itemAssoc);

        return $item;
    }

    /**
     * Removes an item
     *
     * @param Item $item
     */
    public function removeItem(Item $item)
    {
        return $this->itemDb->remove(array(
            '_id' => new \MongoId($item->_id),
        ));
    }
}
