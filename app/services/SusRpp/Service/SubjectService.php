<?php
namespace SusRpp\Service;

use SusRpp\Lib\DbDriver\MongoDbDriver;

use SusRpp\Entity\Subject;
use SusRpp\Factory\SubjectFactory;

use SusRpp\Exception\SubjectNotFoundException;

class SubjectService
{
    const COLLECTION = 'subject';

    /**
     * #dependency
     * @var MongoDbDriver
     */
    protected $dbDriver;

    /**
     * #dependency
     * @var SubjectFactory
     */
    protected $subjectFactory;

    /**
     * @var \MongoCollection
     */
    protected $subjectDb;

    /**
     * Constructor
     *
     * @param MongoDbDriver $dbDriver
     * @param SubjectFactory $subjectFactory
     */
    public function __construct(MongoDbDriver $dbDriver,
            SubjectFactory $subjectFactory)
    {
        $this->dbDriver       = $dbDriver;
        $this->subjectDb      = $dbDriver->getCollection(self::COLLECTION);
        $this->subjectFactory = $subjectFactory;
    }

    /**
     * Builds an array of subject objects
     *
     * @param array $subjectAssocs
     * @return Subject[]
     */
    protected function buildSubjects($subjectAssocs)
    {
        $subjects = array();
        foreach ($subjectAssocs as $subjectAssoc) {
            $subjects[] = $this->subjectFactory->fromArray($subjectAssoc);
        }

        return $subjects;
    }

    /**
     * Finds a subject by id
     *
     * @param mixed $id
     * @throws SubjectNotFoundException
     * @return Subject
     */
    public function findById($id)
    {
        try {
            $subjectAssoc = $this->subjectDb->findOne(array(
                '_id' => new \MongoId($id),
            ));
        } catch (\MongoException $e) {
            $subjectAssoc = null;
        }

        if ($subjectAssoc == null) {
            throw new SubjectNotFoundException("Subject with id {$id} not found!");
        }

        return $this->subjectFactory->fromArray($subjectAssoc);
    }

    /**
     * Finds a subject by subjectName
     *
     * @param string $subjectName
     * @throws SubjectNotFoundException
     * @return Subject
     */
    public function findBySubjectName($subjectName)
    {
        try {
            $subjectAssoc = $this->subjectDb->findOne(array(
                'subjectName' => new \MongoRegex('/' . $subjectName . '/i'),
            ));
        } catch (\MongoException $e) {
            $subjectAssoc = null;
        }

        if ($subjectAssoc == null) {
            throw new SubjectNotFoundException("Subject with name {$subjectName} not found!");
        }

        return $this->subjectFactory->fromArray($subjectAssoc);
    }

    /**
     * Returns all persisted subjects
     *
     * @return Subject[]
     */
    public function findAll()
    {
        return $this->buildSubjects($this->subjectDb->find());
    }

    /**
     * Returns subjects by subjectName
     *
     * @param string $subjectName
     * @return Subject[]
     */
    public function findAllBySubjectName($subjectName)
    {
        $subjectAssocs = $this->subjectDb->find(array(
            'subjectName' => new \MongoRegex('/' . $subjectName . '/i'),
        ));

        return $this->buildSubjects($subjectAssocs);
    }

    /**
     * Creates and persists a subject
     *
     * @param Subject $subject
     * @return Subject
     */
    public function createSubject(Subject $subject)
    {
        $subjectAssoc = $subject->toArray();
        $this->subjectDb->insert($subjectAssoc);

        $subject->_id = $subjectAssoc['_id']->{'$id'};
        return $subject;
    }

    /**
     * Updates and persists a subject
     *
     * @param Subject $subject
     * @return Subject
     */
    public function updateSubject(Subject $subject)
    {
        $subjectAssoc = $subject->toArray();

        // Unset _id, prevents re-creation
        unset($subjectAssoc['_id']);
        $this->subjectDb->update(array(
            '_id' => new \MongoId($subject->_id),
        ), $subjectAssoc);

        return $subject;
    }

    /**
     * Removes a subject
     *
     * @param Subject $subject
     */
    public function removeSubject(Subject $subject)
    {
        return $this->subjectDb->remove(array(
            '_id' => new \MongoId($subject->_id),
        ));
    }
}
