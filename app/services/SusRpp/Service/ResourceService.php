<?php
namespace SusRpp\Service;

use SusRpp\Lib\DbDriver\MongoDbDriver;

use SusRpp\Factory\ResourceFactory;
use SusRpp\Entity\Resource;

use SusRpp\Exception\ResourceNotFoundException;

class ResourceService
{
	const COLLECTION = "resource";

	/**
     * #dependency
     * @var MongoDbDriver
     */
    protected $dbDriver;
    /**
     * #dependency
     * @var resourceFactory
     */
    protected $resourceFactory;

    /**
     * @var MongoCollection
     */
    protected $resourceDb;

    /**
     * Constructor
     *
     * @param MongoDbDriver $dbDriver
     */
    public function __construct(MongoDbDriver $dbDriver, ResourceFactory $resourceFactory)
    {
        $this->dbDriver         = $dbDriver;
        $this->resourceDb       = $dbDriver->getCollection(self::COLLECTION);
        $this->resourceFactory  = $resourceFactory;
    }

    protected function buildObjectsArray($resourceAssocs)
    {
        $resources = array();
        foreach ($resourceAssocs as $resourceAssoc) {
            $resources[] = $this->resourceFactory->fromArray($resourceAssoc);
        }

        return $resources;
    }

    /**
     * Find a resource by id
     *
     * @param mixed $id
     * @return Resource
     */
    public function findById($id)
    {
        try {
            $resourceAssoc = $this->resourceDb->findOne(array(
                '_id' => new \MongoId($id),
            ));
        } catch (\MongoException $e) {
            $resourceAssoc = null;
        }

        if ($resourceAssoc == null) {
            throw new ResourceNotFoundException("Resource with id {$id} not found!");
        }

        return $this->resourceFactory->fromArray($resourceAssoc);
    }

    /**
     * Find resources by itemId
     *
     * @param mixed $itemId
     * @return Resource
     */
    public function findAllByItemId($itemId)
    {
        try {
            $resourceAssocs = $this->resourceDb->find(array(
                '$query' => array(
                   'itemId' => $itemId,
                ),
                '$orderBy' => array('_id' => 1),
            ));
        } catch (\MongoException $e) {
            $resourceAssocs = null;
        }

        if ($resourceAssocs == null) {
            throw new ResourceNotFoundException("Item with {$id} doesn't have resources!");
        }

        return $this->buildObjectsArray($resourceAssocs);
    }

    /**
     * Creates and persists a resource
     *
     * @param Resource $resource
     */
    public function createResource(Resource $resource)
    {
        $resourceAssoc = $resource->toArray();

        $this->resourceDb->insert($resourceAssoc);
        $resource->_id = $resourceAssoc['_id']->{'$id'};

        return $resource;
    }

    /**
     * Updates and persists a resource
     *
     * @param Resource $resource
     */
    public function updateResource(Resource $resource)
    {
        $resourceAssoc = $resource->toArray();

        // Unset _id, prevents re-creation
        unset($resourceAssoc['_id']);
        $this->resourceDb->update(array(
            '_id' => new \MongoId($resource->_id),
        ), $resourceAssoc);

        return true;
    }

    /**
     * Removes a resource from database
     *
     * @param Resource $resource
     */
    public function removeResource(Resource $resource)
    {
        $this->resourceDb->remove(array(
            '_id' => new \MongoId($resource->_id),
        ));

        return true;
    }
}
