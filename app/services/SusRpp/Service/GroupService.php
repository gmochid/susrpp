<?php
namespace SusRpp\Service;

use SusRpp\Lib\DbDriver\MongoDbDriver;

use SusRpp\Entity\User;
use SusRpp\Entity\Group;
use SusRpp\Factory\GroupFactory;

use SusRpp\Exception\GroupNotFoundException;

use SusRpp\Service\UserService;

class GroupService
{
    const COLLECTION = 'groups';

    /**
     * #dependency
     * @var MongoDbDriver
     */
    protected $dbDriver;

    /**
     * #dependency
     * @var UserService
     */
    protected $userService;

    /**
     * #dependency
     * @var GroupFactory
     */
    protected $groupFactory;

    /**
    * @var \MongoCollection
    */
    protected $groupDb;

    /**
     * Constructor
     *
     * @param MongoDbDriver $dbDriver
     * @param UserServiceInterface $userService
     * @param GroupFactory $groupFactory
     */
    public function __construct(MongoDbDriver $dbDriver,
            UserService $userService,
            GroupFactory $groupFactory)
    {
        $this->dbDriver     = $dbDriver;
        $this->userService  = $userService;
        $this->groupFactory = $groupFactory;

        $this->groupDb = $dbDriver->getCollection(self::COLLECTION);
    }

    /**
     * Builds an array of group objects
     *
     * @param array $groupAssocs
     * @return Group[]
     */
    protected function buildGroups($groupAssocs)
    {
        $groups = array();
        foreach ($groupAssocs as $groupAssoc) {
            $groups[] = $this->groupFactory->fromArray($groupAssoc);
        }

        return $groups;
    }

    /**
     * Returns all group
     */
    public function findAll()
    {
        return $this->buildGroups($this->groupDb->find());
    }

    /**
     * Returns a group with specified id
     *
     * @param string $username
     * @return Group
     */
    public function findById($id)
    {
        try {
            $groupAssoc = $this->groupDb->findOne(array(
                '_id' => new \MongoId($id),
            ));
        } catch (\MongoException $e) {
            $groupAssoc = null;
        }

        if ($groupAssoc == null) {
            throw new GroupNotFoundException("Group {$id} not found!");
        }

        return $this->groupFactory->fromArray($groupAssoc);
    }

    /**
     * Returns a group with specified name
     *
     * @param string $name
     * @return Group
     */
    public function findByName($name)
    {
        $groupAssoc = $this->groupDb->findOne(array(
            'groupName' => $name,
        ));

        if ($groupAssoc == null) {
            throw new GroupNotFoundException("Group with name {$name} not found!");
        }

        return $this->groupFactory->fromArray($groupAssoc);
    }

    /**
     * Returns users in a group
     *
     * @param Group $group
     * @return User[]
     */
    public function getUsersFromGroup(Group $group, $status = 1)
    {
        $users = array();

        foreach($group->userIds as $username) {
            $user = $this->userService->findByUsername($username, $status);
            if ($user != null) {
                $users[] = $user;
            }
        }

        return $users;
    }

    /**
     * Adds a user into a group
     */
    public function addUserToGroup(User $user, Group $group)
    {
        $groupId = new \MongoId($group->_id);
        $groupAssoc = $group->toArray();

        $user->groupId = $group->_id;
        $user->groupName = $group->groupName;
        $this->userService->updateUser($user);

        unset($groupAssoc['_id']);

        if (!in_array($user->username, $groupAssoc['userIds'])) {
            $groupAssoc['userIds'][] = $user->username;
        } else {
            return false;
        }

        $this->groupDb->update(array(
            '_id' => $groupId,
        ), $groupAssoc);

        return true;
    }

    /**
     * Removes a user from a group
     */
    public function removeUserFromGroup(User $user, Group $group)
    {
        $index = array_search($user->username, $group->userIds);

        if ($index === false) {
            return false;
        } else {
            $userIds = $group->userIds;
            array_splice($userIds, $index, 1);
            $group->userIds = $userIds;
        }

        $groupId = new \MongoId($group->_id);
        $groupAssoc = $group->toArray();

        // Unset _id, prevents re-creation
        unset($groupAssoc['_id']);
        $this->groupDb->update(array(
            '_id' => $groupId,
        ), $groupAssoc);

        return true;
    }

    /**
     * Removes a user from user's current group
     */
    public function removeUserFromItsGroup(User $user)
    {
        $group = $this->findByName($user->groupName);
        $index = array_search($user->username, $group->userIds);

        if ($index === false) {
            return false;
        } else {
            $userIds = $group->userIds;
            array_splice($userIds, $index, 1);
            $group->userIds = $userIds;
        }

        $groupAssoc = $group->toArray();

        // Unset _id, prevents re-creation
        unset($groupAssoc['_id']);
        $this->groupDb->update(array(
            '_id' => new \MongoId($group->_id),
        ), $groupAssoc);

        return true;
    }

    /**
     * Creates and persists a group
     *
     * @param Group $group
     */
    public function createGroup(Group $group)
    {
        $groupAssoc = $group->toArray();

        $this->groupDb->insert($groupAssoc);
        $group->_id = $groupAssoc['_id']->{'$id'};

        return $group;
    }

    /**
     * Updates and persists a group
     *
     * @param Group $group
     */
    public function updateGroup(Group $group)
    {
        $groupAssoc = $group->toArray();

        // Unset _id, prevents re-creation
        unset($groupAssoc['_id']);
        $this->groupDb->update(array(
            '_id' => new \MongoId($group->_id),
        ), $groupAssoc);

        return true;
    }

    /**
     * Removes a group from database
     *
     * @param Group $group
     */
    public function removeGroup(Group $group)
    {
        $this->groupDb->remove(array(
            '_id' => new \MongoId($group->_id),
        ));

        return true;
    }
}
