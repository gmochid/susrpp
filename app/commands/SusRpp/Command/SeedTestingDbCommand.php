<?php
namespace SusRpp\Command;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use SusRpp\Entity\Item;
use SusRpp\Entity\Subject;
use SusRpp\Entity\User;
use SusRpp\Entity\Group;

use SusRpp\Lib\Factory\ItemFactory;
use SusRpp\Lib\Factory\SubjectFactory;

use SusRpp\Service\ItemServiceInterface;
use SusRpp\Service\UserServiceInterface;

class SeedTestingDbCommand extends Command {
    /**
     * @var \SusRpp\Lib\Factory\ItemFactory
     */
    protected $itemFactory;
    /**
     * @var \SusRpp\Lib\Factory\SubjectFactory
     */
    protected $subjectFactory;
    /**
     * @var \SusRpp\Service\ItemServiceInterface
     */
    protected $itemService;
    /**
     * @var \SusRpp\Service\UserServiceInterface
     */
    protected $userService;
    /**
     * @var \SusRpp\Service\GroupServiceInterface
     */
    protected $groupService;
    /**
     * @var \SusRpp\Service\SubjectServiceInterface
     */
    protected $subjectService;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'stdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed MongoDB with Testing Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->itemFactory = \App::make('itemFactory');
        $this->subjectFactory = \App::make('subjectFactory');
        $this->itemService = \App::make('itemService');
        $this->userService = \App::make('userService');
        $this->groupService = \App::make('groupService');
        $this->subjectService = \App::make('subjectService');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $action = $this->argument('action');

        if ($action == 'seed') {
            $this->seed();
        } else if ($action == 'clear') {
            $this->clear();
        } else if ($action == 'reseed') {
            $this->clear();
            $this->seed();
        } else {
            throw new \RuntimeException("Incorrect action.");
        }
    }

    /**
     * Seeds Subjects
     */
    public function seedSubjects()
    {
        $filename = dirname(__FILE__) . '/../../../resources/subjects.json';
        $json = implode('', file($filename));

        $subjects = json_decode($json, true);
        foreach ($subjects as $subject) {
            $this->subjectService->createSubject($this->subjectFactory->fromArray($subject));
        }
    }

    /**
     * Seeds Items
     */
    public function seedItems()
    {
        $filename = dirname(__FILE__) . '/../../../resources/items.json';
        $json = implode('', file($filename));

        $items = json_decode($json, true);
        foreach ($items as $item) {
            $this->itemService->createItem($this->itemFactory->fromArray($item));
        }
    }

    /**
     * Seeds Users
     */
    public function seedUsers()
    {
        $filename = dirname(__FILE__) . '/../../../resources/users.json';
        $json = implode('', file($filename));

        $users = json_decode($json, true);
        foreach ($users as $user) {
            $this->userService->createUser(new User($user));
        }
    }

    /**
     * Seeds Groups
     */
    public function seedGroups()
    {
        $filename = dirname(__FILE__) . '/../../../resources/groups.json';
        $json = implode('', file($filename));

        $groups = json_decode($json, true);
        foreach ($groups as $group) {
            $this->groupService->createGroup(new Group($group));
        }
    }

    public function seed()
    {
        echo "SEEDING... ";

        $this->seedSubjects();
        $this->seedUsers();
        $this->seedGroups();
        $this->seedItems();

        echo "OK!\n";
    }

    public function clear()
    {
        echo "CLEARING... ";

        $db = \App::make('mongoDbDriver')->getDb();
        $db->item->remove();
        $db->user->remove();
        $db->subject->remove();
        $db->groups->remove();
        $db->comments->remove();

        echo "OK!\n";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('action', InputArgument::REQUIRED, 'An action (seed|clear)'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            // array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
