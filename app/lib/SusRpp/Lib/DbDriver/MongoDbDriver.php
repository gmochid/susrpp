<?php
namespace SusRpp\Lib\DbDriver;

class MongoDbDriver
{
    protected $mongoClient;
    protected $db;

    /**
     * Construct a MongoDB Driver
     *
     * @param string $connectionString e.g 'mongodb://{$username}:${password}@localhost:${port}'
     * @param string $databaseName e.g. 'user'
     */
    public function __construct($connectionString, $databaseName)
    {
        $this->mongoClient = new \MongoClient($connectionString);
        $this->db = $this->mongoClient->selectDB($databaseName);
    }

    public function getMongoClient()
    {
        return $this->mongoClient;
    }

    public function getDb()
    {
        return $this->db;
    }

    public function getCollection($collectionName)
    {
        return $this->db->{$collectionName};
    }
}
