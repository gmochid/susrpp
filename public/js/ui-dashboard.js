(function($, API) {

    // TODO

    window.currentUser = {};

    API.getCurrentUser(function(userData) {
        window.currentUser = userData;
    }, /* synchronous request */ true );

    $('#nameLink').text(window.currentUser.name);

    window.RppList = function RppList($scope) {
        $scope.rppList = {};

        $scope.deleteRpp = function($rppID, $rppTheme) {
            var confirmation = confirm('Anda akan menghapus RPP "' + $rppTheme + '". \n' +
                'Klik "OK" jika anda yakin untuk menghapus.');

            if(confirmation) {
                API.removeItem($rppID, function() {
                        alert("RPP '" + $rppTheme + "' telah dihapus");
                        window.location.reload(true);
                    }, function() {
                        alert("RPP '" + $rppTheme + "' gagal dihapus");
                    }, /* synchronous request */ true
                );
            }
        }

        $scope.finalizeRpp = function($rppID, $rppTheme) {
            var confirmation = confirm('Anda akan memfinalisasi RPP "' + $rppTheme + '"\n' +
                'Klik "OK" jika anda yakin untuk memfinalisasi\n\n' +
                'Proses ini TIDAK DAPAT dikembalikan lagi');

            if(confirmation) {
                var item;

                API.getItem($rppID, function(itemData) {
                    item = itemData;
                }, null, /* synchronous request */ true);

                item.status = "published";

                API.updateItem(item,
                    function(data) {
                        alert("RPP berhasil difinalisasi");
                        location.reload();
                    },
                    function(jqxhr) {
                        alert("RPP gagal difinalisasi");
                    }
                );
            }
        }

        API.findItem("groupName", window.currentUser.groupName,
            function(data) {
                $scope.rppList = data;
            }, /* synchronous request */ true
        );
    }

})(jQuery, SusRppApi);
