(function($) {

    var msgBox = $('#message-box');

    $('#login-form').submit(function(event) {
        event.preventDefault();

        var actionUrl = $(this).attr('action');

        var username = $('input[name="username"]').val();
        var password = $('input[name="password"]').val();

        $.ajax({
            type: 'POST',
            url: actionUrl,
            data: { "username": username, "password": password },
            error: function(jqxhr, status) {
                msgBox.attr('class', 'alert alert-danger');
                msgBox.text('Error! ' + jqxhr.responseJSON['error']);
                msgBox.show();

                setTimeout(function() {
                    msgBox.fadeOut();
                }, 1800);
            },
            success: function(data, status, jqxhr) {

                msgBox.attr('class', 'alert alert-success');
                msgBox.text('Success! ' + data['success']);
                msgBox.show();

                window.open('/', '_self');
            }
        });
    });

})(jQuery);
