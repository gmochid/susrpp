(function(window, API) {

    window.users = [

    ];

    window.subjects = [

    ];

    window.currentUser = {};

    API.findUser('', function(data) {
        window.users = data;
    }, /* synchronous request */ true);

    API.listSubject(function(data) {
        window.subjects = data;
    }, /* synchronous request (!) */ true);

    API.getCurrentUser(function(userData) {
        window.currentUser = userData;
    }, /* synchronous request */ true);

    API.findUserByGroup(window.currentUser.groupName, function(data) {
        window.usersInGroup = data;
    }, /* synchronous request */ true);

    // Default Item
    window.item = {
        "title": "",
        "status": "draft",
        "authors": [

        ],
        "authorIds": [

        ],
        "description": "",
        "lookup": "",
        "tags": [

        ],
        "data": {
            "school": "",
            "topic": "",
            "theme": "",
            "class": 4,
            "term": 1,
            "relatedSubjects": [

            ],
            "goals": [

            ],
            "materials": "",
            "activities": {
                "learnApproach": [

                ],
                "learnMethods": [

                ],
                "learnModels": [

                ],
                "startActivity": "",
                "coreActivity": "",
                "closingActivity": "",
                "startDuration": 5,
                "coreDuration": 5,
                "closingDuration": 10
            },
            "resources": [

            ],
            "assessment": {
                "multipleChoice": [

                ],
                "essay": [

                ],
                "simpleQuiz": [

                ],
                "matching": [

                ]
            },
            "workAssessment": [

            ],
            "observationAssessment": [

            ]
        }
    };

    if ((window.EditorData.action == "edit") || (window.EditorData.action == "view")) {
        var itemId = window.EditorData.id;

        API.getItem(itemId, function(itemData) {
            window.item = itemData;
        }, null, /* synchronous request */ true);
    } else {
        window.item.authors.push(window.currentUser.name);
        window.item.authorIds.push(window.currentUser.username);
    }

    window.learnApproaches = [
        { name: "Pendekatan Scientific", checked: false },
        { name: "Pendekatan Lingkungan", checked: false },
        { name: "Pendekatan Keterampilan Proses Sains", checked: false },
        { name: "Pendekatan Konsep", checked: false },
        { name: "Pendekatan Inquiry", checked: false },
        { name: "Pendekatan Deduktif", checked: false },
        { name: "Pendekatan Induktif", checked: false },
        { name: "Pendekatan Kontekstual", checked: false },
    ];

    window.learnMethods = [
        { name: "Ceramah", checked: false },
        { name: "Diskusi", checked: false },
        { name: "Eksperimen", checked: false },
        { name: "Demonstrasi", checked: false },
        { name: "Role Play", checked: false },
        { name: "Karyawisata", checked: false },
        { name: "Proyek", checked: false }
    ];

    window.learnModels = [
        { name: "Inquiry Learning", checked: false },
        { name: "Discovery Learning", checked: false },
        { name: "Project Based Learning", checked: false },
        { name: "Problem Based Learning", checked: false },
        { name: "Problem Solving", checked: false },
        { name: "Mind Mapping", checked: false },
        { name: "Contextual Teaching Learning", checked: false }
    ];

})(window, SusRppApi);

