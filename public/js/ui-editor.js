(function($, angular, API) {

    // Initalization

    $('#nameLink').text(window.currentUser.name);

    if(window.EditorData.action != "view") {
        $('#itemMaterial').ckeditor();
        $('#itemStartActivity').ckeditor();
        $('#itemCoreActivity').ckeditor();
        $('#itemClosingActivity').ckeditor();
    }

    var editorApp = angular.module('SusrppEditor', ['ngSanitize']);

    // -- Begin helper --
    var helper = {
        refineSubjects: function($scope) {
            angular.forEach($scope.subjects, function(subject) {
                angular.forEach(subject.coreCompetences, function(c) {
                    c['shortdesc'] = c.number + '. ' + c.description.substr(0, 32) + '...';
                    c['fulldesc']  = c.number + '. ' + c.description;
                    c['indicator'] = [];

                    angular.forEach(c.basicCompetences, function(bc) {
                        bc['shortdesc'] = bc.number + '. ' + bc.description.substr(0, 32) + '...';
                        bc['fulldesc']  = bc.number + '. ' + bc.description;
                        bc['selected']  = false;
                    });
                });
            });
        },

        repopulateIndicators: function($scope) {
            $scope.indicators = [];

            angular.forEach($scope.item.data.relatedSubjects, function(subject) {
                angular.forEach(subject.indicator, function(indicator) {
                    $scope.indicators.push(indicator);
                });
            });
        },

        refreshComment: function($scope) {
            if (window.EditorData.id !== '') {
                API.findComment(window.EditorData.id, function(data) {
                    $scope.comments = data;
                    console.log($scope.comments);
                }, null, /* synchronous request */ true);
            }
        },

        addToScopeArray: function($scopeField, fieldId) {
            var val = $(fieldId).val();
            if (val == '')
                return;

            // Uniqueness validation
            if ($scopeField.indexOf(val) === -1) {
                $(fieldId).val('');
                $scopeField.push(val);
            }
        },

        arrayRemoveIndex: function(array, index) {
            var tmp = array;
            array = [];

            for (var i in tmp) {
                if (i != index) {
                    array.push(tmp[i]);
                }
            }

            return array;
        },

        checkCheckbox: function($scopeData, $scopeModel) {
            angular.forEach($scopeData, function(dt) {
                var exist = false;
                angular.forEach($scopeModel, function(m) {
                    if (dt.name == m) {
                        exist = true;
                    }
                });

                if (exist) {
                    dt.checked = true;
                }
            });
        }
    };
    // -- End helper --

    // -- Begin right navigation --
    $('.right-nav-link').click(function() {
        $('.active-nav').removeClass('active-nav');

        var partId = $(this).attr('data-show');
        $(this).addClass('active-nav');

        $('.active-item-part').removeClass('active-item-part');

        $('#' + partId).addClass('active-item-part');
    });

    $('#nav-link-save').click(function() {
        $('#bottom-box > form > button').css('font-size', '0.5em');

        $('#bottom-box > form > button').animate({ fontSize: '1em' }, 250);
    });
    // -- End right navigation --

    // ------------------------------------------------------------------------
    // Editor Controller ------------------------------------------------------
    ////
    editorApp.controller('EditorController', function($scope) {

        ///////////////////////////////////////////////////////////////////////
        // Initialization
        ////

        $scope.currentUser = window.currentUser.username;

        $scope.view = window.EditorData.action == "view";
        $scope.new = window.EditorData.action == "new";
        $scope.item = window.item;
        $scope.item.ownerGroup = window.currentUser.groupName;
        $scope.subjects = window.subjects;
        $scope.authors = [];
        $scope.users = [];
        $scope.indicators = [];
        $scope.comments = [];

        // used for edit sub-item
        $scope.updateRelatedSubjectIndex = null;
        $scope.updateActivitiesIndex = null;
        $scope.updateAssessmentIndex = null;

        $scope.relatedSubjectOption = {
            indicator: []
        };

        $scope.learnApproaches = window.learnApproaches;
        $scope.learnMethods    = window.learnMethods;
        $scope.learnModels     = window.learnModels;

        helper.refineSubjects($scope);
        helper.repopulateIndicators($scope);
        helper.refreshComment($scope);

        for (var i in $scope.item.authors) {
            $scope.authors.push({
                username: $scope.item.authorIds[i],
                name: $scope.item.authors[i]
            });
        }

        var repopulateAuthorOptions = function() {
            $scope.users = [];
            var users = window.usersInGroup;

            angular.forEach(users, function(user) {
               var exist = false;
               angular.forEach($scope.authors, function(author) {
                   if (user.username == author.username) {
                       exist = true;
                   }
               });

               if (!exist) {
                   $scope.users.push(user);
               }
            });

            $scope.authorOption = $scope.users[0];
        };
        repopulateAuthorOptions();

        ////////////////////////////////////////////////////////////////////////
        // Leave window prompt
        ////

        var dataChanged = false;
        var skipDataChangeCheck = false;

        var originalItem = {};
        angular.copy($scope.item, originalItem);

        if (window.EditorData.action == "view") {
            $("#itemMaterialView").html($scope.item.data.materials);
            $("#itemStartActivityView").html($scope.item.data.activities.startActivity);
            $("#itemCoreActivityView").html($scope.item.data.activities.coreActivity);
            $("#itemClosingActivityView").html($scope.item.data.activities.closingActivity);
        }

        var isThereAnyNewInputNotSavedYet = function() {
            // buggy, commented for a while
            /*
            var eIds = [
                "#newGoal",
                "#newResource"
            ];

            for (var i = 0; i < eIds.length; i++) {
                var eId = eIds[i];
                if ($(eId).val() != '') {
                    return true;
                }
            }
            */

            return false;
        }

        var checkDataChanged = function() {
            dataChanged = isThereAnyNewInputNotSavedYet();

            if (!dataChanged) {
                if (!angular.equals($scope.item, originalItem)) {
                    dataChanged = true;
                }
            }
        };

        $(window).bind('beforeunload', function() {
            checkDataChanged();

            if (window.EditorData.action != "view") {
                if (dataChanged) {
                    return "Data yang anda masukkan mungkin belum tersimpan. Akan keluar sekarang?";
                }
            }
        });

        ///////////////////////////////////////////////////////////////////////
        // Methods
        ////

        /**
         * Add author
         */
        $scope.addAuthor = function() {
            if ($scope.authorOption === undefined)
                return;

            // Push the selected option
            $scope.authors.push($scope.authorOption);
            $scope.item.authors.push($scope.authorOption.name);
            $scope.item.authorIds.push($scope.authorOption.username);

            repopulateAuthorOptions();
        };

        /**
         * Remove author
         */
        $scope.removeAuthor = function(username) {
            // Repopulate scope authors
            $scope.authors = [];
            for (var i in $scope.item.authorIds) {
                if ($scope.item.authorIds[i] != username || username == window.currentUser.username) {
                    $scope.authors.push({
                        username: $scope.item.authorIds[i],
                        name: $scope.item.authors[i]
                    });
                }
            }

            // Repopulate item's authors (authorIds and authors)
            $scope.item.authorIds = [];
            $scope.item.authors = [];
            angular.forEach($scope.authors, function(author) {
                $scope.item.authors.push(author.name);
                $scope.item.authorIds.push(author.username);
            });

            repopulateAuthorOptions();
        };

        /**
         * Add tag
         */
        $scope.addTag = function() {
            helper.addToScopeArray($scope.item.tags, '#itemNewTag');
        };

        /**
         * Remove tag
         */
        $scope.removeTag = function(index) {
            $scope.item.tags = helper.arrayRemoveIndex($scope.item.tags, index);
        };

        $scope.resetSubjectOption = function() {
            if ($scope.subjectOption == null) {
                $scope.coreCompetenceOption = null;

                helper.refineSubjects($scope);
            }
        };

        /**
         * Add new indicator which will be added to related subject
         */
        $scope.addRelatedSubjectIndicator = function() {
            helper.addToScopeArray($scope.relatedSubjectOption.indicator, '#newIndicator');
        };

        /**
         * Remove indicator from new indicator list
         */
        $scope.removeRelatedSubjectIndicator = function(index) {
            $scope.relatedSubjectOption.indicator =
                helper.arrayRemoveIndex($scope.relatedSubjectOption.indicator, index);
        };

        /**
         * Prepare form for related subject
         */
        $scope.prepareAddRelatedSubject = function() {
            window.location = "#item-related-subject-popup";

            $scope.updateRelatedSubjectIndex = null;

            $scope.subjectOption = null;
            $scope.resetSubjectOption();
        }

        /**
         * Add related subject
         */
        $scope.addRelatedSubject = function() {
            var subjectName    = $scope.subjectOption.subjectName;
            var subjectId      = $scope.subjectOption._id;
            var coreCompetence = $scope.coreCompetenceOption.fulldesc;
            var indicator      = $scope.relatedSubjectOption.indicator;
            var basicCompetences = [];

            if (indicator.length == 0) {
                alert("Anda belum menambahkan indikator. Mohon menambahkan minimal 1 indikator.");
                return;
            }

            angular.forEach($scope.coreCompetenceOption.basicCompetences, function(bc) {
                if (bc.selected) {
                    basicCompetences.push(bc.fulldesc);
                }
            });

            if (basicCompetences.length == 0) {
                alert("Anda belum memilih kompetensi dasar. Mohon memilih minimal 1 kompetensi dasar.");
                return;
            }

            var newRelatedSubject = {
                subjectId: subjectId,
                subjectName: subjectName,
                coreCompetence: coreCompetence,
                basicCompetence: basicCompetences,
                indicator: indicator
            };

            $scope.relatedSubjectOption.indicator = [];
            $scope.subjectOption = null;
            $scope.resetSubjectOption();

            if($scope.updateRelatedSubjectIndex == null) {
                $scope.item.data.relatedSubjects.push(newRelatedSubject);
            } else {
                $scope.item.data.relatedSubjects[$scope.updateRelatedSubjectIndex] = newRelatedSubject;
                $scope.updateRelatedSubjectIndex = null;
            }
            helper.repopulateIndicators($scope);

            window.alert("Mata pelajaran terkait berhasil disimpan sementara. " +
                "Untuk menyimpan dokumen RPP, klik \"Simpan RPP\".");

            window.location = "#close";
        };

        /**
         * Prepare edit form for related subject
         */
        $scope.prepareUpdateRelatedSubject = function(index) {
            window.location = "#item-related-subject-popup";

            relatedSubject = $scope.item.data.relatedSubjects[index];

            // duplicate add situation
            angular.forEach($scope.subjects, function(subject) {
                if (subject.subjectName == relatedSubject.subjectName) {
                    $scope.subjectOption = subject;
                }
            });

            $scope.coreCompetenceOption = {
                fulldesc: relatedSubject.coreCompetence,
                basicCompetences: []
            };

            $scope.relatedSubjectOption.indicator = relatedSubject.indicator;

            angular.forEach($scope.subjectOption.coreCompetences[0].basicCompetences, function(bc) {
                if (bc.fulldesc == relatedSubject.basicCompetence) {
                    $scope.coreCompetenceOption.basicCompetences.push({
                        selected: true,
                        fulldesc: bc.fulldesc
                    });
                } else {
                    $scope.coreCompetenceOption.basicCompetences.push({
                        selected: false,
                        fulldesc: bc.fulldesc
                    });
                }
            });

            $scope.updateRelatedSubjectIndex = index;
        };

        /**
         * Remove related subject
         */
        $scope.removeRelatedSubject = function(index) {
            var ok = window.confirm("Anda akan menghapus sebuah mata pelajaran " +
                "terkait beserta kompetensi dan indikatornya. " +
                "Klik \"OK\" jika anda yakin untuk menghapus.");

            if (ok) {
                $scope.item.data.relatedSubjects =
                    helper.arrayRemoveIndex($scope.item.data.relatedSubjects, index);
            }
        };

        /**
         * Add goal
         */
        $scope.addGoal = function() {
            helper.addToScopeArray($scope.item.data.goals, '#newGoal');
        };

        /**
         * Remove goal
         */
        $scope.removeGoal = function(index) {
            $scope.item.data.goals = helper.arrayRemoveIndex($scope.item.data.goals, index);
        };

        /**
         * Add resource
         */
        $scope.addResource = function() {
            helper.addToScopeArray($scope.item.data.resources, '#newResource');
        };

        $scope.removeResource = function(index) {
            $scope.item.data.resources = helper.arrayRemoveIndex($scope.item.data.resources, index);
        };

        // Activities ---------------------------------------------------------

        console.log($scope.item);

        // Acitivity Header -- (methods, approaches, models)
        // Learn Methods
        helper.checkCheckbox($scope.learnMethods,
            $scope.item.data.activities.learnMethods);
        // Learn Approach
        helper.checkCheckbox($scope.learnApproaches,
            $scope.item.data.activities.learnApproach);
        // Learn Models
        helper.checkCheckbox($scope.learnModels,
            $scope.item.data.activities.learnModels);

        // Activities -- (start, core, closing)
        $scope.sumDuration = function() {
            var startDuration   = parseInt($scope.item.data.activities.startDuration);
            var coreDuration = parseInt($scope.item.data.activities.coreDuration);
            var closingDuration = parseInt($scope.item.data.activities.closingDuration);

            return startDuration + coreDuration + closingDuration;
        };

        // Assessments --------------------------------------------------------

        $scope.newAssessmentType = "";

        var defaultMcAsmt = {
            indicator: '',
            question: '',
            options: [
                '', '', '', ''
            ],
            solution: null,
            score: 10,
            cognitiveLevel: 'C1'
        };

        var defaultEsAsmt = {
            indicator: '',
            question: '',
            solutions: [

            ],
            cognitiveLevel: 'C1'
        };

        var defaultEsAsmtSolution = {
            keyPoint: '',
            score: 10,
        };

        var defaultSqAsmt = {
            indicator: '',
            question: '',
            solution: '',
            score: 10,
            cognitiveLevel: 'C1'
        };

        var defaultMaAsmt = {
            indicator: '',
            question: '',
            pairA: '',
            pairB: '',
            solution: '',
            score: 10,
            cognitiveLevel: 'C1'
        };

        // Initialization
        $scope.mcAsmt = angular.copy(defaultMcAsmt);
        $scope.esAsmt = angular.copy(defaultEsAsmt);
        $scope.esAsmtSolution = angular.copy(defaultEsAsmtSolution);
        $scope.sqAsmt = angular.copy(defaultSqAsmt);
        $scope.maAsmt = angular.copy(defaultMaAsmt);

        /**
         * Prepare form for assessment
         */
        $scope.prepareAddAssessment = function() {
            window.location = "#item-assessment-popup";

            $scope.updateAssessmentIndex = null;
            $scope.mcAsmt = angular.copy(defaultMcAsmt);
            $scope.esAsmt = angular.copy(defaultEsAsmt);
            $scope.esAsmtSolution = angular.copy(defaultEsAsmtSolution);
            $scope.sqAsmt = angular.copy(defaultSqAsmt);
            $scope.maAsmt = angular.copy(defaultMaAsmt);
        }

        /**
         * Prepare edit form for assessment
         */
        $scope.prepareUpdateAssessment = function(type, index) {
            window.location = "#item-assessment-popup";

            $scope.newAssessmentType = type;

            if ($scope.newAssessmentType == 'mc') {
                $scope.mcAsmt = $scope.item.data.assessment.multipleChoice[index];
            }
            else if ($scope.newAssessmentType == 'es') {
                $scope.esAsmt = $scope.item.data.assessment.essay[index];
            }
            else if ($scope.newAssessmentType == 'sq') {
                $scope.sqAsmt = $scope.item.data.assessment.simpleQuiz[index];
            }
            else if ($scope.newAssessmentType == 'ma') {
                $scope.maAsmt = $scope.item.data.assessment.matching[index];
            }

            $scope.updateAssessmentIndex = index;
        };

        $scope.addAssessment = function() {
            if ($scope.newAssessmentType == 'mc') {
                $scope.addAsmtMultipleChoice();
            }
            else if ($scope.newAssessmentType == 'es') {
                $scope.addAsmtEssay();
            }
            else if ($scope.newAssessmentType == 'sq') {
                $scope.addAsmtSimpleQuiz();
            }
            else if ($scope.newAssessmentType == 'ma') {
                $scope.addAsmtMatching();
            }

            $scope.updateAssessmentIndex = null;

            window.alert("Soal yang dibuat berhasil disimpan sementara. " +
                "Untuk menyimpan dokumen RPP, klik \"Simpan RPP\".");
        };

        $scope.addAsmtMultipleChoice = function() {
            if ($scope.mcAsmt.solution == '') {
                alert("Mohon memilih solusi yang benar.");
                return;
            }

            if($scope.updateAssessmentIndex == null) {
                $scope.item.data.assessment.multipleChoice.push($scope.mcAsmt);
            } else {
                $scope.item.data.assessment.multipleChoice[$scope.updateAssessmentIndex] = $scope.mcAsmt;
            }

            $scope.mcAsmt = angular.copy(defaultMcAsmt);
        };

        $scope.addAsmtSolution = function() {
            if ($scope.esAsmtSolution.keyPoint == '' || $scope.esAsmtSolution.score == '')
                return;

            $scope.esAsmt.solutions.push($scope.esAsmtSolution);
            $scope.esAsmtSolution = angular.copy(defaultEsAsmtSolution);
        };

        $scope.removeAsmtSolution = function(index) {
            $scope.esAsmt.solutions = helper.arrayRemoveIndex($scope.esAsmt.solutions, index);
        };

        $scope.addAsmtEssay = function() {
            if ($scope.esAsmt.solutions.length == 0) {
                alert("Mohon menambahkan minimal 1 solusi.");
                return;
            }

            if($scope.updateAssessmentIndex == null) {
                $scope.item.data.assessment.essay.push($scope.esAsmt);
            } else {
                $scope.item.data.assessment.essay[$scope.updateAssessmentIndex] = $scope.esAsmt;
            }

            $scope.esAsmt = angular.copy(defaultEsAsmt);
            $scope.esAsmtSolution = angular.copy(defaultEsAsmtSolution);
        };

        $scope.addAsmtSimpleQuiz = function() {
            if($scope.updateAssessmentIndex == null) {
                $scope.item.data.assessment.simpleQuiz.push($scope.sqAsmt);
            } else {
                $scope.item.data.assessment.simpleQuiz[$scope.updateAssessmentIndex] = $scope.sqAsmt;
            }

            $scope.sqAsmt = angular.copy(defaultSqAsmt);
        };

        $scope.addAsmtMatching = function() {
            // TODO

            if($scope.updateAssessmentIndex == null) {
                $scope.item.data.assessment.matching.push($scope.maAsmt);
            } else {
                $scope.item.data.assessment.matching[$scope.updateAssessmentIndex] = $scope.maAsmt;
            }

            $scope.maAsmt = angular.copy(defaultMaAsmt);
        };

        /**
         * Remove Assessment (types: mc, es, qs, ma)
         */
        $scope.removeAssessment = function(type, index) {
            var ok = window.confirm("Anda akan menghapus sebuah soal yang telah dibuat/tersimpan. " +
                "Klik \"OK\" jika anda yakin untuk menghapus.");

            if (ok) {
                $asmt = {
                    'mc': 'multipleChoice',
                    'es': 'essay',
                    'sq': 'simpleQuiz',
                    'ma': 'matching'
                };

                $scope.item.data.assessment[$asmt[type]] =
                    helper.arrayRemoveIndex($scope.item.data.assessment[$asmt[type]], index);
            }
        };

        /**
         * Add Work Assessment
         */
        $scope.addWorkAssessment = function() {
            helper.addToScopeArray($scope.item.data.workAssessment,
                '#newWorkAssessment');
        };

        /**
         * Remove Work Assessment
         */
        $scope.removeWorkAssessment = function(index) {
            $scope.item.data.workAssessment = helper.arrayRemoveIndex(
                $scope.item.data.workAssessment, index);
        };

        /**
         * Add Observation Assessment
         */
        $scope.addObservationAssessment = function() {
            helper.addToScopeArray($scope.item.data.observationAssessment,
                '#newObservationAssessment');
        };

        /**
         * Remove Observation Assessment
         */
        $scope.removeObservationAssessment = function(index) {
            $scope.item.data.observationAssessment =
                helper.arrayRemoveIndex($scope.item.data.observationAssessment, index);
        };

        // Comments --------------------------------------------------------

        $scope.addComment = function() {
            commentContent = $('#comment-content').val();

            if(commentContent == '')
                return;

            if (window.EditorData.id !== '') {
                API.postComment(window.EditorData.id, commentContent,
                    function(data) {
                        helper.refreshComment($scope);
                    },
                    function(jqxhr) {
                        alert('Komentar gagal disimpan.');
                    }, true
                );
            }

            $('#comment-content').val('');
        };

        $scope.removeComment = function(index) {
            var id = $scope.comments[index]['_id'];

            var ok = window.confirm(
                "Klik \"OK\" jika anda yakin untuk menghapus komentar ini.");

            if (ok) {
                API.removeComment(id,
                    function(data) {
                        helper.refreshComment($scope);
                    },
                    function(jqxhr) {
                        alert('Komentar gagal dihapus.');
                    }, true
                );
            }
        };

        window.viewItemScope = function() {
            console.log($scope.item);
        };

        window.viewIndicators = function() {
            console.log($scope.indicators);
        };

        window.compileItem = function() {
            var learnApproaches = [];
            var learnMethods = [];
            var learnModels = [];

            angular.forEach($scope.learnApproaches, function(l) {
                if (l.checked) {
                    learnApproaches.push(l.name);
                }
            });

            angular.forEach($scope.learnMethods, function(l) {
                if (l.checked) {
                    learnMethods.push(l.name);
                }
            });

            angular.forEach($scope.learnModels, function(l) {
                if (l.checked) {
                    learnModels.push(l.name);
                }
            });

            $scope.item.data.activities.startActivity   = $('#itemStartActivity').val();
            $scope.item.data.activities.coreActivity    = $('#itemCoreActivity').val();
            $scope.item.data.activities.closingActivity = $('#itemClosingActivity').val();

            $scope.item.data.activities.learnApproach = learnApproaches;
            $scope.item.data.activities.learnMethods  = learnMethods;
            $scope.item.data.activities.learnModels   = learnModels;

            $scope.item.data.materials = $('#itemMaterial').val();

            $scope.item.status = "draft";
            $scope.item.title = $scope.item.data.theme;

            return $scope.item;
        };

        window.validateItem = function() {
            if (($scope.sumDuration() > 70) || ($scope.sumDuration() < 0)) {
                alert("Total durasi maksimal 70 menit.");

                $('.active-nav').removeClass('active-nav');
                $('.active-item-part').removeClass('active-item-part');

                $('#part-activities-nav').addClass('active-nav');
                $('#part-activities').addClass('active-item-part');
                window.location.href = "#part-activities";

                return false;
            }

            if (isThereAnyNewInputNotSavedYet()) {
                alert("Terdapat salah satu input yang belum disimpan. " +
                      "Dimohon untuk memeriksa ulang kembali.");
                return false;
            }

            return true;
        };

        $scope.persistItem = function() {
            if (!window.validateItem()) {
                return;
            }
            window.compileItem();

            if (window.EditorData.action == "edit") {
                API.updateItem($scope.item,
                    function(data) {
                        alert("RPP telah berhasil disimpan.");
                        angular.copy($scope.item, originalItem);
                        dataChanged = false;
                    },
                    function(jqxhr) {
                        alert("RPP gagal disimpan.");
                    }
                );
            } else {
                API.postItem($scope.item,
                    function(data) {
                        alert("RPP baru telah berhasil dibuat.");

                        $scope.item._id = data.id;
                        window.EditorData.action = "edit";

                        console.log($scope.item._id);
                        console.log(data);

                        window.open(window.RootLocation, '_self');
                    },
                    function(jqxhr) {
                        alert("RPP gagal disimpan.");
                    }
                );
            }
        };

        $scope.back = function() {
            window.open(window.RootLocation, "_self");
        };


    // -- End of HomeController --
    });

})(jQuery, angular, SusRppApi);
