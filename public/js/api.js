(function($) {

    if (window.RootLocation == undefined) {
        window.RootLocation = '';
    }

    // Based on routes
    var apiUrl = {
        subject:      '/api/subject',
        subjectAll:   '/api/subject/all',
        subjectFind:  '/api/subject/find',
        item:         '/api/item',
        itemFind:     '/api/item/find',
        itemFindOne:  '/api/item/findone',
        comment:      '/api/comment',
        commentFind:  '/api/comment/item',
        user:         '/api/user',
        userFind:     '/api/user/find',
        userByGroup:  '/api/user/group',
        userLogin:    '/api/user/session/auth',
        userLogout:   '/api/user/session/forget',
        userUpdate:   '/api/user/update',
        userMove:     '/api/user/movegroup',
        userRegister: '/api/user/register',
        createGroup:  '/api/user/creategroup'
    };

    // Append window.RootLocation
    for (var i in apiUrl) {
        apiUrl[i] = window.RootLocation + apiUrl[i];
    }

    // ------------------------------------------------------------------------
    // Helpers ----------------------------------------------------------------
    ////
    var doX = function(xMethod, url, onSuccess, onError, data, sync) {
        $.ajax({
            type: xMethod,
            url: url,
            // @@@
            data: data == undefined ? {} : data,
            success: function(data, status, jqxhr) {
                if (onSuccess != undefined && onSuccess != null) {
                    onSuccess(data, status, jqxhr);
                }
            },
            error: function(jqxhr, status) {
                if (onError != undefined && onError != null) {
                    onError(jqxhr, status);
                }
            },
            async: sync === undefined ? true : false
        });
    };

    var doXJson = function(xMethod, url, dataJson, onSuccess, onError, sync) {
        $.ajax({
            type: xMethod,
            url: url,
            data: JSON.stringify(dataJson),
            processData: false,
            contentType: 'application/json',
            // @@@
            success: function(data, status, jqxhr) {
                if (onSuccess != undefined && onSuccess != null) {
                    onSuccess(data, status, jqxhr);
                }
            },
            error: function(jqxhr, status) {
                if (onError != undefined && onError != null) {
                    onError(jqxhr, status);
                }
            },
            async: sync === undefined ? true : false
        });
    };

    var doGet = function(url, onSuccess, onError, sync) {
        doX('GET', url, onSuccess, onError, undefined, sync);
    };

    var doPut = function(url, onSuccess, onError, sync) {
        doX('PUT', url, onSuccess, onError, undefined, sync);
    }

    var doPost = function(url, data, onSuccess, onError, sync) {
        doX('POST', url, onSuccess, onError, data, sync);
    };

    var doDelete = function(url, onSuccess, onError, sync) {
        doX('DELETE', url, onSuccess, onError, undefined, sync);
    };

    var doPostJson = function(url, dataJson, onSuccess, onError, sync) {
        doXJson('POST', url, dataJson, onSuccess, onError, sync);
    };

    var doPutJson = function(url, dataJson, onSuccess, onError, sync) {
        doXJson('PUT', url, dataJson, onSuccess, onError, sync);
    };

    // ------------------------------------------------------------------------
    // SusRppApi --------------------------------------------------------------
    ////
    window.SusRppApi = {
        /**
         * Get a user (by username).
         */
        getUser: function getUser(username, onSuccess, onError, sync) {
            var url = apiUrl.user + '/' + encodeURIComponent(username);
            doGet(url, onSuccess, onError, sync);
        },

        /**
         * Get the current logged user.
         */
        getCurrentUser: function getCurrentUser(onSuccess, sync) {
            doGet(apiUrl.user, onSuccess, null, sync);
        },

        /**
         * Update User
         */
        updateUser: function updateUser(userData, onSuccess, onError, sync) {
            doPutJson(apiUrl.userUpdate, userData, onSuccess, onError, sync);
        },

        /**
         * Register User
         */
        registerUser: function registerUser(userData, onSuccess, onError, sync) {
            doPostJson(apiUrl.userRegister, userData, onSuccess, onError, sync);
        },

        /**
         *
         */
        createGroup: function createGroup(groupName, onSuccess, onError, sync) {
            doPost(apiUrl.createGroup + '?groupName=' + encodeURIComponent(groupName), null, onSuccess, onError, sync);
        },

        /**
         * Remove User
         */
        removeUser: function removeUser(username, onSuccess, onError, sync) {
            var url = apiUrl.user + '/' + encodeURIComponent(username);
            doDelete(url, onSuccess, onError, sync);
        },

        /**
         * Find users by name; onSuccess will yields all users if name is empty.
         */
        findUser: function findUser(name, onSuccess, sync) {
            var urlParams = '?name=' + encodeURIComponent(name);
            doGet(apiUrl.userFind + urlParams, onSuccess, null, sync);
        },

        /**
         *
         */
        findUserByGroup: function findUserByGroup(groupName, onSuccess, sync) {
            doGet(apiUrl.userByGroup + '/' + encodeURIComponent(groupName), onSuccess, null, sync);
        },

        /**
         *
         */
        moveUser: function moveUser(username, groupName, onSuccess, sync) {
            var urlParams = '?username=' + encodeURIComponent(username) + '&groupname=' + encodeURIComponent(groupName);
            doPut(apiUrl.userMove + urlParams, onSuccess, null, sync);
        },

        /**
         * findUser + status
         */
        findUserWithStatus: function findUserWithStatus(name, status, onSuccess, sync) {
            var urlParams = '?name=' + encodeURIComponent(name) + '&status=' + encodeURIComponent(status);
            doGet(apiUrl.userFind + urlParams, onSuccess, null, sync);
        },

        /**
         * List subject.
         */
        listSubject: function listSubject(onSuccess, sync) {
            doGet(apiUrl.subjectAll, onSuccess, null, sync);
        },

        /**
         * Get an item (by id).
         */
        getItem: function getItem(id, onSuccess, onError, sync) {
            var url = apiUrl.item + '/' + encodeURIComponent(id);
            doGet(url, onSuccess, onError, sync);
        },

        /**
         * Post an item.
         */
        postItem: function postItem(itemData, onSuccess, onError, sync) {
            doPostJson(apiUrl.item, itemData, onSuccess, onError, sync);
        },

        /**
         * Update an item.
         */
        updateItem: function updateItem(itemData, onSuccess, onError, sync) {
            doPutJson(apiUrl.item, itemData, onSuccess, onError, sync);
        },

        /**
         * Remove an item.
         */
        removeItem: function removeItem(id, onSuccess, onError, sync) {
            var url = apiUrl.item + '/' + encodeURIComponent(id);
            doDelete(url, onSuccess, onError, sync);
        },

        /**
         * Find item.
         */
        findItem: function findItem(by, value, onSuccess, sync) {
            var urlParams = "?by=" + encodeURIComponent(by) + "&value=" + encodeURIComponent(value);
            doGet(apiUrl.itemFind + urlParams, onSuccess, null, sync);
        },

        /**
         * Find one item.
         */
        findOneItem: function findOneItem(by, value, onSuccess, onError, sync) {
            var urlParams = "?by=" + encodeURIComponent(by) + "&value=" + encodeURIComponent(value);
            doGet(apiUrl.itemFindOne + urlParams, onSuccess, onError, sync);
        },

        /**
         * Finds comments by item id
         */
        findComment: function findComment(itemId, onSuccess, onError, sync) {
            var url = apiUrl.commentFind + '/' + encodeURIComponent(itemId);
            doGet(url, onSuccess, onError, sync);
        },

        /**
         * Gets a comment by id
         */
        getComment: function findOneComment(id, onSuccess, onError, sync) {
            var url = apiUrl.comment + '/' + encodeURIComponent(id);
            doGet(url, onSuccess, onError, sync);
        },

        /**
         * Posts a new comment on an item
         */
        postComment: function postComment(itemId, comment, onSuccess, onError, sync) {
            var url = apiUrl.comment + '?itemId=' + encodeURIComponent(itemId) + '&comment=' + encodeURIComponent(comment)
            doPost(url, null, onSuccess, onError, sync);
        },

        /**
         * Removes a comment by id
         */
        removeComment: function removeComment(id, onSuccess, onError, sync) {
            var url = apiUrl.comment + '/' + encodeURIComponent(id);
            doDelete(url, onSuccess, onError, sync);
        }
    };

})(jQuery);
