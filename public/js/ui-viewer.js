(function($, angular, API) {

	$('#nameLink').text(window.currentUser.name);

    var viewerApp = angular.module('SusrppEditor', ['ngSanitize']);

    // ------------------------------------------------------------------------
    // Editor Controller ------------------------------------------------------
    ////
    viewerApp.controller('ViewerController', function($scope) {

    	$scope.item = window.item;

    	// Activities -- (start, core, closing)
        $scope.sumDuration = function() {
            var startDuration   = parseInt($scope.item.data.activities.startDuration);
            var coreDuration = parseInt($scope.item.data.activities.coreDuration);
            var closingDuration = parseInt($scope.item.data.activities.closingDuration);

            return startDuration + coreDuration + closingDuration;
        };

        $("#itemMaterialView").html($scope.item.data.materials);
        $("#itemStartActivityView").html($scope.item.data.activities.startActivity);
        $("#itemCoreActivityView").html($scope.item.data.activities.coreActivity);
        $("#itemClosingActivityView").html($scope.item.data.activities.closingActivity);


    });

})(jQuery, angular, SusRppApi);
