(function($, API) {

    window.currentUser = {};

    API.getCurrentUser(function(userData) {
        window.currentUser = userData;
    }, /* synchronous request */ true );

    $('#nameLink').text(window.currentUser.name);

    $('#closeFormButton').click(function() {
        $('#newuser-form').hide();
    });

    $('#openFormButton').click(function() {
        $('#newuser-form').show();
    });

    $('#closeFormButton2').click(function() {
        $('#newgroup-form').hide();
    });

    $('#openFormButton2').click(function() {
        $('#newgroup-form').show();
    });

    window.UserList = function UserList($scope) {
        $scope.userList = {};
        $scope.newUser = {
            username: '',
            password: '',
            name: '',
            email: ''
        };

        $scope.newGroup = {
            groupName: ''
        };

        API.findUserWithStatus('', -1, function(data) {
                $scope.userList = data;
            }, /* synchronous request */ true
        );

        var changeStatus = function(index, status) {
            var user = $scope.userList[index];
            delete user['$$hashKey'];

            user.status = status;

            API.updateUser(user, function(data) {
                $scope.userList[index].status = status;
            });
        }

        $scope.approveUser = function(index) {
            changeStatus(index, 1);
        };

        $scope.disapproveUser = function(index) {
            changeStatus(index, 0);
        }

        $scope.removeUser = function(index) {
            var user = $scope.userList[index];

            var ok = confirm('Lanjutkan menghapus user "' + user.username + '"?');
            if (ok) {
                API.removeUser(user.username, function(data) {});
                $scope.userList.splice(index, 1);
            }
        }

        $scope.registerUser = function() {
            API.registerUser($scope.newUser, function(data) {
                alert("Pembuatan user sukses!");
                window.open("/admin", "_self");
            }, function(jqxhr) {
                alert("Pembuatan user gagal!");
            });
        }

        $scope.registerGroup = function() {
            API.createGroup($scope.newGroup.groupName, function(data) {
                alert("Pembuatan grup sukses!");
                window.open("/admin", "_self");
            }, function(jqxhr) {
                alert("Pembuatan grup gagal!");
            });
        }
    }

})(jQuery, SusRppApi);
