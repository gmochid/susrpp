(function($) {

    var actionUrl = $('#logoutForm').attr('action');

    $.ajax({
        type: 'GET',
        url: actionUrl,
        success: function(data, status, jqxhr) {
            window.open('/', '_self');
        }
    });

})(jQuery);
