(function($, API){

    $('#register-form').submit(function(e) {
       e.preventDefault();

       var data = {
           name:      $('input[name=name]').val(),
           email:     $('input[name=email]').val(),
           username:  $('input[name=username]').val(),
           password:  $('input[name=password]').val(),
           groupName: $('select[name=groupName]').val()
       };

       API.registerUser(data,
           // OK
           function(data) {
               window.open("/login", "_self");
               console.log(data);
           },
           // Fail
           function(jqxhr) {
               // TODO
               alert("Registrasi gagal!");
               console.log(jqxhr);
           }
       );

       console.log(data);
    });

})(jQuery, SusRppApi);
