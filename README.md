## SusRpp

Requirements:

- [Composer](http://getcomposer.org/)
- PHPUnit 3.7.*
- PHP >= 5.3.3 (>= 5.4.0 recommended)
- PHP Mongo Extension
- PHP Mcrypt Extension (for Laravel)

Make sure you have a copy of `app/config/database.php` and
`app/config/mongodriver.php` from `app/config/*.dist` template,
and have them configured correctly.

Run `composer install` to resolve all dependencies.

Run `php artisan stdb reseed` to reseed the testing data.

Run `phpunit` to make sure all tests are passed. (Important)

Run `php artisan serve` to start application with the php's
built-in http server. No complex webserver configuration
required. (Recommended for development)

Read the wiki for more information.
